#!/usr/bin/perl

use warnings;
use strict;

use lib qw|lib ../lib|;
use Zaaksysteem::CLI;

### Load CLI object
my $cli     = Zaaksysteem::CLI->new;

BEGIN {
    eval 'use Zaaksysteem';

    if ($@) {
        print "\n\n=> Zaaksysteem error, is zaaksysteem correct geinstalleerd?\n"
            ."[$@]\n"
            ."\nUPGRADE IS ZINLOOS\n\n";
        exit;
    }
}

$|++;
#system $^O eq 'MSWin32' ? 'cls' : 'clear';


sub main {
    $cli->headline('ZAAKSYSTEEM UPGRADE (v' . $Zaaksysteem::VERSION . ')');

    $ENV{ZAAKSYSTEEM_HOME} = Zaaksysteem->config->{home};

    print "\n=> ZAAKSYSTEEM_HOME      : " . Zaaksysteem->config->{home};
    print "\n=> Zaaksysteem Customers :\n";
    print "  - " . join("\n  - ", keys %{ Zaaksysteem->config->{customers} });

    my $update_info = Zaaksysteem->upgrade_information;
}

main();

print "\n";

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

