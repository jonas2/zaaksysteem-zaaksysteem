/*global define*/
(function ( ) {
	
	function TimelineEvent ( ) {
	}
	
	TimelineEvent.prototype.getMimetype = function ( ) {
		var mimetype = this.mimetype || 'Unknown';
		mimetype = mimetype.replace('application/', '').replace('image/','');
		return mimetype;
	};
	
	// FIXME(dario): this is a fix for a backend bug where
	// timezones are not given for timestamps
	TimelineEvent.prototype.getTimestamp = function ( ) {
		var timestamp = this.timestamp || '';
		if(timestamp.substr(timestamp.length-1) !== 'Z') {
			timestamp += 'Z';
		}
		return timestamp;
	};
	
	define('nl.mintlab.timeline.TimelineEvent', function ( ) {
		return TimelineEvent;
	});
	
})();