/*global angular*/
(function () {
	"use strict";
	angular.module('Zaaksysteem.admin')
		.controller('nl.mintlab.plugins.WozTaxatieverslagController', ['$scope', '$http', function ($scope, $http ) {

			$scope.init = function (id, owner, woz_object_id) {
				var url = '/plugins/woz/object/' + id + '?owner=' + owner + '&object_id=' + woz_object_id;

				$http({
					method: 'GET',
					url: url
				})
					.success(function ( response ) {
						$scope.woz_object = response;
					});
					
				$http({
					method: 'GET',
					url: '/plugins/woz/settings'
				})
					.success(function ( response ) {
						$scope.settings = response;
					});
					
			};
		}]);
}());