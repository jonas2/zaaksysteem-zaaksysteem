/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.dom', [ 'Zaaksysteem.events' ]);
	
})();