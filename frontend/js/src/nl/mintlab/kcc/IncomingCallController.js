/*global angular, fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.IncomingCallController', [ '$scope', 'smartHttp', '$timeout', function ( $scope, smartHttp, $timeout ) {
			
			var Call = fetch('nl.mintlab.kcc.Call'),
				indexOf = fetch('nl.mintlab.utils.shims.indexOf');
				
			var _timeout = 5000,
				_relevance = 30000,
				_polling,
				_timeoutPromise,
				_callsById = {};
				
			$scope.incomingCalls = [];
				
			$scope.setEnabled = function ( enabled ) {
				if(enabled) {
					$scope.startPoll();
				}
			};
			
			$scope.startPoll = function ( ) {
				if(!_polling) {
					_polling = true;
					poll();
				}
			};
			
			$scope.stopPoll = function ( ) {
				if(_polling) {
					_polling = false;
					$timeout.cancel(_timeoutPromise);
					_timeoutPromise = null;
				}
			};
			
			function poll ( ) {
				_timeoutPromise = null;
				smartHttp.connect({
					method: 'GET',
					url: 'event/list/kcc/call',
					interval: _relevance / 1000
				})
					.success(function ( data ) {
						handleData(data);
						_timeoutPromise = $timeout(poll, _timeout);
					})
					.error(function ( data ) {
						handleError(data);
						_timeoutPromise = $timeout(poll, _timeout);
					});
			}
			
			function handleData ( data ) {
				var callEvent,
					call,
					calls = data && data.result ? data.result : [],
					relevantTime = new Date().getTime() - _relevance;
					
					
				for(var i = 0, l = calls.length; i < l; ++i) {
					callEvent = calls[i];
					if(_callsById[callEvent.id]) {
						call = _callsById[callEvent.id];
					} else {
						call = new Call();
						_callsById[callEvent.id] = call;
						$scope.incomingCalls.push(call);
					}
					for(var key in callEvent) {
						call[key] = callEvent[key];
					}
				}
				
				calls = $scope.incomingCalls.concat();
				for(i = 0, l = calls; i < l; ++i) {
					call = calls[i];
					if(new Date(call.timestamp).getTime() < relevantTime) {
						removeCallFromQueue(call);
					}
				}
				$scope.$emit('incomingcall', $scope.incomingCalls);
			}
			
			function handleError ( ) {
				
			}
			
			function removeCallFromQueue ( call ) {
				var index = indexOf($scope.incomingCalls, call);
				delete _callsById[call.id];
				if(index !== -1) {
					$scope.incomingCalls.splice(indexOf($scope.incomingCalls, call), 1);
				}
			}
			
			$scope.$on('callaccept', function ( event, call ) {
				removeCallFromQueue(call);
			});
			
			$scope.$on('callreject', function ( event, call ) {
				removeCallFromQueue(call);
			});
			
		}]);
		
})();