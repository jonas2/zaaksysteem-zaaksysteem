/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.EmailAttachmentsController', [ '$scope', 'smartHttp', 'translationService', function ( $scope, smartHttp, translationService ) {
			
			var indexOf = _.indexOf,
				forEach = _.forEach;
			
			$scope.template = null;
			$scope.templates = [];
			
			function getDataFromActionItem ( ) {
				
				var data = $scope.action.data,
					attachments = [],
					rcpt = data.rcpt;
				
				$scope.emailSubject = data.subject;
				$scope.emailContent = data.body;
				
				if(data.case_document_attachments) {
					forEach(data.case_document_attachments, function ( caseDoc ) {
						attachments.push({ id: caseDoc.case_type_document_id, naam: caseDoc.naam } );
					});
				}
				
				if(rcpt === 'aanvrager') {
					$scope.typeRecipient = 'appealer';
				} else if(rcpt === 'coordinator') {
					$scope.typeRecipient = 'coordinator';
				} else if(data.email) {
					$scope.typeRecipient = 'other';
					$scope.recipient = data.email;
				} else {
					$scope.typeRecipient = 'coworker';
					$scope.recipient = rcpt;
				}
				
			}
			
			$scope.reloadData = function ( ) {
				smartHttp.connect({
					method: 'GET',
					url: 'zaak/' + $scope.caseId + '/get_sjablonen',
					params: {
						type: 'notifications'
					}
				})
					.success(function ( data ) {
						$scope.templates = data.result;
						if(!$scope.typeEmail) {
						 	$scope.typeEmail = $scope.templates && $scope.templates.length ? 'template' : 'custom';
						}
					})
					.error(function ( /*data*/ ) {
						
					});
			};
			
			$scope.sendEmail = function ( ) {
				
				var data,
					recipient,
					typeRecipient,
					subject,
					from,
					body,
					caseId,
					attachments = [],
					fileAttachments = [],
					caseTypeDocumentAttachments = [],
					templateAttachments;
					
				switch($scope.typeRecipient) {
					case 'coworker':
					recipient = $scope.recipient.id;
					typeRecipient = 'medewerker_uuid';
					break;
					
					case 'appealer':
					typeRecipient = 'aanvrager';
					break;
					
					case 'coordinator':
					typeRecipient = 'coordinator';
					break;
					
					case 'other':
					recipient = $scope.recipientAddress;
					typeRecipient = 'custom_address';
					break;
				}
				
				subject = $scope.emailSubject;
				body = $scope.emailContent;
				from = $scope.userId;
				caseId = $scope.caseId;
				
				attachments = [];
				
				forEach($scope.selectedAttachments, function ( value ) {
					attachments.push(value.id);
				});
				
				if($scope.typeEmail === 'template' && $scope.template) {
					templateAttachments = $scope.template.bibliotheek_notificaties_id.attachments;
					forEach(templateAttachments, function ( attachment ) {
						caseTypeDocumentAttachments.push(attachment.bibliotheek_kenmerk_id);
					});
					
					subject = $scope.template.bibliotheek_notificaties_id.subject;
					body = $scope.template.bibliotheek_notificaties_id.message;
				}
				
				data = {
					recipient: recipient,
					recipient_type: typeRecipient,
					subject: subject,
					from: from,
					body: body,
					case_id: caseId
				};
				
				if(attachments.length) {
					if($scope.context === 'docs') {
						fileAttachments = attachments;
					} else {
						caseTypeDocumentAttachments = caseTypeDocumentAttachments.concat(attachments);
					}
				}
				
				if(fileAttachments.length) {
					data.file_attachments = fileAttachments;
				}
				
				if(caseTypeDocumentAttachments.length) {
					data.case_type_document_attachments = caseTypeDocumentAttachments;
				}
				
				smartHttp.connect({
					method: 'POST',
					url: 'zaak/send_mail',
					data: data
				})
					.success(function onSuccess ( /*data*/ ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('E-mail verstuurd')
						});
						$scope.closePopup();
					})
					.error(function onError ( /*data*/ ) {
						
					});
			};
			
			$scope.saveTemplate = function ( ) {
				
			};
			
			$scope.detach = function ( attachment ) {
				var index = indexOf($scope.selectedAttachments, attachment);
				if(index !== -1) {
					$scope.selectedAttachments.splice(index, 1);
				}
			};
			
			$scope.isAttached = function ( attachment ) {
				return indexOf($scope.selectedAttachments, attachment) !== -1;
			};
			
			$scope.getName = function ( attachment ) {
				var context = $scope.context,
					name = '';
				
				if(context === 'actions') {
					name = attachment.naam;
				} else if(context === 'docs') {
					name = attachment.name + attachment.extension;
				}
				return name;
			};
			
			$scope.init = function ( ) {
				
				if(!$scope.context) {
					throw new Error('Context not defined for EmailAttachmentsController');
				}
				
				if($scope.context === 'actions') {
					getDataFromActionItem();
				}
				
				if(!$scope.typeRecipient) {
					$scope.typeRecipient = 'coworker';
				}
				
				if(!$scope.attachments) {
					$scope.attachments = [];
				}
				
				$scope.selectedAttachments = $scope.attachments.concat();
				
				$scope.reloadData();
				
			};
			
			$scope.$watch('templates', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					$scope.template = nw[0];
				}
			});
			
			$scope.$watch('typeRecipient', function ( /*nw, old*/ ) {
				$scope.recipient = null;
			});
			
			$scope.$watch('newAttachment', function ( nw/*, old*/ ) {
				var attachment,
					i,
					l;
				
				if(nw) {
					for(i = 0, l = $scope.attachments.length; i < l; ++i) {
						attachment = $scope.attachments[i];
						if(attachment.id === nw.id) {
							return;
						}
					}
					
					$scope.attachments.push(nw);
					$scope.selectedAttachments.push(nw);
				}
				
				$scope.newAttachment = null;
			});
			
		}]);
	
})();