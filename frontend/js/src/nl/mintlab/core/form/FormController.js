/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.controller('nl.mintlab.core.form.FormController', [ '$scope', '$parse', '$interpolate', '$timeout', 'smartHttp', 'formService', function ( $scope, $parse, $interpolate, $timeout, smartHttp, formService ) {
			
			var forEach = _.forEach,
				indexOf = _.indexOf,
				difference = _.difference,
				safeApply = fetch('nl.mintlab.utils.safeApply'),
				promises = [],
				form;
				
			$scope.submitting = false;
			$scope.lastSaved = NaN;
				
			function init ( ) {
				var i,
					l,
					fields = $scope.zsForm.fields || [],
					field;
					
				setFieldData();
				
				$scope.$emit('form.prepare');
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					$scope.$watch(field.name, function ( nwVal, oldVal/*, scope*/ ) {
						if(angular.isArray(nwVal) && difference(oldVal, nwVal).length === 0) {
							return;
						}
						$scope.$emit('form.change', field);
					});
				}
				
				$timeout(function ( ) {
					$scope.$emit('form.ready');
				});
				
			}
				
			function watch ( promise ) {
				var obj = {
					promise: promise
				};
				
				obj.unwatch = $scope.$watch(promise.watch, function ( nw/*, old*/ ) {
					if(!promise.when || nw===$parse(promise.when)($scope)) {
						$parse(promise.then)($scope);
					}
				});
				
				promises.push(obj);
				
			}
			
			function unwatch ( promise ) {
				
				forEach(promises.concat(), function ( p ) {
					if(p.promise === promise) {
						p.unwatch();
						promises.splice(indexOf(p, 1));
					}
				});
			}
			
			function interpret ( val ) {
				var match = val.match(/^<\[(.*?)\]>$/);
				if(match) {
					return $parse(match[1])($scope);
				}
				
				return val.replace(/<\[(.*?)\]>/, function ( match, expr ) {
					var parsed = $parse(expr)($scope);
					return parsed;
				});
			}
			
			function setFieldData ( ) {
				var fields = $scope.zsForm.fields || [],
					field,
					i,
					l,
					val,
					parsed;
					
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					val = undefined;
					if(field.value !== undefined) {
						val = field.type === 'checkbox' ? !!field.value : field.value;
					} else {
						val = field['default'];
					}
					if(val !== undefined) {
						parsed = !angular.isObject(val) && val !== null && typeof val !== 'boolean' ? interpret(val) : val;
						form.setValue(field.name, parsed);
					} else {
						delete $scope[field.name];
					}
				}
			}
			
			function resolve ( field ) {
				var value = $scope[field.name];
				if(field.data && field.data.resolve) {
					switch(field.type) {
						default:
						value = resolveVal(value, field.data.resolve);
						break;
						
						case 'file':
						value = resolveArray(value, field.data.resolve);
						break;
					}
					
				}
				return value;
			}
			
			function resolveVal ( val, resolve ) {
				return $parse(resolve)(val);
			}
			
			function resolveArray ( val, resolve ) {
				var i,
					l,
					arr = [];
					
				if(!val || !angular.isArray(val)) {
					val = [];
				}
				
				for(i = 0, l = val.length; i < l; ++i) {
					arr.push(resolveVal(val[i], resolve));
				}
				return arr;
			}
			
			function getForm ( ) {
				return $scope[$scope.getName()];
			}
			
			function resetForm ( ) {
				var fields = $scope.zsForm.fields || [],
					field,
					i,
					l;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if(field['default'] !== undefined) {
						$scope[field.name] = field['default'];
					}
				}
			}
			
			function submitForm ( action ) {
				var url = action.data.url ? $interpolate(action.data.url)($scope) : action.data.url,
					data = {},
					fields = $scope.zsForm.fields || [],
					field,
					val,
					i,
					l;
					
				$scope.submitting = true;
				
				for(i = 0, l = fields.length; i < l; ++i) {
					field = fields[i];
					if($scope.showField(field)) {
						val = resolve(field);
						if(val === undefined) {
							val = null;
						}
						data[field.name] = val;
					}
				}
				
				return smartHttp.connect( {
					url: url,
					method: 'POST',
					data: data
				})
					.success(function onSuccess ( data ) {
						$scope.submitting = false;
						$scope.$emit('form.submit.success', action.name, data);
						
						if(action.data.success) {
							$parse(action.data.success)($scope);
						}
						
					})
					.error(function onError ( data ) {
						$scope.submitting = false;
						$scope.$emit('form.submit.error', action.name, data);
						validate(data);
						
						if(action.data.error) {
							$parse(action.data.error)($scope);
						}
						
					});
			}
			
			function validate ( data ) {
				var form = getForm(),
					statuses = data.result[0].data || [],
					status,
					field,
					control,
					messages = {},
					validityType,
					i,
					l;
					
				if(!form) {
					return;
				}
					
					
				for(i = 0, l = statuses.length; i < l; ++i) {
					status = statuses[i];
					switch(status.result) {
						default:
						validityType = '';
						break;
						
						case 'missing':
						validityType = 'required';
						break;
						
						case 'invalid':
						field = $scope.zsForm.fields[i];
						if(field && field.data && field.data.pattern) {
							validityType = 'pattern';
						} else {
							validityType = 'required';
						}
						break;
					}
					control = form[status.parameter];
					if(control) {
						control.$setValidity(validityType, false);
					}
					messages[status.parameter] = status.message;
				}
				
				for(i = 0, l = $scope.zsForm.fields.length; i < l; ++i) {
					field = $scope.zsForm.fields[i];
					field.statusMessage = messages[field.name];
				}
			}
			
			$scope.handleActionClick = function ( action/*, event*/ ) {
				switch(action.type) {
					case 'submit':
					submitForm(action);
					break;
					
					case 'reset':
					resetForm();
					break;
				}
				
			};
			
			$scope.isActionDisabled = function ( action ) {
				var form = getForm(),
					isDisabled,
					isDefined = (action.disabled !== undefined && action.disabled !== null);
					
				if(!form) {
					return;
				}
				
				if(isDefined) {
					isDisabled = $parse(action.disabled)($scope);
				} else if(action.type === 'submit') {
					isDisabled = !form.$valid;
				} else {
					isDisabled = false;
				}
				
				return isDisabled;
			};
			
			$scope.showField = function ( field ) {
				var when = field.when;
				return (when === undefined || when === null) || $parse(when)($scope);
			};
			
			$scope.revertField = function ( field ) {
				if(field['default'] !== null && field['default'] !== undefined) {
					$scope[field.name] = field['default'];
				}
			};
			
			$scope.getRequired = function ( field ) {
				var required = field.required,
					isRequired;
					
				if(typeof required === 'boolean' || typeof required === 'number') {
					isRequired = !!required;
				} else {
					isRequired = $parse(required)($scope);
				}
				return isRequired;
			};
			
			$scope.getFieldId = function ( field ) {
				return field.name;
			};
			
			$scope.getPlaceholder = function ( field ) {
				var placeholder = field.data ? field.data.placeholder : undefined;
				return placeholder !== undefined ? $parse(placeholder)($scope) : '';
			};
			
			$scope.getName = function ( ) {
				return $scope.zsForm.name;
			};
			
			$scope.getOptions = function ( field ) {
				var options = field.data.options.concat(),
					interpolated;
					
				if(typeof options === 'string') {
					try {
						options = JSON.parse($interpolate(options)($scope));
						interpolated = true;
					} catch ( error ) {
						options = [];
					}
				}
				
				if(!interpolated) {
					// causes a recursive loop if interpolated (??)
					field._empty = _.find(options, function ( option ) {
						return (option.value === '' || option.value === undefined || option.value === null);
					});
					if(field._empty) {
						options.splice(_.indexOf(options, field._empty), 1);
					}
				} else {
					field._empty = null;
				}
				
				return options;
			};
			
			$scope.isFormValid = function ( ) {
				return getForm() ? getForm().$valid : false;
			};
			
			$scope.isValid = function ( field ) {
				var form = getForm(),
					control = form ? form[field.name] : null;
				
				return control && control.$valid;
			};
			
			$scope.getValue = function ( name ) {
				return form.getValue(name);
			};
			
			$scope.setValue = function ( name, value ) {
				form.setValue(name, value);
			};
			
			$scope.getTrueValue = function ( field ) {
				return field.data && field.data.trueValue !== undefined ? interpret(field.data.trueValue) : true;
			};
			
			$scope.getFalseValue = function ( field ) {
				return field.data && field.data.falseValue !== undefined ? interpret(field.data.falseValue) : false;
			};
			
			$scope.isVisible = function ( action ) {
				var when = action.when;
				return (when === null || when === undefined) || $parse(when)($scope);
			};
			
			$scope.hasVisibleActions = function ( actions ) {
				var i,
					l;
					
				for(i = 0, l = actions ? actions.length : 0; i < l; ++i) {
					if($scope.isVisible(actions[i])) {
						return true;
					}
				}
				return false;
			};
			
			form = {
				getValue: function ( name ) {
					return $scope[name];
				},
				setValue: function ( name, value) {
					safeApply($scope, function ( ) {
						$scope[name] = value;
					});
				},
				getName: function ( ) {
					return $scope.getName();
				}
			};
			
			formService.register(form);
			
			// we can't use a dynamic ngModel value because
			// it's not interpolated (just parsed), so we
			// expose the scope object
			// see: https://github.com/angular/angular.js/issues/1404
			$scope.scope = $scope;
			
			$scope.$watch('zsForm.promises', function ( nw/*, old*/ ) {
				if(nw && nw.length) {
					forEach(nw, function ( value ) {
						watch(value);
					});
				}
				
			});
			
			$scope.$on('$destroy', function ( ) {
				if(form) {
					formService.unregister(form);
				}
			});
			
			$scope.$on('form.change.committed', function ( ) {
				safeApply($scope, function ( ) {
					var autosave = $scope.zsForm.options ? $scope.zsForm.options.autosave : false,
						actions = $scope.zsForm.actions || [],
						action,
						i;
					
					if(autosave && getForm().$valid) {
						for(i = actions.length-1; i >= 0; --i) {
							action = actions[i];
							if(action.type === 'submit') {
								submitForm(action).success(function ( /*response*/ ) {
									$scope.lastSaved = new Date().getTime();
								});
							}
						}
					}
				});
			});
			
			init();
			
		}]);
	
})();