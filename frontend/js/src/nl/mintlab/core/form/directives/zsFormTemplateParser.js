/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/form/form.html';
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormTemplateParser', [ '$q', '$timeout', 'smartHttp', 'templateCompiler', '$compile', function ( $q, $timeout, smartHttp, templateCompiler, $compile) {			
			return {
				scope: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var elSource,
							formName;
						
						function clearScope ( ) {
							
							var el = element[0],
								childNodes = el.childNodes,
								scopeChild = scope.$$childHead;
							
							while(childNodes.length) {
								el.removeChild(childNodes[0]);
							}
							
							while(scopeChild) {
								scopeChild.$destroy();
								scopeChild = scopeChild.$$nextSibling;
							}
							
							formName = '';
							
							scope.zsForm = {
								name: formName,
								options: {},
								fieldsets: [],
								fields: [],
								actions: [],
								promises: []
							};
							
						}
						
						function recompile ( ) {
							
							if(elSource) {
								var clone = elSource.clone();
								for(var i = 0, l = clone.length; i < l; ++i) {
									if(clone[i].tagName.toLowerCase() === 'form') {
										clone.eq(i).attr('name', formName);
										break;
									}
								}
								
								$compile(clone)(scope, function ( clonedElement ) {
									for(var i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[0]);
									}
								}); 
								
							}
						}
						
						function loadConfig ( url ) {
							
							if(!url) {
								clearScope();
								return;
							}
							
							smartHttp.connect({
								url: url,
								method: 'GET'
							})
								.success(function onSuccess ( data ) {
									clearScope();
									setConfig(data.result[0]);
									recompile();
								})
								.error(function onError ( ) {
									clearScope();
								});
						}
						
						function loadTemplate ( url ) {
							elSource = null;
							if(url) {
								templateCompiler.getElement(url).then(function ( element ) {
									elSource = element;
									recompile();
								});
							}
						}
						
						function setConfig ( config ) {
							var data = config.data,
								fields = [],
								i,
								l;
							
							for(i = 0, l = config.fieldsets ? config.fieldsets.length : 0; i < l; ++i) {
								if(config.fieldsets[i].fields) {
									fields = fields.concat(config.fieldsets[i].fields);
								}
							}
							
							formName = config.name;
							
							scope.zsForm.name = config.name;
							scope.zsForm.fieldsets = config.fieldsets || [];
							scope.zsForm.fields = fields || [];
							scope.zsForm.actions = config.actions || [];
							scope.zsForm.promises = config.promises || [];
							scope.zsForm.options = config.options || {};
														
							for(var key in data) {
								scope[key] = data[key];
							}
						}
						
						scope.getFormName = function ( ) {
							return formName;
						};
						
						attrs.$observe('zsFormTemplateParser', function ( ) {
							var obj;
							try {
								obj = JSON.parse(attrs.zsFormTemplateParser);
								clearScope();
								setConfig(obj);
								recompile();
							} catch ( e ) {
								loadConfig(attrs.zsFormTemplateParser);
							}
						});
						
						attrs.$observe('zsFormTemplateUrl', function ( ) {
							loadTemplate(attrs.zsFormTemplateUrl || TEMPLATE_URL);
						});
						
						clearScope();

					};
				}
				
			};
			
		}]);
	
})();