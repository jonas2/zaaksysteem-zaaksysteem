/*global angular,$,updateField,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsBagInput', [ '$timeout', function ( $timeout ) {
			
			var isEqual = _.isEqual;
			
			return {
				
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var modelId = scope.$eval(attrs.zsBagInput);
						
						scope.$watch(modelId, function ( nw, old ) {
							if(!isEqual(nw, old)) {
								$timeout(function ( ) {
									updateField($(element[0]), null, scope.$eval(attrs.zsBagInputName));
								}, 0);
							}
						}, true);
						
					};
				}
			};
			
		}]);
	
})();