/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.form')
		.directive('zsFormField', [ function ( ) {
			
			return {
				priority: 100,
				require: [ 'ngModel' ],
				compile: function ( ) {
					
					return function link ( scope, element, attrs, controllers ) {
						
						var ngModel = controllers[0];
						
						// attrs.$set('name', scope.field.name);
						// element.attr('name', scope.field.name);
						// ngModel.$name = scope.field.name;
						
						function commitChange ( ) {
							if(ngModel.$valid) {
								scope.$emit('form.change.committed', scope.field);
							}
						}
						
						if(element.attr('type') === 'text') {
							if(attrs.zsSpotEnlighter === undefined) {
								element.bind('focusout', commitChange);
							} else {
								ngModel.$viewChangeListeners.push(function ( ) {
									commitChange();
								});
							}
							element.bind('keyup', function ( event ) {
								if(event.keyCode === 13 || event.keyCode === 27) {
									commitChange();
								}
							});
						} else {
							ngModel.$viewChangeListeners.push(commitChange);
						}
						
					};
					
				}
			};
			
		}]);
	
})();