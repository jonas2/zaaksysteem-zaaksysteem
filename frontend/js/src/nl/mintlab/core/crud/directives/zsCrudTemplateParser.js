
/*global angular*/
(function ( ) {
	
	var TEMPLATE_URL = '/html/core/crud/crud-interface.html';
	
	angular.module('Zaaksysteem')
		.directive('zsCrudTemplateParser', [ '$compile', '$parse', '$q', '$timeout', '$interpolate', 'smartHttp', 'templateCompiler', 'formService', function ( $compile, $parse, $q, $timeout, $interpolate, smartHttp, templateCompiler, formService ) {
			
			return {
				scope: true,
				compile: function ( ) {
					
					return function link ( scope, element, attrs ) {
						
						var templateUrl = scope.$eval(attrs.zsCrudTemplateUrl) || TEMPLATE_URL,
							baseUrl,
							templateElement,
							config,
							sortBy = null,
							sortOrder = false;
							
						function attemptInit ( ) {
							if(!config || !templateElement) {
								return;
							}
							
							compileInterface();
						}
							
						function reloadConfig ( ) {
							
							if(!attrs.zsCrudTemplateParser) {
								setConfig(null);
								return;
							}
							
							var data;
							try {
								data = JSON.parse(attrs.zsCrudTemplateParser);
								setConfig(data);
							} catch ( error ) {
								var url = attrs.zsCrudTemplateParser;
								smartHttp.connect({
									url: url,
									method: 'GET',
									params: {
										zapi_crud: 1
									}
								})
									.success(function ( data ) {
										setConfig(data.result[0]);
										attemptInit();
									});
							}
						}
							
						function compileInterface ( ) {
							
							if(element.children().length) {
								return;
							}
								
							templateCompiler.getCompiler(templateUrl).then(function ( compiler ) {
								compiler(scope, function ( clonedElement ) {
									for(var i = 0, l = clonedElement.length; i < l; ++i) {
										element[0].appendChild(clonedElement[0]);
									}
								});
							});
							
						}
						
						function setUrl ( url ) {
							scope.baseUrl = baseUrl = url;
						}
						
						function setTemplateElement ( el ) {
							templateElement = el;
						}
						
						function setItems ( items ) {
							scope.items = items;
						}
						
						function setConfig ( cnfg ) {
							
							var baseUrl,
								i,
								l;
								
							config = cnfg;
							
							cnfg = cnfg || {};
							
							scope.name = attrs.zsCrudName || cnfg.name || scope.$id;
							scope.actions = cnfg.actions;
							scope.columns = cnfg.columns;
							scope.options = cnfg.options;
							scope.filters = cnfg.filters;
							scope.style = cnfg.style;
							scope.selectionType = 'subset';
							
							scope.numPages = 1;
							scope.currentPage = 1;
							scope.perPage = 10;
							scope.numRows = 0;
							
							for(i = 0, l = scope.filters ? scope.filters.length : 0; i < l; ++i) {
								scope.filters[i].name = scope.filters[i].name.replace(/\./g, '$dot$');
							}
							
							baseUrl = config ? (attrs.zsCrudBaseUrl ? attrs.zsCrudBaseUrl : $interpolate(cnfg.url || '')(scope)) : '';
							
							setItems([]);
							
							setUrl(baseUrl);
							
							scope.reloadData();
						}
						
						scope.sort = function ( id, reversed ) {
							sortBy = id;
							sortOrder = reversed ? 'desc' : 'asc';
							scope.reloadData();
						};
						
						scope.setSelectionType = function ( selectionType ) {
							scope.selectionType = selectionType;
						};
						
						scope.getFilterValues = function ( ) {
							var vals = {},
								form = scope.getForm(),
								filters = scope.filters,
								i,
								l,
								val,
								filter;
								
							if(!form || !filters) {
								return vals;
							}
								
							for(i = 0, l = filters.length; i < l; ++i) {
								filter = filters[i];
								val = form.getValue(filter.name);
								if(val === undefined) {
									continue;
								}
								switch(filter.type) {
									case 'checkbox':
									val = !!val ? 1 : 0;
									break;
								}
								vals[filter.name.replace(/\$dot\$/g, '.')] = val;
							}
							
							return vals;
						};
						
						scope.reloadData = function ( ) {
							var params = {
									zapi_page: scope.currentPage
								},
								filters = scope.filters,
								form = scope.getForm(),
								vals = scope.getFilterValues();
								
							if(!form && (filters && filters.length)) {
								return;
							}
							
							for(var key in vals) {
								params[key] = vals[key];
							}
							
							if(sortBy) {
								params.zapi_order_by = sortBy.indexOf('.') !== -1 ? sortBy : ('me.' + sortBy);
								params.zapi_order_by_direction = sortOrder;
							}
							
							if(baseUrl) {
								smartHttp.connect({
									url: baseUrl,
									method: 'GET',
									params: params
								})
									.success(function ( data ) {
										setItems(data.result);
										scope.numPages = Math.ceil(data.num_rows/scope.perPage);
										scope.numRows = data.num_rows;
										if(scope.currentPage > scope.numPages) {
											scope.currentPage = scope.numPages || 1;
										}
									})
									.error(function ( data ) {
										console.log('Encountered error while collecting item data', data);
										setItems([]);
									});
							} else {
								setItems([]);
							}
						};
						
						scope.getForm = function ( ) {
							var formName = 'crud-filters-' + scope.name;
							return formService.get(formName);	
						};
						
						scope.getFormConfig = function ( ) {
							var filters = scope.filters || [],
								form;
								
							if(filters.length) {
								form = {
									name: 'crud-filters-' + scope.name,
									fieldsets: [
										{
											fields: filters
										}
									]
								};
							}
							
							return form;
						};
						
						scope.$on('form.ready', function ( event ) {
							if(event.targetScope.getFormName() === ('crud-filters-' + scope.name)) {
								scope.reloadData();
								scope.$on('form.change', function ( /*event, field*/ ) {
									scope.reloadData();
								});
							}
						});
						
						scope.$on('page.change', function ( event, page ) {
							scope.currentPage = page;
							scope.reloadData();
						});
						
						attrs.$observe('zsCrudTemplateParser', function ( ) {
							reloadConfig();
						});
						
						templateCompiler.getElement(templateUrl).then(function ( element ) {
							setTemplateElement(element);
							attemptInit();
						});
						
					};
				}
				
			};
			
		}]);
	
})();