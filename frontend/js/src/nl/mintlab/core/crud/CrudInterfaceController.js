/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.controller('nl.mintlab.core.crud.CrudInterfaceController', [ '$scope', '$parse', '$interpolate', 'smartHttp', 'translationService', '$window', function ( $scope, $parse, $interpolate, smartHttp, translationService, $window ) {
			
			var indexOf = _.indexOf,
				forEach = _.forEach;
			
			$scope.selectedItems = [];
			
			function applySelected ( selected ) {
				$scope.selectedItems = selected;
			}
			
			function getItemId ( item ) {
				return item.id;
			}
			
			function getItemById ( id ) {
				var items = $scope.items || [],
					i,
					l,
					item;
					
				for(i = 0, l = items.length; i < l; ++i) {
					item = items[i];
					if(getItemId(item) === id) {
						return item;
					}
				}
				
				return null;
			}
			
			$scope.isAllSelected = function ( ) {
				return $scope.selectedItems.length && $scope.items && $scope.selectedItems.length === $scope.items.length;
			};
			
			$scope.isSelected = function ( item ) {
				return indexOf($scope.selectedItems, item) !== -1;
			};
			
			$scope.handleSelectAllClick = function ( /*event*/ ) {
				if($scope.isAllSelected()) {
					$scope.deselectAll();
				} else {
					$scope.selectAll();
				}
			};
			
			$scope.selectAll = function ( ) {
				var items = $scope.items || [];
				
				$scope.selectedItems.length = 0;
				$scope.selectedItems.push.apply($scope.selectedItems, items);
			};
			
			$scope.deselectAll = function ( ) {
				$scope.selectedItems.length = 0;	
			};
			
			$scope.deselectItem = function ( item ) {
				var index = indexOf($scope.selectedItems, item);
				if(index !== -1) {
					$scope.selectedItems.splice(index, 1);
				}
			};
			
			$scope.deleteItems = function ( ) {
				var selected = $scope.selectedItems.concat();
				forEach(selected, function ( item/*, key*/ ) {
					$scope.deselectItem(item);
					$scope.items.splice(indexOf($scope.items, item));
				});
			};
			
			$scope.hasVisibleActions = function ( ) {
				var actions = $scope.actions,
					action,
					visible;
					
				if(!$scope.selectedItems.length) {
					return false;
				}
				
				for(var i = 0, l = actions.length; i < l; ++i) {
					action = actions[i];
					visible = action.when === null || action.when === undefined || $parse(action.when)($scope);
					if(visible) {
						return true;
					}
				}
				
				return false;
			};
			
			$scope.handleActionClick = function ( action/*, $event*/ ) {
				
				var params = {
					selection_type: $scope.selectionType
					},
					ids = [],
					i,
					l,
					items = $scope.selectedItems.concat(),
					item,
					filterVals = $scope.getFilterValues();
							
				for(i = 0, l = items.length; i < l; ++i) {
					item = items[i];
					ids.push(getItemId(item));
				}
				
				for(var key in filterVals) {
					params[key] = filterVals[key];
				}
				
				params.selection_id = ids;
				
				switch(action.type) {
					default:
					throw new Error('Action type ' + action.type + ' not implemented for CrudInterfaceController');
					
					case 'download':
					$window.location = action.data.url;
					break;
					
					case 'update':
					smartHttp.connect({
						method: 'POST',
						url: action.data.url,
						data: params
					})
						.success(function ( data ) {
							var updated = data.result,
								i,
								l,
								update,
								item;
								
							for(i = 0, l = updated.length; i < l; ++i) {
								update = updated[i];
								item = getItemById(getItemId(update));
								if(item) {
									for(var key in update) {
										item[key] = update[key];
									}
								}
							}
							
						})
						.error(function ( ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Er ging iets fout bij het wijzigen van de geselecteerde items')
							});
							
						});
					break;
					
					case 'delete':
					while(items.length) {
						var index;
						item = items.shift();
						index = indexOf($scope.selectedItems, item);
						if(index !== -1) {
							$scope.selectedItems.splice(index, 1);
						}
						index = indexOf($scope.items, item);
						if(index !== -1) {
							$scope.items.splice(index, 1);
						}
					}
					
					smartHttp.connect({
						method: 'POST',
						url: action.data.url,
						data: params
					})
						.success(function ( /*data*/ ) {
							$scope.$emit('systemMessage', {
								type: 'info',
								content: translationService.get('Items succesvol verwijderd')
							});
							
							$scope.reloadData();
							
						})
						.error(function ( /*data*/ ) {
							$scope.$emit('systemMessage', {
								type: 'error',
								content: translationService.get('Niet alle items konden worden verwijderd')
							});
							
							$scope.reloadData();
							
						});
					break;
				}
			};
			
			$scope.getItemStyle = function ( item ) {
				var obj = {},
					classes;
				if($scope.style && $scope.style.classes) {
					classes = $scope.style.classes;
					for(var key in classes) {
						obj[key] = !!$parse(classes[key])(item);
					}
				}
				return obj;
			};
			
			$scope.handleTableRowClick = function ( item, event ) {
				var url;
				$scope.$emit('crud.item.click', item);
				if($scope.options && $scope.options.link) {
					url = $interpolate($scope.options.link)(item);
					$window.location = url;
				}
				event.stopPropagation();
			};
			
			$scope.$watch('selectedItems', function ( ) {
				if($scope.selectionType === 'all' && !$scope.isAllSelected()) {
					$scope.setSelectionType('subset');
				}
			}, true);
			
			$scope.$on('zsSelectableList:change', function ( event, selected ) {
				if(!$scope.$$phase && !$scope.$root.$$phase) {
					$scope.$apply(function ( ) {
						applySelected(selected);
					});
				} else {
					applySelected(selected);
				}
			});
			
			$scope.$watch('items', function ( ) {
				$scope.selectedItems.length = 0;
			});
			
		}]);
	
})();