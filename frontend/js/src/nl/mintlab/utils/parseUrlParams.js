/*global define*/
(function ( ) {
	
	define('nl.mintlab.utils.parseUrlParams', function ( ) {
		
		return function ( url ) {
			var params = {},
				qs = url.split("+").join(" "),
				tokens,
				re = /[?&]?([^=]+)=([^&]*)/g;

			while ((tokens = re.exec(qs))) {
				params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
			}

			return params;
		};
		
	});
	
})();