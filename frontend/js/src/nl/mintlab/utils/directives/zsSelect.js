/*global angular,$*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsSelect', [ function ( ) {
			
			return {
				link: function ( scope, element, attrs ) {
					
					var menu;
					
					function onChange ( event, object ) {
						var options = element.find('option'),
							val = object.value;
						
						angular.forEach(options, function ( opt ) {
							opt = angular.element(opt);
							if(opt.val() === val) {
								opt.attr('selected', 'selected');
							} else {
								opt.removeAttr('selected');
							}
						});
						
						element.triggerHandler('change');
					}
					
					scope.$watch('ngOptions', function ( /*old, nw, scope*/ ) {
						if(menu) {
							menu.$destroy();
						}
						menu = $(element[0]).selectmenu( {
							style: 'dropdown',
							//width: 100,
							//menuWidth: 150,
							change: onChange
						} );
						
					});
					
				}
			};
			
		}]);
	
})();