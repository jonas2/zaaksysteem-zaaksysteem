/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopupMenu', [ '$document', function ( $document ) {
			
			var contains = fetch('nl.mintlab.utils.dom.contains'),
				getViewportSize = fetch('nl.mintlab.utils.dom.getViewportSize'),
				fromGlobalToLocal = fetch('nl.mintlab.utils.dom.fromGlobalToLocal'),
				fromLocalToGlobal = fetch('nl.mintlab.utils.dom.fromLocalToGlobal'),
				cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				scope: false,
				compile: function ( ) {
					
					return function link ( scope, element/*, attrs*/ ) {
						
						var isOpen = false,
							button = element.find('button');
						
						function onDocumentClick ( event ) {
							var clickTarget = event.target;
							
							if(!(contains(element[0], clickTarget))) {
								closeMenu();
							}
						}
						
						function positionMenu ( ) {
							var menu = element.find('ul'),
								width = menu[0].clientWidth,
								height = menu[0].clientHeight,
								viewportSize = getViewportSize(),
								x = 0,
								y = 0,
								parent = angular.element(menu[0].offsetParent),
								p = fromLocalToGlobal(parent[0], { x: 0, y: 0 }),
								parentBounds = { x: p.x, y: p.y, height: parent[0].clientHeight, width: parent[0].clientWidth };
								
							if(parentBounds.x + parentBounds.width + width > viewportSize.width) {
								x = parentBounds.x + parentBounds.width - width;
								parent.attr('data-zs-popup-menu-horizontal-orient', 'left');
							} else {
								x = parentBounds.x;
								parent.attr('data-zs-popup-menu-horizontal-orient', 'right');
							}
							
							if(parentBounds.y + parentBounds.height + height > viewportSize.height) {
								y = parentBounds.y + parentBounds.height - height;
								parent.attr('data-zs-popup-menu-vertical-orient', 'top');
							} else {
								y = parentBounds.y + parentBounds.height;
								parent.attr('data-zs-popup-menu-vertical-orient', 'bottom');
							}
							
							p = fromGlobalToLocal(parent[0], { x: x, y: y });
							
							// menu.css('top', p.y + 'px');
							// menu.css('left', p.x + 'px');
						}
						
						function openMenu ( ) {
							
							var menu = element.find('ul');
							
							if(isOpen) {
								return;
							}
							
							isOpen = true;
							
							menu.css('visibility', 'hidden');
							menu.addClass('popup-menu-open');
							button.addClass('zs-popup-menu-button-active');
							
							positionMenu();
							menu.css('visibility', 'visible');
							
							$document.bind('click', onDocumentClick);
							
						}
						
						function closeMenu ( ) {
							
							var menu = element.find('ul');
							
							if(!isOpen) {
								return;
							}
							
							isOpen = false;
							
							button.removeClass('zs-popup-menu-button-active');
							menu.removeClass('popup-menu-open');
							
							$document.unbind('click', onDocumentClick);
						}
						
						button.bind('click', function ( evt ) {
							if(isOpen) {
								closeMenu();
							} else {
								openMenu();
							}
							
							cancelEvent(evt);
						});
						
						button.addClass('zs-popup-menu-button');
						
						closeMenu();
						
					};
					
				}
			};
		}]);
	
})();