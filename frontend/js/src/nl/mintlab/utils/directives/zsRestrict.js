/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsRestrict', [ function ( ) {
			
			return {
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						var restrict = scope.$eval(attrs.zsRestrict);
						console.log(restrict);
						
					};
					
				}
			};
			
		}]);
	
})();