/*global angular,console*/
/* parse date in format YYYYMMDD to convert it into something Date()/Angular understands */
(function () {
    "use strict";
    angular.module('Zaaksysteem')
        .filter('dateParse', function () {

            function parse(str) {
                if (!/^(\d){8}$/.test(str)) {
                    return "-";
                }
                var y = str.substr(0, 4),
                    m = str.substr(4, 2),
                    d = str.substr(6, 2);
                return new Date(y, m, d).getTime();
            }

            return function (source) {
                if (typeof source === 'undefined') {
                    return;
                }
                return parse(source);
            };
        });
}());