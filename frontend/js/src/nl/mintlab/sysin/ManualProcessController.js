/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin')
		.controller('nl.mintlab.sysin.ManualProcessController', [ '$scope', 'translationService', function ( $scope, translationService ) {
			
			$scope.getManualUploadOptions = function ( ) {
				
				var options = [],
					interfaces = $scope.interfaces || [],
					item,
					i,
					l;
					
				for(i = 0, l = interfaces.length; i < l; ++i) {
					item = interfaces[i];
					options.push({
						name: item.name,
						value: item.id,
						label: item.name
					});
				}
				
				return options;
			};
			
			$scope.allowUploadType = function ( interfaceId, type ) {
				var interfaces = $scope.interfaces,
					intFace,
					i,
					l,
					module,
					isAllowed = false;
					
				for(i = 0, l = interfaces.length; i < l; ++i) {
					if(interfaces[i].id === interfaceId) {
						intFace = interfaces[i];
						break;
					}
				}
				
				if(intFace) {
					module = $scope.getModuleByName(intFace.module);
					if(module) {
						isAllowed = (module.manual_type && _.indexOf(module.manual_type, type) !== -1);
					}
				}
				return isAllowed;
			};
			
			$scope.getSubmitUrl = function ( interfaceId ) {
				return '/sysin/interface/' + interfaceId + '/manual_process';
			};
			
			$scope.getDefaultInterfaceId = function ( ) {
				return $scope.link ? $scope.link.id : null;	
			};
			
			$scope.$on('form.submit.success', function ( event, name, data ) {
				var transactionId;
				if(event.targetScope.getFormName() === 'manual-process') {
					transactionId = data.result[0].id;
					
					$scope.$emit('systemMessage', {
						type: 'info',
						content: "<a href='/beheer/sysin/transactions/" + transactionId + "'>" + translationService.get('Transactie verwerkt') + "</a>"
					});
				}
			});
			
			$scope.$on('form.submit.error', function ( event, name, data ) {
				var errorMessage,
					error = data.result ? data.result[0] : null;
				
				if(error && error.type === 'sysin/modules/process/inactive_module') {
					errorMessage = 'Deze module is niet actief';
				} else {
					errorMessage = translationService.get('Er ging iets fout bij het verwerken van de transactie');
				}
				
				$scope.$emit('systemMessage', {
					type: 'error',
					content: errorMessage
				});
			});
			
		}]);
	
})();