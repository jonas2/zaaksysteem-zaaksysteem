package Zaaksysteem::Schema::ChecklistMogelijkheden;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("checklist_mogelijkheden");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('checklist_mogelijkheden_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "vraag_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "mogelijkheid_type",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "label",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "input_constraint",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "option_order",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("checklist_mogelijkheden_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "vraag_id",
  "Zaaksysteem::Schema::ChecklistVraag",
  { id => "vraag_id" },
);
__PACKAGE__->has_many(
  "checklist_vraags",
  "Zaaksysteem::Schema::ChecklistVraag",
  { "foreign.depends_on_option" => "self.id" },
);
__PACKAGE__->has_many(
  "zaaktype_checklist_vraags",
  "Zaaksysteem::Schema::ZaaktypeChecklistVraag",
  { "foreign.depends_on_option" => "self.id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:BCqo4MurGCce+uInNWc91w


# You can replace this text with custom content, and it will be preserved on regeneration
1;
