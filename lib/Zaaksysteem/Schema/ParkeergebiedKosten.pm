package Zaaksysteem::Schema::ParkeergebiedKosten;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ParkeergebiedKosten

=cut

__PACKAGE__->table("parkeergebied_kosten");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'parkeergebied_kosten_id_seq'

=head2 betrokkene_type

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 parkeergebied

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 parkeergebied_id

  data_type: 'integer'
  is_nullable: 1

=head2 aanvraag_soort

  data_type: 'integer'
  is_nullable: 1

=head2 geldigheid

  data_type: 'integer'
  is_nullable: 1

=head2 prijs

  data_type: 'real'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "parkeergebied_kosten_id_seq",
  },
  "betrokkene_type",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "parkeergebied",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "parkeergebied_id",
  { data_type => "integer", is_nullable => 1 },
  "aanvraag_soort",
  { data_type => "integer", is_nullable => 1 },
  "geldigheid",
  { data_type => "integer", is_nullable => 1 },
  "prijs",
  { data_type => "real", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:44:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:N33E2yfmjYXXvrwN0jtdeA





# You can replace this text with custom content, and it will be preserved on regeneration
1;
