package Zaaksysteem::Schema::ObjectSubscription;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::ObjectSubscription

=cut

__PACKAGE__->table("object_subscription");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'object_subscription_id_seq'

=head2 interface_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 external_id

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 local_table

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 local_id

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 1
  original: {default_value => \"now()"}

=head2 date_deleted

  data_type: 'timestamp'
  is_nullable: 1

=head2 object_preview

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "object_subscription_id_seq",
  },
  "interface_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "external_id",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "local_table",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "local_id",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 1,
    original      => { default_value => \"now()" },
  },
  "date_deleted",
  { data_type => "timestamp", is_nullable => 1 },
  "object_preview",
  { data_type => "text", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 interface_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Interface>

=cut

__PACKAGE__->belongs_to(
  "interface_id",
  "Zaaksysteem::Schema::Interface",
  { id => "interface_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-07-31 12:10:19
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2jqMb/iJpCZkaId1mRHaRg

__PACKAGE__->resultset_class('Zaaksysteem::Backend::ObjectSubscription::ResultSet');

__PACKAGE__->load_components(qw/
    +Zaaksysteem::Backend::ObjectSubscription::Component
    +DBIx::Class::Helper::Row::ToJSON
/);


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
