package Zaaksysteem::Schema::ZaaktypeChecklistMogelijkheden;

use strict;
use warnings;

use base 'DBIx::Class';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "Core");
__PACKAGE__->table("zaaktype_checklist_mogelijkheden");
__PACKAGE__->add_columns(
  "id",
  {
    data_type => "integer",
    default_value => "nextval('zaaktype_checklist_mogelijkheden_id_seq'::regclass)",
    is_nullable => 0,
    size => 4,
  },
  "zaaktype_checklist_vraag_id",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
  "mogelijkheid_type",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "label",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "input_constraint",
  {
    data_type => "text",
    default_value => undef,
    is_nullable => 1,
    size => undef,
  },
  "option_order",
  { data_type => "integer", default_value => undef, is_nullable => 1, size => 4 },
);
__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint("zaaktype_checklist_mogelijkheden_pkey", ["id"]);
__PACKAGE__->belongs_to(
  "zaaktype_checklist_vraag_id",
  "Zaaksysteem::Schema::ZaaktypeChecklistVraag",
  { id => "zaaktype_checklist_vraag_id" },
);


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2012-05-16 12:12:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WOj4YkHFGym3WER7Y1c/lw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
