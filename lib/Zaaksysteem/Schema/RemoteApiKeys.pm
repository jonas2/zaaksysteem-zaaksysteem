package Zaaksysteem::Schema::RemoteApiKeys;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::RemoteApiKeys

=cut

__PACKAGE__->table("remote_api_keys");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'remote_api_keys_id_seq'

=head2 key

  data_type: 'varchar'
  is_nullable: 0
  size: 60

=head2 permissions

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "remote_api_keys_id_seq",
  },
  "key",
  { data_type => "varchar", is_nullable => 0, size => 60 },
  "permissions",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-06-18 10:16:14
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:p8nKpsSG39UMdJQi5ChFKA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
