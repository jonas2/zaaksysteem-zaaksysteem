package Zaaksysteem::Schema::Seen;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::Seen

=cut

__PACKAGE__->table("seen");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'seen_id_seq'

=head2 component

  data_type: 'text'
  is_nullable: 1

=head2 kennisbank_producten_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 kennisbank_vragen_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 betrokkene_dsn

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "seen_id_seq",
  },
  "component",
  { data_type => "text", is_nullable => 1 },
  "kennisbank_producten_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "kennisbank_vragen_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "betrokkene_dsn",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 kennisbank_producten_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::KennisbankProducten>

=cut

__PACKAGE__->belongs_to(
  "kennisbank_producten_id",
  "Zaaksysteem::Schema::KennisbankProducten",
  { id => "kennisbank_producten_id" },
);

=head2 kennisbank_vragen_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::KennisbankVragen>

=cut

__PACKAGE__->belongs_to(
  "kennisbank_vragen_id",
  "Zaaksysteem::Schema::KennisbankVragen",
  { id => "kennisbank_vragen_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-01-10 07:44:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gFS8V70RzCkuAJrqUA7pig

__PACKAGE__->add_columns('created',
    { %{ __PACKAGE__->column_info('created') },
    set_on_create => 1,
});




# You can replace this text with custom content, and it will be preserved on regeneration
1;
