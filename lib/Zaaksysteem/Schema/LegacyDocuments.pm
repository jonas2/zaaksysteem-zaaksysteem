package Zaaksysteem::Schema::LegacyDocuments;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

=head1 NAME

Zaaksysteem::Schema::LegacyDocuments

=cut

__PACKAGE__->table("legacy_documents");

=head1 ACCESSORS

=head2 search_index

  data_type: 'tsvector'
  is_nullable: 1

=head2 search_term

  data_type: 'text'
  is_nullable: 1

=head2 object_type

  data_type: 'varchar'
  default_value: 'documents'
  is_nullable: 1
  size: 100

=head2 searchable_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'searchable_searchable_id_seq'

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'legacy_documents_id_seq'

=head2 pid

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_id

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene

  data_type: 'text'
  is_nullable: 1

=head2 description

  data_type: 'text'
  is_nullable: 1

=head2 filename

  data_type: 'text'
  is_nullable: 1

=head2 filesize

  data_type: 'integer'
  is_nullable: 1

=head2 mimetype

  data_type: 'text'
  is_nullable: 1

=head2 documenttype

  data_type: 'text'
  is_nullable: 1

=head2 category

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 post_registratie

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 verplicht

  data_type: 'integer'
  is_nullable: 1

=head2 catalogus

  data_type: 'integer'
  is_nullable: 1

=head2 zaakstatus

  data_type: 'integer'
  is_nullable: 1

=head2 betrokkene_id

  data_type: 'text'
  is_nullable: 1

=head2 ontvangstdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 dagtekeningdatum

  data_type: 'timestamp'
  is_nullable: 1

=head2 versie

  data_type: 'integer'
  is_nullable: 1

=head2 help

  data_type: 'text'
  is_nullable: 1

=head2 pip

  data_type: 'boolean'
  is_nullable: 1

=head2 private

  data_type: 'boolean'
  is_nullable: 1

=head2 md5

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1

=head2 deleted_on

  data_type: 'timestamp'
  is_nullable: 1

=head2 option_order

  data_type: 'integer'
  is_nullable: 1

=head2 zaaktype_kenmerken_id

  data_type: 'integer'
  is_nullable: 1

=head2 queue

  data_type: 'integer'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 public

  data_type: 'boolean'
  is_nullable: 1

=head2 new_file_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "search_index",
  { data_type => "tsvector", is_nullable => 1 },
  "search_term",
  { data_type => "text", is_nullable => 1 },
  "object_type",
  {
    data_type => "varchar",
    default_value => "documents",
    is_nullable => 1,
    size => 100,
  },
  "searchable_id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "searchable_searchable_id_seq",
  },
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "legacy_documents_id_seq",
  },
  "pid",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_id",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene",
  { data_type => "text", is_nullable => 1 },
  "description",
  { data_type => "text", is_nullable => 1 },
  "filename",
  { data_type => "text", is_nullable => 1 },
  "filesize",
  { data_type => "integer", is_nullable => 1 },
  "mimetype",
  { data_type => "text", is_nullable => 1 },
  "documenttype",
  { data_type => "text", is_nullable => 1 },
  "category",
  { data_type => "text", is_nullable => 1 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "post_registratie",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "verplicht",
  { data_type => "integer", is_nullable => 1 },
  "catalogus",
  { data_type => "integer", is_nullable => 1 },
  "zaakstatus",
  { data_type => "integer", is_nullable => 1 },
  "betrokkene_id",
  { data_type => "text", is_nullable => 1 },
  "ontvangstdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "dagtekeningdatum",
  { data_type => "timestamp", is_nullable => 1 },
  "versie",
  { data_type => "integer", is_nullable => 1 },
  "help",
  { data_type => "text", is_nullable => 1 },
  "pip",
  { data_type => "boolean", is_nullable => 1 },
  "private",
  { data_type => "boolean", is_nullable => 1 },
  "md5",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1 },
  "deleted_on",
  { data_type => "timestamp", is_nullable => 1 },
  "option_order",
  { data_type => "integer", is_nullable => 1 },
  "zaaktype_kenmerken_id",
  { data_type => "integer", is_nullable => 1 },
  "queue",
  { data_type => "integer", is_nullable => 1 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "public",
  { data_type => "boolean", is_nullable => 1 },
  "new_file_id",
  { data_type => "integer", is_nullable => 1 },
);
__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 pid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::LegacyDocuments>

=cut

__PACKAGE__->belongs_to("pid", "Zaaksysteem::Schema::LegacyDocuments", { id => "pid" });

=head2 legacy_documents

Type: has_many

Related object: L<Zaaksysteem::Schema::LegacyDocuments>

=cut

__PACKAGE__->has_many(
  "legacy_documents",
  "Zaaksysteem::Schema::LegacyDocuments",
  { "foreign.pid" => "self.id" },
  {},
);

=head2 legacy_documents_mails

Type: has_many

Related object: L<Zaaksysteem::Schema::LegacyDocumentsMail>

=cut

__PACKAGE__->has_many(
  "legacy_documents_mails",
  "Zaaksysteem::Schema::LegacyDocumentsMail",
  { "foreign.document_id" => "self.id" },
  {},
);


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2013-05-24 14:24:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:bE96wB7hTd7lLTqYCV1lrA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
