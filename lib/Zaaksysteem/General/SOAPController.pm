{   package Zaaksysteem::General::SOAPController;

    use strict;
    use base 'Catalyst::Controller::SOAP';

    sub _parse_SOAP_attr {
        my $self        = shift;

        my %actionattrs = $self->next::method(@_);

        ### ActionClass code
        if ($actionattrs{ActionClass} !~ /^Catalyst/) {
            $actionattrs{ActionClass} = 'Catalyst::Action::'
                . $actionattrs{ActionClass};
        }

        return %actionattrs;

    }

}

1;

=head1 NAME

Zaaksysteem::General::SOAPController -  Wrapper around
                                        Catalyst::Controller::SOAP

=head1 DESCRIPTION

The only purpose of this module is making sure the ActionClass is properly
prefixed. Somehow, this has changed in a future version of Catalyst.

Remove the ActionClass code when we reach "the" future version of Catalyst

=cut