package Zaaksysteem::General::ZaakResultPermissions;

use strict;
use warnings;

use Data::Dumper;

use Scalar::Util qw/blessed/;
use Net::LDAP;

use Zaaksysteem::Constants qw/
    ZAAKSYSTEEM_AUTHORIZATION_ROLES
    ZAAKSYSTEEM_AUTHORIZATION_PERMISSIONS
    ZAAKSYSTEEM_CONSTANTS
/;


sub check_zaak_result_permission {
    my ($c, @check_permissions) = @_;

 #  $c->log->debug(
 #      '$c->check_any_zaak_permission: check for zaak and logged in user,'
 #       . ' caller: ' . [caller(1)]->[3] . ' / ' . [caller(2)]->[3]
 #   );

    ### Zaak exists?
    return unless $c->stash->{zaak};

    ### User exists?
    return unless $c->user_exists;

#    $c->log->debug(
#        '$c->check_any_zaak_permission: zaak found and user logged in'
#    );

    ### First, check if user is allowed to view zaken
    unless (
        $c->check_any_user_permission(
            'gebruiker'
        )
    ) {
        $c->push_flash_message('Geen toegang: u heeft geen rechten'
            .' om zaken te bekijken [!gebruiker]'
        );

        return;
    }

    ### Admins have special rightos
#    $c->log->debug(
#        '$c->check_any_zaak_permission: checking for admin'
#    );

    return 1 if $c->check_any_user_permission('admin');

    ### Info about betrokkene
    my $orgeenheid;
    {
        my $bid     = $c->user->uidnumber or return;
#        $c->log->debug(
#            '$c->check_any_zaak_permission: checking for'
#            .' betrokkene medewerker'
#        );

        my $bo      = $c->model('Betrokkene')->get(
            {
                extern  => 1,
                type    => 'medewerker',
            },
            $bid
        );

        $orgeenheid = $bo->org_eenheid or return;
    }

    ### Loop over roles in zaaktype information
    my $authroles = $c->stash->{zaak}->zaaktype_id->zaaktype_authorisaties->search;

    ### Damn system not giving me role ids, we have to find out ourself
    ### Retrieve all ldaproles
    my $ldaproles = $c->model('Groups')->search;

    #$c->log->debug('Authrole: ldaproles: ' . Dumper($ldaproles));

    ### Strip those from ldaproles which user does not have
    my @userldaproles = $c->user->roles;

#    $c->log->debug('Authrole: userroles: ' . Dumper(\@userldaproles));

#    $c->log->debug(
#        '$c->check_any_zaak_permission: checking zaaktype permissions'
#        . ' for permissions: ' . join(',', @check_permissions)
#    );
    my %hasldaproles;
    for my $ldaprole (@{ $ldaproles }) {
        if (grep {
                $ldaprole->{short_name} eq $_
            } @userldaproles
        ) {
            $hasldaproles{$ldaprole->{id}} = 1;
        }
    }

#    $c->log->debug('Authrole: hasldaproles: ' . Dumper(\%hasldaproles));

    while (my $authrole = $authroles->next) {
        ### Ou id not same as org_eenheid id from this user?
        if ($authrole->ou_id && $orgeenheid->ldapid ne $authrole->ou_id) {
            next;
        }
#        $c->log->debug(
#            '$c->check_any_zaak_permission: check permission '
#            . $authrole->recht
#        );

        if ($hasldaproles{$authrole->role_id}) {
            if (grep {
                    $_ eq $authrole->recht
                } @check_permissions
            ) {
#                $c->log->debug(
#                    '$c->check_any_zaak_permission: found permission'
#                    . $authrole->recht
#                );
                return 1;
            }
        }
    }


    return;
}

1;

