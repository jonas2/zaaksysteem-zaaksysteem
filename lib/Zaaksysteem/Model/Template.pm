package Zaaksysteem::Model::Template;

use base 'Exporter';

use Zaaksysteem::Model::Template::ODF;
use Zaaksysteem::Model::Template::Plaintext;

our @EXPORT = qw[instantiate_template];

sub instantiate_template {
    my $thing = shift;

    if(eval { $thing->isa('OpenOffice::OODoc::Document') }) {
        return Zaaksysteem::Model::Template::ODF->new($thing);
    }

    return Zaaksysteem::Model::Template::Plaintext->new($thing);
}

1;
