package Zaaksysteem::SBUS::Dispatcher::Soap::TestTransport;

use Moose;
use XML::Tidy;

use Zaaksysteem::SBUS::Dispatcher::Soap::TestTransportConstants;

has [qw/log xml response_xml berichtsoort entiteit/]   => (
    'is'        => 'rw',
);

sub BUILD {
    my $self    = shift;

    if (ref($self->xml)) {
        my $tidy    = XML::Tidy->new(xml => $self->xml->content);
        my $xml     = $tidy->tidy()->toString();

        $self->xml($xml);
    }
}

sub process {
    my $self            = shift;

    my ($entiteit)      = $self->xml =~ /entiteittype>(.*?)</;
    my ($berichtsoort)  = $self->xml =~ /berichtsoort>(.*?)</;

    $self->entiteit(lc($entiteit));
    $self->berichtsoort(lc($berichtsoort));

    $self->log->debug(
        'TestTransport: Processing entiteit: ' . $self->entiteit
        . ' / soort: ' . $self->berichtsoort
    );

    my $processor   = $self->can('_process_' .  $self->entiteit);
    die('entity not supported')
        unless $processor;

    $processor->($self);
}

sub response {
    my $self    = shift;

    return HTTP::Response->new(
        200,
        'Constant',
        [ 'Content-Type' => 'text/xml' ],
        $self->response_xml
    );
}

sub _process_prs {
    my $self    = shift;

    if ($self->berichtsoort eq 'lv02') {
        $self->response_xml(
            TEST_XML->{ $self->entiteit }->{ 'bv01' }->{default}
        );
    }

    if ($self->berichtsoort eq 'lv01') {
        if ($self->xml =~ /BG:PRS.*?sleutelGegevensbeheer/s) {
            $self->response_xml(
                TEST_XML->{ $self->entiteit }->{ 'la01' }->{single}
            );
        } else {
            $self->response_xml(
                TEST_XML->{ $self->entiteit }->{ 'la01' }->{multi}
            );
        }
    }

    if ($self->berichtsoort eq 'lk01') {
        $self->response_xml(
            TEST_XML->{ $self->entiteit }->{ 'bv01' }->{default}
        );
    }
}

1;
