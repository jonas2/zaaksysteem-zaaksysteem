package Zaaksysteem::Object::Importer::Sim;

use Moose::Role;

use XML::Twig;

use Data::Dumper;

use constant MODEL => 'KennisbankProducten';
use constant FILESTORE_MODEL => 'Filestore';

### Redefine error
# sub handles {
#     my $self = shift;
#     my $object_type = shift;

#     return $object_type eq 'product';
# }

sub execute_import_state {
    my $self = shift;
    my $filestore = $self->schema->resultset(FILESTORE_MODEL);

    my $fileref = $filestore->find_by_uuid($self->state->import_fileref);

    my $doc = XML::Twig->new(
        twig_handlers => { entry => sub {
            $self->import_entry($_->simplify);
        }}
    );

    $doc->parsefile($fileref->get_path);
}

sub import_entry {
    my $self = shift;
    my $entry = shift;

    my $model = $self->schema->resultset(MODEL);
    my $option = $self->state->get_by('id', $entry->{ id });

    # If not in the state, or the option is to ignore, this item should not be processed
    return unless $option;
    return if $option->selected_option eq 'ignore';

    my $product;

    if($option->selected_option eq 'import') {
        $product = $model->new_result({});

        $self->copy_entry_fields($entry, $product);

        if($option->config->{ product_name }) {
            $product->naam($option->config->{ product_name });
        }

        $product->coupling('default');
        $product->commit_message('Geimporteerd');
        $product->bibliotheek_categorie_id($option->config->{ library_id } // $self->state->library_id);
        $product->versie(1);
        $product->insert;
    }

    if($option->selected_option eq 'existing') {
        unless($option->config->{ product_id }) {
            die('Item configured for linking with an existing product, but no product_id supplied');
        }

        my $existing_product = $model->find($option->config->{ product_id } // 0);

        unless($existing_product) {
            die('Item ' . $option->config->{ product_id } . ' configured for updating, but selected product to couple could not be found.');
        }

        $product = $existing_product->duplicate;

        $self->copy_entry_fields($entry, $product);

        $product->commit_message('Gelinked en geupdated: via import');
        $product->coupling('default');
        $product->external_id($entry->{ id });
        $product->update;
    }

    if($option->selected_option eq 'update') {
        my $existing_product = $model->search(
            { external_id => $entry->{ id }, deleted => undef },
            { order_by => { '-desc' => 'id' } }
        )->first;

        unless($existing_product) {
            die('Item ' . $entry->{ id } . ' configured for updating, but original product could not be found');
        }

        $product = $existing_product->duplicate;
        
        $self->copy_entry_fields($entry, $product);

        $product->commit_message('Geupdated: via import');
        $product->coupling($product->coupling);
        $product->external_id($product->external_id);
        $product->update;

    }

    $option->source_uri('/kennisbank/product/' . $product->bibliotheek_categorie_id->id . '/' . $product->id . '/' . $product->naam);
}

sub copy_entry_fields {
    my $self = shift;
    my $item = shift;
    my $product = shift;

    $product->naam($item->{ title });
    $product->kosten($item->{ content }{ kosten });
    $product->external_id($item->{ id });

    $product->omschrijving($self->extract_text(
        'centraal',
        $item->{ content }{ samenvatting }{ tekst }
    ));

    $product->voorwaarden($self->extract_text(
        'centraal',
        $item->{ content }{ voorwaarden }{ tekst }
    ));

    $product->aanpak($self->extract_text(
        'centraal',
        $item->{ content }{ gangvanzaken }{ tekst }
    ));
}

sub extract_text {
    my $self = shift;
    my $source = shift;
    my $subtree = shift;

    my @texts;

    if(ref $subtree eq 'ARRAY') {
        push(@texts, @{ $subtree });
    } else {
        push(@texts, $subtree);
    }

    my ($text) = grep { $_->{ bron } eq $source } @texts;

    return $text->{ content };
}

sub hydrate_import_state {
    my $self = shift;
    my $fileref = shift;

    unless($self->validate($fileref)) {
        die('Importbestand kon niet geinterpreteerd worden als geldige Kluwer XML');
    }

    $self->state->import_fileref($fileref->uuid);
    $self->state->format($self->format);
    $self->state->object_type($self->object_type);

    my $doc = XML::Twig->new(
        twig_handlers => { entry => sub {
            $self->parse($_->simplify);
        }}
    );

    $doc->parsefile($fileref->get_path);
}

# sub parse {
#     my $self = shift;
#     my $model = $self->schema->resultset(MODEL);
#     my $item = shift;

#     my $row = $model->search(
#         { external_id => $item->{ id }, deleted => undef },
#         { order_by => { -desc => 'id' } }
#     )->first;

#     my $state_item = $self->state->new_item({
#         id => $item->{ id },
#         label => $item->{ title }
#     });

#     # New import item. full control over it
#     unless($row) {
#         $state_item->add_option('import', 'Voeg meegeleverd product toe');
#         $state_item->add_option('existing', 'Gebruik bestaand product');
#         $state_item->add_option('ignore', 'Product niet importeren');

#         return;
#     }

#     # Decoupled or not, add these options, select option depeding on coupling
#     $state_item->add_option('update', 'Update bestaand product');
#     $state_item->add_option('ignore', 'Product niet importeren');
#     $state_item->select_option($row->coupling eq 'local' ? 'ignore' : 'update');
# }

# sub validate {
#     my $self = shift;
#     my $fileref = shift;

#     return unless($fileref->mimetype eq 'application/xml');
# }

1;
