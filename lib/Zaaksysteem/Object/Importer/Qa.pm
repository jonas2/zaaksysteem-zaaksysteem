package Zaaksysteem::Object::Importer::Sim;

use Moose::Role;

use XML::Twig;

use Data::Dumper;

has 'parser' => ( is => 'rw' );
has 'rows' => ( is => 'rw', default => sub { [] } );

sub handles {
    my $self = shift;
    my $format = shift;

    $self->parser($format);

    return ($format eq 'product' || $format eq 'question');
}

sub execute {
    my $self = shift;
    my $fileref = shift;

    unless($self->validate($fileref)) {
        die('Importbestand kon niet geinterpreteerd worden als geldige Kluwer XML');
    }

    my $doc = XML::Twig->new(
        twig_handlers => { item => sub {
            $self->parse(
                $self->parser,
                $fileref->result_source->schema,
                $_->simplify
            );
        }}
    );

    $doc->parsefile($fileref->get_path);
}

sub parse {
    my $self = shift;
    my $type = shift;
    my $schema = shift;

    my $parser = 'parse_' . $type;
    my $model;

    if($type eq 'product') { $model = $schema->resultset('KennisbankProducten'); }
    if($type eq 'question') { $model = $schema->resulset('KennisbankVragen'); }

    return $self->$parser($model, @_);
}

sub parse_question {
    my $self = shift;
    my $model = shift;
    my $item = shift;
}

sub parse_product {
    my $self = shift;
    my $model = shift;
    my $item = shift;

    my $row = $model->search({ external_id => $item->{ identifier } });

    unless($row) {
        $row = $model->new_result({
            external_id => $item->{ identifier },
            vraag => $item->{ question },
            antwoord => $item->{ answer }
        });

        warn 'Neues item';
    }

    push(@{ $self->rows }, $row);
}

sub validate {
    my $self = shift;
    my $fileref = shift;

    return unless($fileref->mimetype eq 'application/xml');

    return 1;
}

1;
