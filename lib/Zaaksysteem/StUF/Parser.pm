package Zaaksysteem::StUF::Parser;

use Moose;
use FindBin qw/$Bin/;

use XML::Tidy;

use Zaaksysteem::Exception;

use XML::LibXML;
use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Util;
use XML::Compile::Translate::Reader;

use constant WRITER_CONFIG  => {
    'hooks' => [
        {
            before => sub {
                my ($doc, $value, $path) = @_;

                if (UNIVERSAL::isa($value, 'Zaaksysteem::StUF::Body::Field')) {
                    $value = $value->value;
                }

                if (UNIVERSAL::isa($value, 'HASH')) {
                    for my $key (keys %{ $value }) {
                        if (
                            UNIVERSAL::isa(
                                $value->{$key},
                                'Zaaksysteem::StUF::Body::Field'
                            )
                        ) {
                            $value->{$key} = $value->{$key}->value;
                        }                        
                    }
                }

#                warn(Data::Dumper::Dumper($value));

                # if (my ($novalue) = $value =~ /NIL:(.*)/) {
                #     my ($tag)       = $path =~ /\/([\w\d_-]*)$/;

                #     $value          = XML::LibXML::Element->new( 'BG:' . $tag );
                #     $value->setAttribute('xsi:nil', 'true');
                #     $value->setAttribute('StUF:noValue', $novalue);
                # }

                # if (
                #     UNIVERSAL::isa($value, 'HASH') &&
                #     #exists($value->{})
                #     exists($value->{kerngegeven})
                # ) {
                #     #warn(Data::Dumper::Dumper($value));
                #     delete($value->{kerngegeven});
                #     delete($value->{gegevengroep});
                #     delete($value->{indOnvolledigeDatum});                
                # }

                return $value;
            }
        },
    ],
    #prefixes        => {
    #    'http://www.egem.nl/StUF/StUF0204'  => 'STUF',
    #    'http://www.egem.nl/StUF/sector/bg/0204'  => 'BG'
    #}
};

use constant READER_CONFIG  => {
    sloppy_integers => 1,
    interpret_nillable_as_optional => 1,
    check_values    => 0,
    'hooks' => [
        {
            before => sub {
                my ($node, $value, $path) = @_;


                ### StUF Patch, somehow we do not know how to parse PRSADRINS messages. Also,
                ### we ignore the PRSADRINS information, so this information is not needed
                ### for zaaksysteem. Therefore, we remove the PRSADRINS blocks to keep the sysin
                ### from halting
                if ($node->localName =~ /^PRS$/) {
                    my @children = $node->childNodes();
                    my $found = {};
                    for my $child (@children) {
                        if ($child->localName && $child->localName =~ /^PRSADRINS$/) {
                            if ($found->{$child->nodeName}) {
                                $node->removeChild($child);
                            }

                            $found->{$child->nodeName} = 1;
                        }
                    }
                }

                ### Vicrea Patch, add nillable when we get a noValue
                ### attribute
                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'noValue'
                    ) &&
                    !$node->hasAttributeNS(
                        'http://www.w3.org/2001/XMLSchema-instance',
                        'nil'
                    )
                ) {
                    $node->setAttributeNS(
                        'http://www.w3.org/2001/XMLSchema-instance',
                        'nil',
                        'true'
                    );
                }

                return $node;
            },
            after => sub {
                my ($node, $value, $path) = @_;

                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelGegevensbeheer'
                    )
                ) {
                    $value->{
                        'sleutelGegevensbeheer'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelGegevensbeheer'
                    );
                }

                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'verwerkingssoort'
                    )
                ) {
                    $value->{
                        'verwerkingssoort'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'verwerkingssoort'
                    );
                }

                if (
                    $node->hasAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelVerzendend'
                    )
                ) {
                    $value->{
                        'sleutelVerzendend'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'sleutelVerzendend'
                    );
                }


                if (
                    $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'noValue'
                    )
                ) {
                    $value->{
                        'noValue'
                    } = $node->getAttributeNS(
                        'http://www.egem.nl/StUF/StUF0204',
                        'noValue'
                    );
                }
                
                return $value;
            }
        }
    ]
};

has 'reader_writer_config'    => (
    is      => 'ro',
    default => sub {
        return {
            'READER'    => READER_CONFIG,
            'WRITER'    => WRITER_CONFIG,
        };
    }
);

has 'xml_definitions'   => (
    'is'    => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return [
            $self->home . '/share/wsdl/stuf/0204/stuf0204.xsd',
            $self->home . '/share/wsdl/stuf/bg0204/bgstuf0204.xsd',
            $self->home . '/share/wsdl/stuf/bg0204/bg0204.xsd',
        ];
    }
);

has 'schema'    => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        return $self->cache->{schema} if exists $self->cache->{schema};

        my $schema  = XML::Compile::Cache->new($self->xml_definitions);
        $schema->compileAll;

        return $self->cache->{schema} = $schema;
    }
);

has 'xml'       => (
    is      => 'rw',
);

has 'namespace'      => (
    is      => 'rw',
);

has 'element'       => (
    is      => 'rw',
);

has 'data'      => (
    is      => 'rw',
);

has 'home'      => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has 'body'                  => (
    is      => 'rw',
);

has 'stuurgegevens'         => (
    is      => 'rw',
);

has 'berichtsoort'          => (
    is      => 'rw',
);

has 'cache'                 => (
    is      => 'rw',
    default => sub {{}; },
);

sub _get_body_as_element {
    my $self            = shift;
    my $dom             = XML::LibXML->load_xml(string => $self->xml);

    my ($soap_body)     = $dom->findnodes("//*[local-name()='Body']");

    my $elem;
    if ($soap_body) {
        ($elem)          = $soap_body->getChildrenByTagName('*');
    } else {
        ### Could be without Soap body....argh:
        for my $element ($self->schema->elements) {
            next if $elem;
            my ($ns, $localname) = unpack_type $element;

            next unless $ns eq 'http://www.egem.nl/StUF/sector/bg/0204';

            ($elem)          = $dom->findnodes("//*[local-name()='" . $localname . "']");
        }
    }

    # if (!$elem) {

    #     die(Data::Dumper::Dumper($self->schema->elements));
    # }

    return $elem;

    # for my $element ($self->schema->elements) {
    #     my ($ns, $tag)  = $element =~ /^{(.*?)}(.*)$/;

    #     unless ($ns =~ /sector/) {
    #         next;
    #     }


    #     my @elements    = $dom->getElementsByTagNameNS($ns, $tag);

    #     return shift(@elements) if scalar(@elements);
    # }
}

sub process {
    my $self        = shift;

    throw(
        'stuf/parser/parse_xml/no_xml',
        'No XML given'
    ) unless $self->xml;

    my $stufxml = $self->_get_body_as_element();

    my $elem    = pack_type $stufxml->namespaceURI, $stufxml->localName;

    {
        local $SIG{__WARN__} = sub {}; # locally ignore any warnings
        $self->schema->declare('READER' => $elem, %{ READER_CONFIG() });
    }


    my $reader  = $self->schema->reader($elem);

    $self->namespace($stufxml->namespaceURI);
    $self->element($stufxml->localName);
    $self->data($reader->($stufxml));
}

sub to_xml {
    my $self        = shift;

    my $perldata    = {
        stuurgegevens   => $self->stuurgegevens,
    };

    $perldata->{body}   = $self->body if $self->body;

    my $elem            = pack_type $self->namespace, $self->element;

    {
        local $SIG{__WARN__} = sub {}; # locally ignore any warnings
        $self->schema->declare('WRITER' => $elem, %{ WRITER_CONFIG() });
    }

    my $writer          = $self->schema->writer($elem);

    my $doc     = XML::LibXML::Document->new('1.0', 'UTF-8');

    my $xmlelem = $writer->($doc, $perldata);

    my $tidy    = XML::Tidy->new(xml => $xmlelem->toString);
    $tidy->tidy;

    return $tidy->toString();
}

__PACKAGE__->meta->make_immutable();
