package Zaaksysteem::StUF::VBO;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'extra.authentiekeIdentificatieVerblijfsobject' => 'verblijfsobject_identificatie',
#    'bouwjaar'                                      => 'verblijfsobject_bouwjaar',
    'datumBegin'                                    => 'verblijfsobject_begindatum',
    'extra.statusCode'                              => 'verblijfsobject_status',
    'extra.aanduidingGegevensInOnderzoek'           => 'verblijfsobject_inonderzoek',
    'extra.codeGebruiksdoel'                        => 'verblijfsobject_gebruiksdoel',

    'adr.extra.authentiekeIdentificatieWoonplaats'      => 'woonplaats_identificatie',
    'adr.extra.authentiekeWoonplaatsnaam'               => 'woonplaats_naam',

    'adr.extra.authentiekeIdentificatieOpenbareRuimte'  => 'openbareruimte_identificatie',
    'adr.extra.officieleStraatnaam'                     => 'openbareruimte_naam',

    'adr.extra.identificatieNummerAanduiding'   => 'nummeraanduiding_identificatie',
    'adr.extra.ingangsdatum'                    => 'nummeraanduiding_begindatum',
    'adr.huisnummer'                            => 'nummeraanduiding_huisnummer',
    'adr.huisletter'                            => 'nummeraanduiding_huisletter',
    'adr.huisnummertoevoeging'                  => 'nummeraanduiding_huisnummertoevoeging',
    'adr.postcode'                              => 'nummeraanduiding_postcode',
    'adr.extra.aanduidingGegevensInOnderzoek'   => 'nummeraanduiding_inonderzoek',
    'adr.extra.status'                          => 'nummeraanduiding_status',
};

use constant STUF_GEBRUIKSDOEL_CODES    => {
    1   => 'woonfunctie',
    11  => 'bijeenkomstfunctie',
    21  => 'celfunctie',
    31  => 'gezondheidszorgfunctie',
    41  => 'industriefunctie',
    51  => 'kantoorfunctie',
    61  => 'logiesfunctie',
    71  => 'onderwijsfunctie',
    76  => 'sportfunctie',
    81  => 'winkelfunctie',
    91  => 'overige',
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

#TODO GEBRUIKSDOEL

sub get_params_for_vbo {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{VBO};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my $object_key  = $key;
        my $object_value;

        if ($object_key =~ /^adr\.extra\./) {
            $object_key =~ s/^adr\.extra\.//;

            next unless exists($object_params->{ 'VBOADR' }->[0]->{ADR}->{ 'extraElementen' }-> { $object_key });
            $object_value = $object_params->{ 'VBOADR' }->[0]->{ADR}->{ 'extraElementen' }-> { $object_key };
        } elsif ($object_key =~ /^adr\./) {
            $object_key =~ s/^adr\.//;

            next unless exists($object_params->{ 'VBOADR' }->[0]->{ADR}-> { $object_key });
            $object_value = $object_params->{ 'VBOADR' }->[0]->{ADR}-> { $object_key };
        } elsif ($object_key =~ /^extra\./) {
            $object_key =~ s/^extra\.//;

            next unless exists($object_params->{ 'extraElementen' }-> { $object_key });
            $object_value = $object_params->{ 'extraElementen' }-> { $object_key };
        } else {
            next unless exists($object_params->{ $object_key });

            $object_value = $object_params->{ $object_key };
        }

        $params->{ADDRESS_MAP->{$key}} = $object_value;
    }

    ### Code Gebruiksdoel, strip undefs
    ### DO SOMETHING GEBRUIKSDOEL
    if ($params->{verblijfsobject_gebruiksdoel}) {
        my @gebruiksdoelen;
        my @raw_gebruiksdoelen = (
            UNIVERSAL::isa(
                $params->{verblijfsobject_gebruiksdoel}, 'ARRAY'
            )
                ? @{ $params->{verblijfsobject_gebruiksdoel} }
                : $params->{verblijfsobject_gebruiksdoel}
        );

        for my $gebruiksdoel (@raw_gebruiksdoelen) {
            next unless defined($gebruiksdoel);

            push(@gebruiksdoelen, STUF_GEBRUIKSDOEL_CODES->{int($gebruiksdoel)});
        }

        $params->{verblijfsobject_gebruiksdoel} = \@gebruiksdoelen;
    }

    ### DO SOMETHING STATUS

    return $params;
}


1;