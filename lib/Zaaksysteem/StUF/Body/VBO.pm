package Zaaksysteem::StUF::Body::VBO;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'VBO';

use constant    OBJECT_VALUES   => [
    {
        name        => 'bouwjaar',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'verblijfsObjectNummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'verblijfsObjectType',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    ### Relationships
    {
        name        => 'VBOADR',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Relationship]',
        related     => 'ADR',
    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);


has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;