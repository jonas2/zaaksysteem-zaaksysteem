package Zaaksysteem::StUF::Body::ADR;

use Moose;

extends 'Zaaksysteem::StUF::Body';

use constant    OBJECT_NAME     => 'ADR';

use constant    OBJECT_VALUES   => [
    {
        name        => 'postcode',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'woonplaatsnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'straatnaam',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisletter',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'huisnummertoevoeging',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'aanduidingBijHuisnummer',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',
    },
    {
        name        => 'ingangsdatum',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
    # {
    #     name        => 'ingangsdatum',
    #     isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    # },
    # {
    #     name        => 'einddatum',
    #     isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    # },
    {
        name        => 'gemeentecode',
        isa         => 'Maybe[Zaaksysteem::StUF::Body::Field]',

    },
];

### Generate Moose Attributes
for my $value (@{ OBJECT_VALUES() }) {
    my %attr_opts = map {
        $_      => $value->{$_}
    } grep ({ exists($value->{$_}) } qw/is isa default lazy/);
    $attr_opts{is}          = 'rw' unless $attr_opts{is};
    $attr_opts{predicate}   = 'has_' . $value->{name};
    $attr_opts{clearer}     = 'clear_' . $value->{name};

    has $value->{name} => %attr_opts;
}

has '_object_params'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_VALUES;
    }
);


has '_object_name'  => (
    is          => 'rw',
    lazy        => 1,
    default     => sub {
        return OBJECT_NAME;
    }
);

__PACKAGE__->meta->make_immutable;