package Zaaksysteem::DB::ResultSet::ZaaktypeKenmerken;

use strict;
use warnings;

use Moose;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

use Zaaksysteem::Constants qw/
    DEFAULT_KENMERKEN_GROUP_DATA
/;

use constant    PROFILE => {
    required        => [qw/
        bibliotheek_kenmerken_id
        label
    /],
    optional        => [qw/
        description
        help
        created
        pip
        publish_public
        zaakinformatie_view
        document_categorie
        value_mandatory
        value_default
        referential
    /],
};





sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(@_, $profile);
}

sub search {
    my $self            = shift;
    my $search          = shift;

    unless ($search) {
        $search         = {};
    }


    my $remove = $self->next::method($search, @_);

    return $remove;
}

sub _commit_session {
    my $self                    = shift;

    ### Remove old authorisations
    my $node                    = shift;
    my $element_session_data    = shift;

    use Data::Dumper;
    while (my ($key, $data) = each %{ $element_session_data }) {
        unless (
            (
                $data->{naam} &&
                $data->{bibliotheek_kenmerken_id}
            ) ||
            $data->{is_group}
        ) {
            delete($element_session_data->{$key});
            next;
        }
    }

    $self->next::method( $node, $element_session_data );
}


sub _retrieve_as_session {
    my $self            = shift;

    my $rv              = $self->next::method({
        search  => {
            is_group    => [ 1, undef ],
        }
    });

    return $rv unless UNIVERSAL::isa($rv, 'HASH');

    ### Detect groupen, when not found, create one
    ### BACKWARDS 1.1.9 COMPATIBILITY {
    if (scalar(keys %{ $rv }) && !$rv->{1}->{is_group}) {
        my $group_info  = DEFAULT_KENMERKEN_GROUP_DATA;

        my $newrv       = {};

        $newrv->{1} = $self->_get_session_template;
        $newrv->{1}->{zaaktype_node_id} = $rv->{1}->{zaaktype_node_id};
        $newrv->{1}->{zaak_status_id}   = $rv->{1}->{zaak_status_id};

        $newrv->{1}->{is_group}         = 1;
        $newrv->{1}->{help}             = $group_info->{help};
        $newrv->{1}->{label}            = $group_info->{label};

        for (my $counter = 2; $counter <= (scalar( keys %{ $rv }) + 1); $counter++) {
            $newrv->{ $counter } = $rv->{($counter - 1)};
        }

        $rv = $newrv;
    }
    ### } END BACKWARDS 1.1.9 COMPATIBILITY

    return $rv;
}

sub search_fase_kenmerken {
    my $self        = shift;
    my $fase        = shift;

    my $kenmerken   = $self->search(
        {
            zaak_status_id     => $fase->id,
        },
        {
            'order_by'  => { '-asc' => 'me.id' },
            'prefetch'  => 'bibliotheek_kenmerken_id'
        }
    );

    $kenmerken->count;

    if (scalar(@_)) {
        return $kenmerken->search(@_);
    }

    return $kenmerken;
}

=head2 mangle_defaults(\@given_properties) || mangle_defaults(\%given_properties)

Returns a array-/hashref of parameters stemming from the current given
properties plus the ones having a default property according to our library.

B<Examples>

  # Given an ARRAYREF
    my $props = $rs->mangle_defaults([
        { 1     => 'User overwritten beer brew: Amstel' },
        { 3     => 'Hertog' }
    ]);

    print Dumper($props);

    #returns:
    #    $VAR1 = [
    #        { 1     => 'User overwritten beer brew: Amstel' },
    #        { 3     => 'Hertog' },
    #        { 2     => 'Amstel' }
    #    ];

  # Given an HASHREF
    my $props = $rs->mangle_defaults(
         1     => 'User overwritten beer brew: Amstel',
         3     => 'Hertog'
    ]);

    print Dumper($props);

    #returns:
    #    $VAR1 = {
    #         1     => 'User overwritten beer brew: Amstel',
    #         3     => 'Hertog',
    #         2     => 'Amstel'
    #    };

=cut

sub mangle_defaults {
    my ($self, $user_properties) = @_;

    die('User_properties undefined or not an ARRAYREF/HASHREF')
        unless (
            UNIVERSAL::isa($user_properties, 'ARRAY') ||
            UNIVERSAL::isa($user_properties, 'HASH')
        );

    my $return_properties   = {};
    if (UNIVERSAL::isa($user_properties, 'ARRAY')) {
        ### Make this set of data machine readable
        for my $user_prop (@{ $user_properties }) {
            my ($bibliotheek_kenmerken_id, $values) = each %$user_prop;
            $return_properties->{$bibliotheek_kenmerken_id} = $values;
        }
    } else {
        $return_properties  = { %{ $user_properties } };

    }

    # TODO - it looks like this query also gets defaults from later phases. restrict?
    # or is there a good reason to do this right away?
    my $properties          = $self->search(
        {
            is_group                                    => undef,
            'bibliotheek_kenmerken_id.id'               => { '!='   => undef },
            'bibliotheek_kenmerken_id.value_type'       => { '!='   => 'file' },
            'bibliotheek_kenmerken_id.value_default'    => { '!='   => undef },
            'bibliotheek_kenmerken_id.id'           => {
                'NOT IN' => [ keys %{ $return_properties } ]
            }
        },
        {
            prefetch    => 'bibliotheek_kenmerken_id'
        }
    );

    while(my $property = $properties->next) {
        my $value_default   = $property
                            ->bibliotheek_kenmerken_id
                            ->value_default;

        ### Value maybe defined, but could be empty. In our context: this
        ### means still 'defined'
        next unless $value_default;

        my $bib_id          = $property
                            ->bibliotheek_kenmerken_id
                            ->id;

        ### Overdone test below, because the IN query above should sort this
        ### out...but by the the concept of not having tests
        next if defined($return_properties->{ $bib_id });

        $return_properties->{ $bib_id } = $value_default;
    }

    ### Make this set of data back to zaak readable
    if (UNIVERSAL::isa($user_properties, 'ARRAY')) {
        my @mangled_params;
        for my $key (keys %{ $return_properties }) {
            push(
                @mangled_params,
                { $key  => $return_properties->{ $key } }
            );
        }
        return \@mangled_params;
    } else {
        return $return_properties;
    }

    return;
}

1;
