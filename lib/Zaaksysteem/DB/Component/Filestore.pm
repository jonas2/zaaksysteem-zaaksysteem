package Zaaksysteem::DB::Component::Filestore;

use strict;
use warnings;

use Moose;

use Data::Dumper;
use Zaaksysteem::JSON;
use File::Path qw/remove_tree/;

extends 'DBIx::Class';

# TODO use on_delete trigger?
sub remove_file {
    my ($self, $opts) = @_;
    
    my $files_dir = $opts->{files_dir} or die "need files_dir";

    my $disk_location = $self->disk_location({
        files_dir => $files_dir
    });

    remove_tree($disk_location);
}


sub disk_location {
    my ($self, $opts) = @_;

    my $files_dir = $opts->{files_dir} or die "need files_dir";
    
    return $files_dir . "/filestore/" . $self->id;
}


1; #__PACKAGE__->meta->make_immutable;

