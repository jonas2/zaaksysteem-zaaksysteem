package Zaaksysteem::DB::Component::Logging::Kcc::Call;

use Moose::Role;
use Data::Dumper;
use JSON;

sub onderwerp {
    my ($self) = @_;

    return sprintf("KCC Registratie: %s naar toestel %s", $self->data->{ phonenumber }, $self->data->{ extension });
}

sub _add_magic_attributes {
    shift->meta->add_attribute('contact' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        my ($contact) = $self->result_source->schema->resultset('ContactData')->find_by_phonenumber(
            $self->data->{ phonenumber }
        );

        return $contact;
    }));
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    $data->{ contact } = $self->contact;
    $data->{ accepted } = exists $self->data->{ accepted } ? ($self->data->{ accepted } ? JSON::true : JSON::false) : JSON::null;

    return $data;
};

sub _should_filter {
    my $self = shift;

    return
        (!exists $self->data->{ accepted }) &&
        (defined $self->contact) &&
        $self->data->{ extension } eq 201; # hack voor vianen presentatie
}

1;
