package Zaaksysteem::DB::Component::Logging::Case::Create;

use Moose::Role;

sub onderwerp {
    sprintf('Zaak %s geregistreerd', shift->data->{ case_id });
}

sub event_category { 'case-mutation'; }

1;
