package Zaaksysteem::DB::Component::Logging::Case::Update::SettleDate;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Streefafhandeldatum voor zaak %s gewijzigd naar: %s, opmerking: %s',
        $self->data->{ case_id },
        $self->data->{ settle_date },
        $self->data->{ reason }
    );
}

sub event_category { 'case-mutation'; }

1;
