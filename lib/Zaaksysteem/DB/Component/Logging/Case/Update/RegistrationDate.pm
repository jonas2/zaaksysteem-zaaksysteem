package Zaaksysteem::DB::Component::Logging::Case::Update::RegistrationDate;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Registratiedatum voor zaak: %d gewijzigd naar: %s',
        $self->data->{ case_id },
        $self->data->{ registration_date }
    );
}

sub event_category { 'case-mutation'; }

1;
