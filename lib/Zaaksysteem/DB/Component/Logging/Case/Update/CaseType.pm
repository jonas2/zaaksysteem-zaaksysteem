package Zaaksysteem::DB::Component::Logging::Case::Update::CaseType;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak gekopieerd met ander Zaaktype van zaak %s naar zaak %s',
        $self->data->{ case_id },
        $self->data->{ new_case_id }
    );
}

sub event_category { 'case-mutation'; }

1;
