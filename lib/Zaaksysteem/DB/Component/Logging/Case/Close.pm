package Zaaksysteem::DB::Component::Logging::Case::Close;

use Moose::Role;

sub onderwerp {
    my $self        = shift;

    if($self->data->{ case_result }) {
        return sprintf(
            'Zaak %s afgehandeld: %s',
            $self->data->{ case_id },
            $self->data->{ case_result }
        );
    } else {
        return sprintf(
            'Zaak %s afgehandeld',
            $self->data->{ case_id }
        );
    }
}

1;
