package Zaaksysteem::DB::Component::Logging::Case::Subject::Add;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Betrokkene "%s" toegevoegd aan zaak %s als %s',
        $self->data->{ subject_name },
        $self->data->{ case_id },
        $self->data->{ role }
    );
}

1;
