package Zaaksysteem::DB::Component::Logging::Case::Checklist::Item::Update;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Status van checklistitem "%s" gewijzigd naar: %s',
        $self->data->{ checklist_item_name },
        $self->data->{ checklist_item_value }
    );
}

sub event_category { 'case-mutation'; }

1;
