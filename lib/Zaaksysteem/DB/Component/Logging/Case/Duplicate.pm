package Zaaksysteem::DB::Component::Logging::Case::Duplicate;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf('Zaakgegevens gekopieerd van zaak %s naar nieuwe zaak %s', $self->data->{ case_id }, $self->data->{ duplicate_id });
}

1;
