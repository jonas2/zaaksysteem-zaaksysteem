package Zaaksysteem::DB::Component::Logging::Case::Document;

use Moose::Role;

has file => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('File')->find($self->data->{ file_id } // $self->component_id);
});

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $file = $self->file;

    return $data unless $file;

    $data->{ file_id } = $file->id;
    $data->{ file_name } = $file->name;
    $data->{ mimetype } = $file->filestore_id->mimetype;

    return $data;
};

sub event_category { 'document'; }

1;
