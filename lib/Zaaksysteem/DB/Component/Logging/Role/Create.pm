package Zaaksysteem::DB::Component::Logging::Role::Create;

use Moose::Role;

sub onderwerp {
    sprintf('Rol %s succesvol toegevoegd', shift->data->{ role });
}

1;
