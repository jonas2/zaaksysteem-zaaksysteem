package Zaaksysteem::DB::Component::Logging::Role::Remove;

use Moose::Role;

sub onderwerp {
    sprintf('Entry met DN: %s succesvol verwijderd', shift->data->{ dn });
}

1;
