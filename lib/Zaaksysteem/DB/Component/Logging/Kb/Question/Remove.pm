package Zaaksysteem::DB::Component::Logging::Kb::Question::Remove;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Kennisbank vraag %s verwijderd: %s',
        $self->question->naam,
        $self->data->{ reason }
    );
}

sub _add_magic_attributes {
    shift->meta->add_attribute('question' => (is => 'ro', lazy => 1, default => sub {
        my $self = shift;
        
        $self->result_source->schema->resultset('KennisbankVragen')->find($self->data->{ question_id });
    }));
}

sub event_category { 'question' }

1;
