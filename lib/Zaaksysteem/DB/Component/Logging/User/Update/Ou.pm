package Zaaksysteem::DB::Component::Logging::User::Update::Ou;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Gebruiker %s succesvol verplaatst naar organisatorische eenheid: %s',
        $self->data->{ user_dn },
        $self->data->{ ou_dn }
    );
}

1;
