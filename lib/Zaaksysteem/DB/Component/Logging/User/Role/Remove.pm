package Zaaksysteem::DB::Component::Logging::User::Role::Remove;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Rol %s succesvol verwijderd van gebruiker: %s',
        $self->data->{ role_dn },
        $self->data->{ entry_dn }
    );
}

1;
