package Zaaksysteem::DB::Component::Logging::Subject::Note::Create;

use Moose::Role;

sub onderwerp {
    'Notitie toegevoegd'
}

sub event_category { 'note' };

1;
