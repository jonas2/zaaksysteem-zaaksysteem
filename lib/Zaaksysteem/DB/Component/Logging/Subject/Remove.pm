package Zaaksysteem::DB::Component::Logging::Subject::Remove;

use Moose::Role;

sub onderwerp {
    sprintf('Betrokkene "%s" verwijderd.', shift->data->{ subject_name });
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->resultset('Betrokkene')->find($self->data->{ subject_id });
    }));
}

1;
