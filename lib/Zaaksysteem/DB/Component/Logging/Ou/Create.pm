package Zaaksysteem::DB::Component::Logging::Ou::Create;

use Moose::Role;

sub onderwerp {
    sprintf('Organisatorische eenheid %s succesvol toegevoegd', shift->data->{ ou });
}

1;
