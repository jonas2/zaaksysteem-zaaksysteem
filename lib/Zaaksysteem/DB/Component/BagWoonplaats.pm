package Zaaksysteem::DB::Component::BagWoonplaats;

use strict;
use warnings;

use base qw[Zaaksysteem::DB::Component::BagGeneral Zaaksysteem::Geo::BAG];

# Stub geocode (so that we can translate this object into a thing that is
# queryable on Google maps for example).
sub geocode_term {
    join ',', ('Nederland', shift->naam);
}

# Really simple to_string stub to align with the API for BagNummeraanduiding
# and BagOpenbareruimte modules
sub to_string {
    shift->naam;
}

1;
