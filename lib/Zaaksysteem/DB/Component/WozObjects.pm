package Zaaksysteem::DB::Component::WozObjects;

use strict;
use warnings;

use Moose;

use Data::Dumper;
use Zaaksysteem::JSON;

extends 'DBIx::Class';


# convert the json structure back to perl hashref
sub as_hashref {
    my ($self) = @_;
    
    my $json = new Zaaksysteem::JSON;
#    warn "obj dat: " . Dumper $self->object_data;
    my $decoded = $json->_decode_from_json($self->object_data);
#    warn "decoded: " . Dumper $decoded;
    return $decoded;
}

1; #__PACKAGE__->meta->make_immutable;

