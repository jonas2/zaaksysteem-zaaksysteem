package Zaaksysteem::DB::Component::BibliotheekCategorie;

use strict;
use warnings;
use Data::Dumper;


use base qw/DBIx::Class/;



sub list_of_children {
    my $self        = shift;
    my $rv          = [];

    my $children    = $self->categorien->search;
    while (my $child = $children->next) {
        push(@{ $rv }, @{ $child->list_of_children });
        push(@{ $rv }, $child->id);
    }

    return $rv;
}


sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param. 
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    #delete $self->{_column_data}->{searchable_id};

    $self->_set_search_term();
    $self->next::method(@_);
}


sub update {
    my ($self) = shift;

    $self->_set_search_term();
    $self->next::method(@_);
}


sub _set_search_term {
    my ($self) = @_;


    my $search_term = '';
    if($self->naam) {
        $search_term .= $self->naam;
    }
    if($self->description) {
        $search_term .= ' ' . $self->description;
    }
    if($self->label) {
        $search_term .= ' ' . $self->label;
    }
    $self->search_term($search_term);
}

1;
