package Zaaksysteem::SOAP;

use Moose;
use Data::Dumper;
use XML::Tidy;

use Zaaksysteem::Exception;

use FindBin qw/$Bin/;

#use Zaaksysteem::SBUS::Dispatcher::Soap::TestTransport;

use FindBin qw/$Bin/;

use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Translate::Reader;
use XML::Compile::Util;



has 'wsdl_file'     => (
    'is'    => 'rw',
    'isa'   => 'Str',
);

has 'home'      => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has 'soap_endpoint'     => (
    'is'        => 'rw',
);

has 'soap_port'         => (
    'is'        => 'rw',
);

has 'soap_service'      => (
    'is'        => 'rw',
);

has 'soap_action'       => (
    'is'        => 'rw',
);

has 'is_test'      => (
    'is'        => 'rw',
);

has 'reader_writer_config'  => (
    'is'        => 'rw',
    'default'   => sub { return {}; }
);

has 'xml_definitions'       => (
    'is'        => 'rw',
    'default'   => sub { return []; }
);


has wsdl => (
    'is'        => 'ro',
    'lazy'      => 1,
    'default'   => sub {
        my $self    = shift;

        my %opts    = ();
        $opts{opts_readers}     = $self->reader_writer_config->{READER}
            if $self->reader_writer_config->{READER};
        $opts{opts_writers}     = $self->reader_writer_config->{WRITER}
            if $self->reader_writer_config->{WRITER};

        my $wsdl = XML::Compile::WSDL11->new(
            (
                $self->wsdl_file =~ /^\//
                    ? $self->wsdl_file
                    : $self->home . '/share/wsdl/' . $self->wsdl_file
            ),
            %opts
        );

        for my $def (@{ $self->xml_definitions }) {
            $wsdl->importDefinitions(
                $def
            );
        }

        return $wsdl;
    }
);

sub transport {
    my $self        = shift;
    my $call        = shift;

    my $transport   = XML::Compile::Transport::SOAPHTTP->new(
        'address'   => $self->soap_endpoint,
        'timeout'   => 25,
    );

    my $sbus_options = {};
    if (
        defined(
            $sbus_options->{default_ssl_key}
        ) &&
        $sbus_options->{default_ssl_key}
    ) {
        $transport->userAgent->ssl_opts(
            'SSL_key_file' =>
                $sbus_options->{default_ssl_key}
        );

        $transport->userAgent->ssl_opts(
            'SSL_cert_file' =>
                $sbus_options->{default_ssl_crt}
        );

        $transport->userAgent->ssl_opts(
            'verify_hostname' => 0
        );
    }

    $transport->userAgent->default_header('SOAPAction', $self->soap_action);

    return $transport;
}

sub get_dispatch_xml {
    my $self                        = shift;
    my ($call, $params)             = @_;

    my $client_options              = {
        transport_hook  => sub {
            my $xml         = shift;

            return $xml;
        }
    };

    my ($answer, $trace)            = $self->_dispatch_call(
        {
            call                => $call,
            params              => $params,
            client_options      => $client_options,
        }
    );

    return $trace->request->content;
}

sub dispatch {
    my $self                        = shift;
    my ($call, $params)             = @_;

    my $client_options              = {
        transport       => $self->transport($call, $self->soap_port),
    };

    my ($answer, $trace)            = $self->_dispatch_call(
        {
            call                => $call,
            params              => $params,
            client_options      => $client_options,
        }
    );

    if (!$answer) {
        throw(
            'stuf/soap/no_answer',
            'No answer received from other party, that cannot be good',
            $trace,
        );
    }

    return ($answer, $trace);
}

Params::Profile->register_profile(
    method      => '_dispatch_call',
    profile     => {
        required        => [qw/call params client_options/],
    }
);

sub _dispatch_call {
    my $self                        = shift;
    my $options                     = assert_profile(shift)->valid;

    my %soap_options                = ();
    $soap_options{service}          = $self->soap_service   if $self->soap_service;
    $soap_options{port}             = $self->soap_port      if $self->soap_port;

    my $operation                   = $self->wsdl->operation(
        $options->{call},
        %soap_options
    );

    my $client                      = $operation->compileClient(
        %{ $options->{client_options} }
    );

    my ($answer, $trace)            = $client->($options->{params});

    return ($answer, $trace);
}

sub from_xml {
    my $self    = shift;
    my $xml     = shift;

    die('xml needs to be set to be able to transform xml')
        unless $xml;

    my $doc     = XML::LibXML->load_xml(string => $xml);

    my ($node)  = $doc->findnodes('//soap:Envelope/soap:Body/*');

    my $elem    = pack_type $node->namespaceURI, $node->localName;

    my $reader  = $self->wsdl->compile(
        READER => $elem,
        %{ $self->reader_writer_config->{READER} }
    );

    # {kennisgeving} => {}
    return {
        name    => $node->localName,
        content => $reader->($node)
    };
}

1;
