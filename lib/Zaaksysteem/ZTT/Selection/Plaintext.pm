package Zaaksysteem::ZTT::Selection::Plaintext;

use Moose;

BEGIN { extends 'Zaaksysteem::ZTT::Selection' };

has start => ( is => 'ro', isa => 'Int', required => 1 );

has tag => ( is => 'ro', isa => 'Zaaksysteem::ZTT::Tag' );
has subtemplate => ( is => 'ro', isa => 'Str' );

has end => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    return $self->start + $self->length;
});

has length => ( is => 'ro', lazy => 1, default => sub {
    return length shift->selection;
});

sub as_string {
    my $self = shift;

    sprintf('%d:%d ("%s")', $self->start, $self->end, $self->selection);
}

1;
