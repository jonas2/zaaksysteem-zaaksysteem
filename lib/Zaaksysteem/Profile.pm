package Zaaksysteem::Profile;

use base 'Exporter';

use Params::Profile;
use Moose::Util::TypeConstraints qw[find_type_constraint];

use Zaaksysteem::Exception;
use Zaaksysteem::Constants;

our @EXPORT = qw[define_profile];

=head1 Zaaksysteem L<Params::Profile> helper

This module exports a single sub, C<define_profile> that helps with
defining L<Param::Profile> profiles.

=head2 define_profile

This function is nothing but some syntactic sugar for registering a
L<Params::Profile> profile.

    use Zaaksysteem::Profile;

    define_profile method_name => (
        required => [qw/required parameters/]],
        optional => [qw/optional params/]],
        constraint_methods => {
            required => qr[true|false]
        }
    );

Or, using the extended C<typed> profile syntax:

    define_profile method_name => (
        required => [qw/object string/],
        typed => {
            object => 'Package::Name', # Works, isa is called first
            string => 'Str'            # Also works, uses Moose to validate
        }
    );

=cut

sub define_profile ($%) {
    my $profile_name = shift;
    my %profile = @_;

    my $typed = delete $profile{ typed };

    if($typed) {
        $profile{ constraint_methods } = inject_typed_constraints(
            $typed,
            $profile{ constraint_methods }
        );
    }

    unless ($profile{ msgs }) {
        $profile{ msgs } = PARAMS_PROFILE_DEFAULT_MSGS;
    }

    my ($calling_class) = caller 0;

    Params::Profile->register_profile(
        method => $profile_name,
        caller => $calling_class,
        profile => \%profile
    );
}

=head2 inject_typed_constraints

This is a bit of Moose integration for L<Params::Profile>. This method
produces a hashref suitable for L<Data::FormValidator>'s constraint_methods
key.

It basically boils down to an implicit validator-chain built from combining
the C<typed> key in the validator profile and the C<constraint_methods> in a
subref that checks first if the supplied value L<UNIVERSAL::isa> specific type,
and tries to apply Moose's typesystem as a fallback.

There is one major caveat though. L<Data::FormValidator> constraint_methods
can be more than just a coderef or regexp, this code will unelegantly fail
catastrophically in that case. Deal with it.

=cut

sub inject_typed_constraints {
    my $types = shift;
    my $constraints = shift || {};

    for my $fieldname (keys %{ $types }) {
        my $type = find_type_constraint($types->{ $fieldname }) || $types->{ $fieldname };

        my $existing_constraint = delete $constraints->{ $fieldname };
        
        if($existing_constraint && (ref $existing_constraint ne 'CODE' || ref $existing_constraint ne 'Regexp')) {
            throw('params/profile/validation_chain', sprintf(
                'Could not inject wrapped validator for "%s", existing validator not a subref or regexp',
                $fieldname
            ));
        }

        $constraints->{ $fieldname } = sub {
            my $dv = shift;
            my $value = pop;

            # Here's the magic bit. This tries to run UNIVERSAL::isa on the 
            # value to see if it's an instance of $type, otherwise uses the
            # Moose type for whatever you supplied
            if(eval { $type->isa('Moose::Meta::TypeConstraint') }) {
                return $type->check($value);
            }

            if(eval { $value->isa($type); }) {
                return 1;
            }

            return unless $existing_constraint;

            if(ref $existing_constraint eq 'CODE') {
                return $existing_constraint->($dv, $value);
            } else {
                return $value =~ $existing_constraint;
            }
        };
    }

    return $constraints;
}

1;
