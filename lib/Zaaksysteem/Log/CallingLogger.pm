package Zaaksysteem::Log::CallingLogger;

use Moose;
use Catalyst::Log;
use Data::Dumper;

BEGIN { extends 'Catalyst::Log'; }

if($ENV{ CATALYST_DEBUG }) {
    override '_log' => sub {
        my ($self, $level, @error) = @_;

        my ($module, $line);

        my $iter = 2;

        while(my @frame = caller $iter++) {
            next unless $frame[0] ne __PACKAGE__;

            ($module, $line) = @frame[0, 2];
            
            last;
        }

        if($module =~ m[^Zaaksysteem]) {
            @error = map { "\t" . $_ } @error;

            unshift(@error, sprintf("In %s at line %d:", $module, $line));
            push(@error, "\n");
        }

        $self->SUPER::_log($level, @error);
    };
}

sub trace {
    my ($self, @message) = @_;

    my @frames;
    my $iter = 1;

    # Capture the namespace from which the trace call initiated
    # Use that namespace as a filter for the trace
    my $calling_namespace = (split /::/, (caller)[0])[0];

    while(my @frame = caller $iter++) {
        next unless $frame[0] =~ m[^$calling_namespace];

        push(@frames, sprintf('[%d] %s at line %d', $iter, @frame[0, 2]));
    }

    $self->debug(@frames, '', @message);
}

1;
