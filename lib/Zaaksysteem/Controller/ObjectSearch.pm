package Zaaksysteem::Controller::ObjectSearch;

use Moose;

BEGIN { extends 'Catalyst::Controller'; }

has 'page' => ( is => 'rw' );

use Data::Dumper;

use POSIX qw(strftime);
use Date::Parse qw/strptime/;

use Zaaksysteem::Betrokkene::Object::Medewerker;
use Net::LDAP;

use Zaaksysteem::Constants qw/
    OBJECTSEARCH_TABLENAMES
    OBJECTSEARCH_TABLE_ORDER
    STATUS_LABELS
/;

sub ezra_objectsearch : Chained('/') :PathPart('objectsearch') :Args(0) {
	my ($self, $c) = @_;
    
    return unless($c->req->is_xhr);

    my $params = $c->req->params();

    my $object_type = $params->{object_type};

    my $results;
    
    if($object_type) {
        my $extra_params = {};

        if($object_type eq 'zaaktype') {
            $extra_params->{trigger} = $params->{trigger}; #{ 'like' => '%'.($params->{trigger}||'extern').'%'};
            if($params->{trigger} eq 'intern') {
                $extra_params->{betrokkene_type} = 'medewerker';
            } else {
                if($params->{betrokkene_type} ne 'natuurlijk_persoon') {
                    $extra_params->{betrokkene_type} = ['niet_natuurlijk_persoon', 'niet_natuurlijk_persoon_na'];
                } else {
                    $extra_params->{betrokkene_type} = 'natuurlijk_persoon';
                }
            }
        }

        if($params->{rows}) {
            $extra_params->{rows} = $params->{rows};
        }

        $results = $self->_search_searchable_object_type({
            c            => $c,
            query        => $params->{query},
            object_type  => $params->{object_type},
            extra_params => $extra_params,
        });
    } else {
        $results = $self->_search_searchable({
            c       => $c,
            query   => $params->{query},
        });
    }

    my $entries = [];

    if($results) {
        my $object_types_seen = {};
        foreach my $result (@$results) {
            my $object_type = $result->{object_type};

            next unless $result->{searchable_id};
            
            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => $object_type,
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push( @{ $entries }, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => $result->{object_type},
                object      => $object,
            });
        }
    }

    $c->stash->{json} = { entries => $entries };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

sub ezra_objectsearch_address : Chained('/') :PathPart('objectsearch/address') : Args(0) {
	my ($self, $c) = @_;

	return unless($c->req->is_xhr);

    my $params = $c->req->params();
    
    my $entries = [];

    my $query = $params->{query};

    if($query =~ m|^\w+.*\d+\,|) {
        $entries = $self->_search_huisnummer({
            c       => $c,
            query   => $params->{query}
        });
    } else {
        $entries = $self->_search_openbareruimte({
            c       => $c,
            query   => $params->{query}
        });
    }

    $c->stash->{json} = {
        'entries'    => $entries,
    };

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub natuurlijk_persoon_search : Chained('/') :PathPart('objectsearch/contact/natuurlijk_persoon') {
	my ($self, $c) = @_;

	return unless($c->req->is_xhr);

    my $params = $c->req->params();
    
    my $entries = $self->_search_natuurlijk_persoon({
        c       => $c, 
        query   => $params->{query}
    });

    $c->stash->{json} = {
        entries	=> $entries,
    };

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub case_type_document_search : Chained('/') : PathPart('objectsearch/case_type_document') {
    my ($self, $c) = @_;
    
    my $params  = $c->req->params();

    my $entries = $self->_search_case_type_document({
        c       => $c, 
        query   => $params->{query},
    });

    $c->stash->{json} = {
        entries => $entries,
    };

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub bedrijf_search : Chained('/') :PathPart('objectsearch/contact/bedrijf') {
	my ($self, $c) = @_;

    return unless($c->req->is_xhr);

    my $params = $c->req->params();
    
    my $entries = $self->_search_bedrijf({
        c       => $c, 
        query   => $params->{query}
    });

    $c->stash->{json} = {
        entries	=> $entries,
    };

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub medewerker_search : Chained('/') :PathPart('objectsearch/contact/medewerker') {
	my ($self, $c) = @_;

    return unless $c->req->is_xhr;

    my $params = $c->req->params();
    
    my $opts = {
        intern => 0,
        type => 'medewerker',
        rows_per_page => '',
    };

    my $entries = $c->model('Betrokkene')->search($opts, {
        freetext => $params->{ query }
    });

    my $hashresults = [];

    if($entries) {
        while(my $entry = $entries->next()) {
            my $object = {
                id          => $entry->ex_id,
                object_type => 'medewerker',
                naam        => $entry->naam,
            };

            my $hashresult = { 
                id          => $entry->ex_id,
                object_type => 'medewerker',
                label       => $entry->naam,
                object      => $object, 
            };
            push @$hashresults, $hashresult;
        }
    }

    $c->stash->{json} = {
        entries	=> $hashresults,
    };

    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}

sub bag_search : Chained('/') : PathPart('objectsearch/bag') {
    my ($self, $c) = @_;

    my $query = $c->req->param('query') || '';

    my ($postal_code, $rest) = $query =~ m[(\d{4}\s?[a-zA-Z]{2})\s?(.*)];

    if($postal_code) {
        $c->detach('_bag_postal', [ $postal_code, $rest ]);
    }

    my ($number, $letter, $suffix) = $self->_parse_street_number($query);
    my $city = '';

    my @street_parts = (substr $query, 0, index($query, $number // length($query)) - 1);

    unless($number) {
        @street_parts = grep { length >= 3 } split m[\s+], $query;

        if(scalar @street_parts > 1) {
            $city = pop @street_parts;

            unless($c->model('DB::BagWoonplaats')->search({ naam => { ilike => $city } })->count) {
                unshift @street_parts, $city;
                $city = undef;
            }
        }
    }

    unless(scalar(@street_parts)) {
        $c->stash->{ json } = { entries => [] };
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    my $entries = $c->model('DB::BagNummeraanduiding')->search(
        { -and => [ map { { 'openbareruimte.naam' => { ilike => sprintf('%%%s%%', $_) } } } @street_parts ] },
        {
            order_by => [
                qw[woonplaats.naam openbareruimte.naam huisnummer],
                \'huisletter NULLS FIRST',
                \'huisnummertoevoeging NULLS FIRST',
            ],
            join => { openbareruimte => 'woonplaats' },
            rows => 20
        }
    );

    if($city) {
        $entries = $entries->search({ 'woonplaats.naam' => $city });
    }

    if($number) {
        $entries = $entries->search({ 'me.huisnummer' => $number });

        if($letter) { $entries = $entries->search({ 'me.huisletter' => uc($letter) }); }
        if($suffix) { $entries = $entries->search({ 'me.huisnummertoevoeging' => $suffix }); }
    }

    $c->log->debug('Querying BAG',
        sprintf('Street: %s', join ", ", @street_parts),
        sprintf('No.:', join ", ", grep { defined } ($number, $letter, $suffix)),
        sprintf('City: %s', $city),
        Dumper $entries->as_query
    );

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

sub _parse_street_number {
    my $self = shift;
    my $input = shift;

    return $input =~ m[(\d+)([a-zA-Z])?[\s\-]?(\w+)?];
}

sub _build_entry_set {
    my $self = shift;
    my $rs = shift;

    my @results;

    for my $entry ($rs->all) {
        push(@results, {
            id => (
                UNIVERSAL::isa($entry, 'BagNummeraanduiding')
                    ? 'nummeraanduiding-' . $entry->identificatie
                    : 'openbareruimte-' . $entry->identificatie
            ),
            object_type => UNIVERSAL::isa($entry, 'BagNummeraanduiding') ? 'bag_address' : 'bag_street',
            label => $entry->to_string,
            object => $entry
        });
    }

    return \@results;
}

sub _bag_postal : Private() {
    my ($self, $c, $postal_code, $rest) = @_;

    $c->log->debug('BAG Postal',
        sprintf('Postal Code: %s', $postal_code)
    );

    $postal_code =~ s/\s//;
    $postal_code = uc($postal_code);

    my $entries;

    if(defined $rest) {
        $entries = $c->model('DB::BagNummeraanduiding')->search(
            { 'me.postcode' => $postal_code },
            {
                order_by => [
                    qw[woonplaats.naam openbareruimte.naam postcode huisnummer],
                    \'huisletter NULLS FIRST',
                    \'huisnummertoevoeging NULLS FIRST'
                ],
                join => { openbareruimte => 'woonplaats' },
                rows => 20
            }
        );

        if($rest =~ m[^\d]) {
            my ($number, $letter, $suffix) = $self->_parse_street_number($rest);

            $c->log->debug('BAG Postal parsed street number',
                sprintf('No.: %d', $number),
                sprintf('Letter: %s', $letter),
                sprintf('Suffix: %s', $suffix)
            );

            if($number) { $entries = $entries->search({ 'me.huisnummer' => $number }); }
            if($letter) { $entries = $entries->search({ 'me.huisletter' => $letter }); }
            if($suffix) { $entries = $entries->search({ 'me.huisnummertoevoeging' => $suffix }); }
        }
    } else {
        $entries = $c->model('DB::BagOpenbareruimte')->search(
            { 'hoofdadressen.postcode' => $postal_code },
            {
                join => [qw[hoofdadressen woonplaats]],
                order_by => [
                    qw[woonplaats.naam me.naam hoofdadressen.postcode hoofdadressen.huisnummer],
                    \'hoofdadressen.huisletter NULLS FIRST',
                    \'hoofdadressen.huisnummertoevoeging NULLS FIRST'
                ],
                group_by => [qw[me.identificatie me.begindatum me.einddatum me.naam me.officieel me.woonplaats me.type me.inonderzoek me.documentdatum me.documentnummer me.status me.correctie]],
                rows => 20
            }
        );
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

sub bag_search_street : Chained('/') : PathPart('objectsearch/bag-street') {
    my ($self, $c) = @_;

    my $query = $c->req->param('query');

    my ($postal_code, $rest) = $query =~ m[(\d{4}\s?[a-zA-Z]{2})\s?(.*)];

    if($postal_code) {
        $c->detach('_bag_postal', [ $postal_code ]);
    }

    my @query_parts = grep { length $_ >= 3 } split m[\s], $query;
    my ($city, $street);

    if(scalar(@query_parts) > 1) {
        $city = pop @query_parts;
    }

    unless(scalar(@query_parts)) {
        $c->stash->{ json } = { entries => [] };
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    my $entries = $c->model('DB::BagOpenbareruimte')->search(
        { -and => [ map { { 'me.naam' => { ilike => sprintf('%%%s%%', $_) } } } @query_parts ] },
        { join => [ 'woonplaats' ], rows => 20, order_by => [qw[woonplaats.naam me.naam]] }
    );

    if($city) {
        $entries = $entries->search({ 'woonplaats.naam' => { ilike => sprintf('%%%s%%', $city) } });
    }

    $c->stash->{ json } = { entries => $self->_build_entry_set($entries) };
    $c->detach('Zaaksysteem::View::JSONlegacy');
}

Params::Profile->register_profile(
    method  => '_retrieve_object',
    profile => {
        required => [ qw/c object_type searchable_id/ ]
    }
);

sub _retrieve_object {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _retrieve_object" unless $dv->success;

    my $c               = $params->{c};
    my $object_type     = $params->{object_type};
    my $searchable_id   = $params->{searchable_id};

    my $tablename = OBJECTSEARCH_TABLENAMES->{$object_type}->{tablename};
    
    my $options = {};

    if($object_type eq 'natuurlijk_persoon') {
        $options->{prefetch} = ['adres_id'];
    } elsif($object_type eq 'zaaktype') {
        $options->{prefetch} = [
            'zaaktype_node_id', 
            { 'zaaktype_node_id' => 'zaaktype_definitie_id'} 
        ];
    }

    $options->{result_class} = 'DBIx::Class::ResultClass::HashRefInflator';
    
    my $resultset = $c->model("DB::" . $tablename)->search(
        { searchable_id => $searchable_id },
        $options
    );

    ### Deleted of store, ignore
    if($object_type eq 'natuurlijk_persoon' || $object_type eq 'bedrijf') {
        return if $resultset->first->{deleted_on};
    }
    
    # searchable_id is unique, so it's safe to return first. there ain't gonna
    # be more rows.
    return $self->_preprocess_object($c, $resultset->first, $object_type);
}    

sub _preprocess_object {
    my ($self, $c, $object, $object_type) = @_;

    if($object_type eq 'natuurlijk_persoon') {
        if($object->{voornamen}) {
            $object->{voorletters} = $self->_get_voorletters({
                voornamen => $object->{voornamen}
            });
        }

        if($object->{geboortedatum}) {
            my ($year, $month, $day) = $object->{geboortedatum} =~ m|(\d+)-(\d+)-(\d+)|;
            $object->{geboortedatum} = "$day-$month-$year";
        }
        
    } elsif($object_type eq 'kennisbank_vragen') {
        $object->{vraag_plaintext} = $self->_limit_text(
            $self->_strip_html($object->{vraag} || ''),
            40
        );
        $object->{antwoord_plaintext} = $self->_limit_text(
            $self->_strip_html($object->{antwoord} || ''),
            40
        );
    } elsif($object_type eq 'kennisbank_producten') {
        $object->{omschrijving_plaintext} = $self->_limit_text(
            $self->_strip_html($object->{omschrijving}),
            40
        );
    } elsif($object_type eq 'zaak') {
        my $status = $object->{status};

        $object->{status_formatted} = STATUS_LABELS->{$status};

        my $case = $c->model('DB::Zaak')->find($object->{ aanvrager }->{ zaak_id });

        if($case) {
            $object->{ description } = $case->onderwerp;
            $object->{ fase } = $case->volgende_fase ? $case->volgende_fase->fase : $case->huidige_fase->fase;
        }
    } elsif($object_type eq 'zaaktype') {

        if(my $preset_client = $object->{zaaktype_node_id}->{zaaktype_definitie_id}->{preset_client}) {
            my ($betrokkene_type, $betrokkene_id) = $preset_client =~ m|betrokkene-(\w+)-(\d+)|;
            
            if($betrokkene_type && $betrokkene_id) {
                my $betrokkene = $c->model('Betrokkene')->get({ type=> $betrokkene_type}, $betrokkene_id);
                
                if($betrokkene) {    
                    $object->{preset_client} = {
                        naam        => $betrokkene->display_name,
                        id          => $preset_client,
                    };
                }
            }
        }
    } elsif($object_type eq 'bag') {
        
    }

    return $object;
}

sub _limit_text {
    my ($self, $text, $maxlength) = @_;

    my @words = split /\b/, $text;
    
    my $limited = '';
    
    foreach my $word (@words) {
        if((length ($limited) + length ($word)) > $maxlength) {
        
            my $difference = $maxlength - length($limited);
            if($difference > 10) {
                $limited .= substr($word, 0, 8) . '...';
            }
            last;
        }
        $limited .= $word;
    }
    
    return $limited;
}

sub _strip_html {
    my ($self, $html) = @_;
    my $tree = HTML::TreeBuilder->new;
    $tree->parse($html);
    $tree->eof();
    return $tree->as_text();
}

Params::Profile->register_profile(
    method  => '_search_natuurlijk_persoon',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_natuurlijk_persoon {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_natuurlijk_persoon" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};

	my $entries = [];

    my $resultset;

	if(length $query > 1 && length $query <= 3) {
		$resultset = $c->model("DB::NatuurlijkPersoon")->search(
			{
				'lower(geslachtsnaam)' => {
					like => lc($query) . '%',
				}
			},
			{
				rows => 5,
				page => 1,
    			order_by => 'me.search_term', 
				prefetch => 'adres_id',
			}
		);
		
	} elsif(length $query > 3) {

        my $query_parts = $self->_parse_query({
            query => $query
        });
        
        my $whereClause = [];
        foreach my $query_part (@$query_parts) {
            push @$whereClause, {
                'lower(search_term)'    => {
                    like    => '%' . lc($query_part) . '%'
                },
            };
        }
        
        $resultset = $c->model("DB::NatuurlijkPersoon")->search({ 
            -and =>	$whereClause,
        }, {
				rows => 5,
				page => 1,
    			order_by => 'me.search_term', 
				prefetch => 'adres_id',
			}
        );
    
    }
	
	if($resultset) {
	    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

		while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'natuurlijk_persoon',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push( @{ $entries }, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'natuurlijk_persoon',
                object      => $object,
            });
		}
	}
	
	return $entries;
}

Params::Profile->register_profile(
    method  => '_search_case_type_document',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_case_type_document {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_case_type_document" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};

    my $entries = [];

    my $resultset;

    if(length $query >= 1) {
        $resultset = $c->model('DB::BibliotheekKenmerken')->search({
            value_type => 'file',
            naam       => {'~*' => $query},
        });
    }
    
    if($resultset) {
        $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

        while (my $result = $resultset->next) {

            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bibliotheek_kenmerken',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push( @{ $entries }, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'bibliotheek_kenmerken',
                object      => $object,
            });
        }
    }
    
    return $entries;
}

Params::Profile->register_profile(
    method  => '_search_searchable_object_type',
    profile => {
        required => [ qw/c query object_type/ ],
        optional => [ qw/extra_params/],
    }
);

sub _search_searchable_object_type {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_searchable_object_type" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};
    my $object_type = $params->{object_type};
    my $extra_params = $params->{extra_params};

    return undef if(length $query < 1);

    my @results = $self->_build_resultset({
        object_type     => $object_type,
        query           => $query,
        c               => $c,
        extra_params    => $extra_params,
        max_items       => 12,
    });
    return \@results;
}

sub _build_resultset {
    my ($self, $params) = @_;
    
    my $object_type  = $params->{object_type};
    my $query        = $params->{query};
    my $c            = $params->{c};
    my $extra_params = $params->{extra_params};
    my $max_items    = $params->{max_items} || 3;

    my $whereClause = $self->_search_parts({
        query   => $query,
    });

    if($object_type eq 'contact') {

        return $self->_search_contacts({
            whereClause => $whereClause,
            c => $c,
            limit => $max_items,
        });

    }
    
    my $model = OBJECTSEARCH_TABLENAMES->{$object_type}->{tablename};
    my $resultset;

    my $where = { -and => $whereClause };
    my $options = {
        rows        => $max_items, 
        order_by    => 'me.search_term', 
    };


    # don't include documents that belong to a deleted zaak
    if($object_type eq 'file') {

        $options->{join} = 'case_id';
        $whereClause->{'case_id.deleted'} = undef;
        $whereClause->{'date_deleted'} = undef;

    } elsif(
        $object_type eq 'kennisbank_producten' || 
        $object_type eq 'kennisbank_vragen'
    ) {

        $whereClause->{'me.deleted'} = undef;
    } elsif(
        $object_type eq 'zaaktype'
    ) {
        $resultset = $c->model('DB::Zaaktype')->search_with_options({
            trigger         => $extra_params->{trigger},
            betrokkene_type => $extra_params->{betrokkene_type},
            term            => $query,
        });
        $resultset = $resultset->search({'me.active' => 1}, {
            order_by    => 'me.id', 
        });         
    }


    if($model eq 'Zaak') {
        $resultset = $c->model('DB::'.$model)->search_extended($where, $options);        
        $resultset = $resultset->search({'me.deleted' => undef});
    } elsif($object_type ne 'zaaktype') {
        $resultset = $c->model('DB::'.$model)->search($where, $options);
    }

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

    my @results = $resultset->all;

    ### Last minute safety checks
    if ($object_type eq 'documents') {
        for (my $i = 0; $i < scalar(@results); $i++) {
            my $value = $results[$i];

            $c->stash->{zaak} = $c->model('DB::Zaak')->find($value->{zaak_id});

            unless (
                $c->check_any_zaak_permission('zaak_read','zaak_beheer','zaak_edit')
            ) {
                $c->log->debug('No permission for case: ' .
                    $c->stash->{zaak}->id
                );

                delete($results[$i]);
            }
        }
    }

    return @results;
}

Params::Profile->register_profile(
    method  => '_search_searchable',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_searchable {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_searchable" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};
    
    return undef if(length $query < 1);

    my $whereClause = $self->_search_parts({
        query   => $query,
    });

    my $resultset_distinct = $c->model("DB::Searchable")->search($whereClause,
    {
        select => ['object_type'],
        distinct => 1,
    });
    $resultset_distinct->result_class('DBIx::Class::ResultClass::HashRefInflator');

    # we want the output ordered. order comes from the constant OBJECTSEARCH_TABLE_ORDER
    # a little perl poetry to accomplish this. 
    my $grouped_results = [];
    
    foreach my $object_type (OBJECTSEARCH_TABLE_ORDER) {
    
        my @results = $self->_build_resultset({
            object_type => $object_type,
            query => $query,
            c => $c,
        });

        push @$grouped_results, @results;
    }

    return $grouped_results;
}

Params::Profile->register_profile(
    method  => '_search_contacts',
    profile => {
        required => [ qw/whereClause c limit/ ],
    }
);

sub _search_contacts {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_contacts" unless $dv->success;

    my $whereClause   = $params->{whereClause};
    my $c   = $params->{c};
    my $limit = $params->{limit};

    my $resultset = $c->model('DB::Searchable')->search({
        -and        =>	$whereClause,
        object_type => { -in => [ qw/natuurlijk_persoon bedrijf/] },
    }, 
    {
        rows        => $limit, 
        order_by    => 'me.search_term', 
    });

    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');
    return $resultset->all;
}

Params::Profile->register_profile(
    method  => '_search_parts',
    profile => {
        required => [ qw/query/ ]
    }
);

sub _search_parts {
    my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_parts" unless $dv->success;

    my $query   = $params->{query};
    
    my $query_parts = $self->_parse_query({
        query => $query
    });

    my $whereClause = [];
    foreach my $query_part (@$query_parts) {
        push @$whereClause, {
            'lower(me.search_term)'    => {
                like    => '%' . lc($query_part) . '%'
            },
        };
    }
    
    return { '-and' => $whereClause };
}

Params::Profile->register_profile(
    method  => '_search_bedrijf',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_bedrijf {
	my ($self, $params) = @_;


    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_bedrijf" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};

	my $entries = [];
	
	my $resultset;
	
	if(length $query > 1 && length $query <= 3) {
		$resultset = $c->model('DB::Bedrijf')->search(
			{
				'lower(handelsnaam)' => {
					like => lc($query) . '%',
				}
			},
			{
				rows => 5,
				page => 1,
			}
		);
		
	} elsif(length $query > 3) {
		my $query_parts = $self->_parse_query({
		    query => $query
		});

		my $whereClause = [];			
		foreach my $query_part (@$query_parts) {
			push @$whereClause, {
				'lower(search_term)'    => {
					like    => '%' . lc($query_part) . '%'
				},
			};
		}
		
		$resultset = $c->model('DB::Bedrijf')->search({ 
			-and =>	$whereClause,
		}, 
		{
			rows => 5, 
			order_by => 'me.handelsnaam', 
			page => 1,
		});
	} 
		
	if($resultset) {
	    $resultset->result_class('DBIx::Class::ResultClass::HashRefInflator');

		while (my $result = $resultset->next) {
		
            my $object = $self->_retrieve_object({
                c               => $c,
                object_type     => 'bedrijf',
                searchable_id   => $result->{searchable_id}
            });

            next unless $object;

            push( @{ $entries }, {
                id          => $result->{searchable_id},
                label       => $result->{search_term},
                object_type => 'bedrijf',
                object      => $object,
            });
			
		}
	}
	
	return $entries;
}

Params::Profile->register_profile(
    method  => '_parse_query',
    profile => {
        required => [ qw/query/ ]
    }
);

sub _parse_query {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _parse_query" unless $dv->success;

    my $query   = $params->{query};

	my $seen = {};
	$query =~ s|\.||gis;
	my @query_parts = sort {length $b <=> length $a}
		grep { !$seen->{$_}++}  
		split /\s/, $query;
	
	return \@query_parts;
}

Params::Profile->register_profile(
    method  => '_get_voorletters',
    profile => {
        required => [ qw/voornamen/ ]
    }
);

sub _get_voorletters {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _get_voorletters" unless $dv->success;

    my $voornamen   = $params->{voornamen};

	my ($firstchar) = $voornamen =~ /^(\w{1})/;
	my @other_chars = $voornamen =~ / (\w{1})/g;

	return join(". ", $firstchar, @other_chars) . (
		($firstchar || @other_chars) ?
		'.' : ''
	);	
}

Params::Profile->register_profile(
    method  => '_search_openbareruimte',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_openbareruimte : Private {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_openbareruimte" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};

	return [] unless $query;
	$query =~ s/.+> //;
	$query =~ s|\,.*||;
	
	my $resultset = $c->model('DB::BagOpenbareruimte')->search(
		{
			'lower(naam)'    => {
				like    => lc($query) . '%'
			}
		},
	);

	my $entries = [];

	while (my $result = $resultset->next) {

		push( @{ $entries },
			{
				id 		=> $result->id,
				label 	=> $result->naam,
				type 	=> 'straatnaam',
				woonplaats => $result->woonplaats->naam,
			},
		);
	}

	return $entries;
}

Params::Profile->register_profile(
    method  => '_search_huisnummer',
    profile => {
        required => [ qw/c query/ ]
    }
);

sub _search_huisnummer : Private {
	my ($self, $params) = @_;

    my $dv = Params::Profile->check(params => $params);
    die "invalid options for _search_huisnummer" unless $dv->success;

    my $c       = $params->{c};
    my $query   = $params->{query};
	
	$query =~ s|\,.*||is;
	my ($straatnaam, $huisnummer) = $query =~ m|^(.*)\s+(\d+)|;
	
	$straatnaam =~ s/.+> //;

	my $straten = $c->model('DB::BagOpenbareruimte')->search(
		{
			'lower(naam)'    => {
				like    => lc($straatnaam) . '%'
			}
		},
	);

	my $entries = [];

	if($straten->count) {
		my $straat  = $straten->first;
	
		my $nas     = $straat->hoofdadressen->search(
			{
				'CAST(me.huisnummer as text)' => {
					like => $huisnummer . '%',
				}
			},
			{
				order_by => 'huisnummer',
			}
		);
	
	
		while (my $na = $nas->next) {

			push( @{ $entries },
				{
					label 		=> $straat->naam . ' ' . $na->nummeraanduiding,
					woonplaats 	=> $straat->woonplaats->naam,
					id    		=> 'nummeraanduiding-' . $na->identificatie,
				},
			);
		}
	}

	return $entries;
}

1;
