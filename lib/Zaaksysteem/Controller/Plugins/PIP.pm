package Zaaksysteem::Controller::Plugins::PIP;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

use File::stat;
use Try::Tiny;

use Zaaksysteem::Constants;

use Zaaksysteem::Exception;

use Data::Dumper;
use JSON;

sub base : Chained('/') : PathPart('pip'): CaptureArgs(0) {
    my ($self, $c) = @_;

    ### ANNOUNCE PIP
    $c->stash->{pip} = 1;
    
    my $saml_state = $c->session->{ _saml } || {};

    ### Make sure we are logged in, or clean everything up, preventing other
    ### problems
    unless (
        $c->session->{pip} &&
        (
            (
                $c->session->{pip}->{ztc_aanvrager} =~
                    /^betrokkene-natuurlijk_persoon/ &&
                $saml_state->{ success }
            ) ||
            (
                $c->session->{pip}->{ztc_aanvrager} =~
                    /^betrokkene-bedrijf/ &&
                $c->model('Plugins::Bedrijfid')->succes
            )
        )
    ) {
        $c->log->debug(
            'Lost digid session, deleting pip: ' .
            ($c->session->{pip}->{ztc_aanvrager}||'-') . ':' .
            $saml_state->{ success } . ':' .
            $c->model('Plugins::Bedrijfid')->succes
        );

        delete($c->session->{pip});
    }

    if (
        !$c->session->{pip} &&
        $c->req->action !~ /pip\/login/
    ) {
        $c->response->redirect($c->uri_for('/pip/login'));
        $c->detach;
    }

    $c->stash->{betrokkene} = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    ) if $c->session->{pip};

    # only do this when actually logged in
    if($c->stash->{betrokkene}) {
        $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search_betrokkene_objects({
            betrokkene => $c->stash->{betrokkene}
        });
    }

    ### Make sure user is logged in with DigID
    $c->stash->{pip_session} = 1 if $c->session->{pip};
    $c->stash->{template_layout} = 'plugins/pip/layouts/pip.tt';
}

sub zaak : Chained('base') : PathPart('zaak'): CaptureArgs(1) {
    my ($self, $c, $zaaknr) = @_;

    unless ($zaaknr =~ /^\d+$/) {
        $c->res->redirect($c->uri_for('/pip'));
        $c->detach;
    }

    $c->stash->{'zaak'} = $c->model('DB::Zaak')->find($zaaknr);

    unless ($c->stash->{zaak}) {
        $c->response->redirect($c->uri_for('/pip'));
        $c->detach;
    }


    ### Security
    unless (
        $c->stash->{zaak}->aanvrager &&
        $c->stash->{zaak}->aanvrager_object->betrokkene_identifier eq
            $c->session->{pip}->{ztc_aanvrager}
    ) {
        $c->res->redirect(
            $c->uri_for('/pip')
        );

        $c->detach;
    }

    if($c->stash->{ zaak }->status eq 'deleted') {
        $c->res->redirect($c->uri_for('/pip'));
        $c->detach;
    }

    if($c->stash->{ zaak }->zaaktype_node_id->prevent_pip) {
        $c->res->redirect($c->uri_for('/pip'));
        $c->detach;
    }

    ### Find fase
    my $fase;
    if (($fase = $c->req->params->{fase}) && $fase =~ /^\d+$/) {
        my $fases = $c->stash->{zaak}
            ->zaaktype_node_id
            ->zaaktype_statussen
            ->search({ status  => $fase });

        $c->stash->{requested_fase} = $fases->first if $fases->count;
    } else {
        $c->stash->{requested_fase} = (
            $c->stash->{zaak}->volgende_fase ||
            $c->stash->{zaak}->huidige_fase
        );
    }

    $c->forward('/zaak/_execute_regels');
    ### Find fase
#    {
#        if ($c->req->params->{fase} =~ /^\d+$/) {
#            my $fases = $c->stash->{zaak}->zaaktype_node_id->zaaktype_statussen->search(
#                {
#                    status  => $c->req->params->{fase}
#                }
#            );
#
#            $c->stash->{requested_fase} = $fases->first if $fases->count;
#        } else {
#            $c->stash->{requested_fase} =
#                $c->stash->{zaak}->huidige_fase;
#        }
#    }
}

=head2 file_search

Searches for zero or more files matching the given parameters. By default only
the last version of a file is returned.

=head3 Arguments

=over

=item case_id [required]

=back

=head3 Location

/pip/file/search/(file_id|case_id)/1234

=head3 Returns

A list containing one or more JSON structures containing the file
properties.
 
=cut

sub file_search : Chained('base') : PathPart('file/search/case_id') : Args() {
    my ($self, $c, $case_id) = @_;

    # Convert to the DB-key for the search
    my @search = $c->model('DB::File')->search({case_id => $case_id, publish_pip => 1});

    # Only return the last versions of files. This could be written shorter, but
    # that would make it (more) unreadable.
    my %seen;
    my @valid_files;

    for my $file (@search) {
        # If a file is deleted -and- not accepted, this document should not
        # be displayed.
        if ($file->date_deleted && !$file->accepted) {
            next;
        }

        my $last_version = $file->get_last_version;
        my $root_id;

        # First version of a file doesn't have a root ID set.
        if ($last_version->root_file_id) {
            $root_id = $last_version->root_file_id->id;
        }
        else {
            $root_id = $last_version->id;
        }

        # Only add root last versions of new root files.
        if (!$seen{$root_id}) {
            push @valid_files, $last_version;
            $seen{$root_id} = 1;
        }
    }

    no strict 'refs';
    *DateTime::TO_JSON = sub {shift->iso8601};
    use strict;

    my @result = map {$_->get_pip_data} @valid_files;

    $c->{stash}->{json} = {
      rows_total => scalar @result,
      result     => \@result,
    };

    $c->forward('Zaaksysteem::View::JSON');
}

=head2 file_create

Create a new file.

=head3 Arguments

=over

=item case_id [required]

Assign this file to a case. When this parameter is ommitted, the file
will show up in the global file queue for later processing.

=item publish_website [optional]

Boolean, decides whether this file may be exported outside of Zaaksysteem.

=item return_content_type

Option to override the content-type this call returns. Mostly for IE-compatibility.

=back

=head3 Location

POST: /pip/file/create

=head3 Returns

A JSON structure containing file properties.

=cut

sub file_create : Chained('base') : PathPart('file/create') {
    my ($self, $c) = @_;

    $c->{stash}->{json_content_type} = $c->req->params->{return_content_type};

    my %optional;
    if ($c->req->params->{case_id}) {
        $optional{case_id} = $c->req->params->{case_id};
    }
    my $upload  = $c->req->upload('file');
    my $subject = $c->session->{pip}->{ztc_aanvrager};

    # Create the DB-entry
    my $result = $c->model('DB::File')->file_create({
        db_params => {
            accepted     => 0,
            created_by   => $subject,
            %optional,
            publish_pip  => 1,
        },
        name              => $upload->filename,
        file_path         => $upload->tempname,
    });

    # Needs standardizing. Possibly in the JSON viewer?
    no strict 'refs';
    *DateTime::TO_JSON = sub {shift->iso8601};
    use strict;
    
    $c->{stash}->{json} = [$result->get_pip_data];
    $c->forward('Zaaksysteem::View::JSON');
}

=head2 get_thumbnail

Returns the thumbnail.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/pip/file/thumbnail/file_id/1234

=head3 Returns

Returns the thumbnail data.

=cut

sub get_thumbnail : Chained('base') : PathPart('file/thumbnail/file_id') : Args() {
    my ($self, $c, $file_id) = @_;

    my ($file) = $c->model('DB::File')->search({id => $file_id});

    if (!$file) {
        throw('/file/get_thumbnail/file_not_found', "File with ID $file_id not found");
    }

    # Generate thumbnail if it doesn't have one yet.
    if (!$file->filestore->thumbnail_uuid) {
        $file->filestore->generate_thumbnail;
    }

    my $thumbnail_path = $file->filestore->get_thumbnail_path;

    $c->serve_static_file($thumbnail_path);
    $c->res->headers->content_length(stat($thumbnail_path)->size);
    $c->res->headers->content_type('image/jpeg');
}

=head2 download

Offers a file up as a download regardless of mimetype/extension.

=head3 Arguments

=over

=item file_id [required]

=back

=head3 Location

/pip/file/download/file_id/1234

=head3 Returns

Returns the file as a download.

=cut

sub document_base : Chained('zaak') : PathPart('document') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    unless($file_id =~ m[\d+]) {
        throw('request/invalid_parameter', 'file_id parameter must be numeric');
    }

    my $file = $c->stash->{ zaak }->files->find($file_id);

    unless($file) {
        throw('request/invalid_parameter', 'file_id parameter did not resolve to a file in the database');
    }

    if($file->deleted_by) {
        throw('file/not_available', 'file_id parameter resolved to a file that was deleted');
    }

    unless($file->filestore->filestat) {
        throw('file/not_available', 'file_id parameter resolved to a file that does not physically exist on this server');
    }

    $c->stash->{ file } = $file;
}

sub download : Chained('document_base') : PathPart('download') {
    my ($self, $c, $format) = @_;

    my $file = $c->stash->{ file };

    my ($path, $mime, $size, $name) = $file->get_download_info($format);

    $c->serve_static_file($path);
    $c->res->headers->content_length($size);
    $c->res->headers->content_type('application/octet-stream');
    $c->res->header('Cache-Control', 'must-revalidate');
    $c->res->header('Pragma', 'private');
    $c->res->header('Content-Disposition', sprintf('attachment; filename="%s"', $name));
}


sub view_element : Chained('zaak'): PathPart('view_element'): Args(1) {
    my ($self, $c, $element) = @_;

    $c->forward('/zaak/view_element', [ $element ]);
}

sub overview : Chained('zaak') : PathPart(''): Args() {
    my ($self, $c) = @_;

    ### Get feedback


    $c->stash->{template} = 'plugins/pip/overview.tt';
}


sub index : Chained('base') : PathPart(''): Args(0) {
    my ($self, $c) = @_;

    my $view = $c->req->params->{view};

    $c->stash->{current_page} = 'zaken';

    $c->stash->{template} = 'plugins/pip/index.tt';

    my $ownid = $c->session->{pip}->{ztc_aanvrager};
    $ownid =~ s/betrokkene-//g;

    my $ownsql = '
        (
            CF.{aanvrager} LIKE "' . $ownid . '-%"
        )
    ';

    my $sql = {
        'zaken'         =>
            $ownsql . ' AND Status!="resolved"',
        'afgehandelde_zaken' =>
            $ownsql . ' AND Status="resolved"',
    };

    ### Asked for a search view?
    if (
        $view || (
            $c->session->{search_query}->{raw_sql} &&
            $c->req->params->{paging_page}
        )
    ) {
        if ($view eq 'zaken') {
            $c->session->{search_query}->{raw_sql} = $sql->{zaken};
        } elsif ($view eq 'afgehandelde_zaken') {
            $c->session->{search_query}->{raw_sql}
                = $sql->{afgehandelde_zaken};
        }
        $c->forward('/search/load_search_results');
        $c->detach;
    }


    $c->log->debug('ZTC Aanvrager: '.$c->session->{pip}->{ztc_aanvrager});
    $c->log->debug('USER-ID: '.Dumper($c->user));

    my $onafgeronde_zaken = $c->model('DB::ZaakOnafgerond')->search(
        {
            betrokkene              => $c->session->{pip}->{ztc_aanvrager}
        },
        {
            join                    => 'zaaktype_id',
            page                    => ($c->req->params->{'page'} || 1),
            rows                    => 10,
        }
    );


    $c->log->debug('COUNT ONAFGEHANDELDE ZAKEN: '.$onafgeronde_zaken->count());


    $c->stash->{'onafgehandelde_zaken_json'} = ();
    while (my $onafgeronde_zaak = $onafgeronde_zaken->next) {
        $c->log->debug('Onafgehandelde zaak tegengekomen!');

        push (@{$c->stash->{'onafgehandelde_zaken_json'}},
            JSON->new->utf8(0)->decode($onafgeronde_zaak->get_column('json_string')));
    }

    my $zaaktype_ids = ();
    for my $onafgerond (@{$c->stash->{'onafgehandelde_zaken_json'}}) {
        push (@{$zaaktype_ids}, $onafgerond->{'zaaktype_id'});
    }

    if (!$zaaktype_ids) {
        $zaaktype_ids = ([]);
    }

    $c->log->debug('Nodeids: ' . Dumper($zaaktype_ids));

    $c->stash->{'onafgeronde_zaken'}         = $onafgeronde_zaken->search;
    $c->stash->{'hostname'}                  = 'dev.zaaksysteem.nl:3011'; # TODO cleanup
    $c->stash->{'display_fields_onafgerond'} = [qw/titel/];

    $c->stash->{zaken}  = $c->model('Zaken')->zaken_pip(
        {
            page                    => ($c->req->params->{'page'} || 1), 
            rows                    => 10,
            betrokkene_type         => $c->stash->{betrokkene}->btype,
            gegevens_magazijn_id    => $c->stash->{betrokkene}->ex_id,
            type_zaken              => ['new', 'open', 'stalled'],
        }
    );
    $c->stash->{afgehandelde_zaken}  = $c->model('Zaken')->zaken_pip(
        {
            page                    => ($c->req->params->{'page'} || 1), 
            rows                    => 10,
            betrokkene_type         => $c->stash->{betrokkene}->btype,
            gegevens_magazijn_id    => $c->stash->{betrokkene}->ex_id,
            type_zaken              => ['resolved', 'overdragen'],
        }
    );
    $c->stash->{'display_fields'} = $c->model('SearchQuery')->get_display_fields({
        pip => 1
    });

    $c->log->debug('COUNT ZAKEN: ' . $c->stash->{zaken}->count);
    $c->log->debug('COUNT AFGEHANDELDE ZAKEN: ' . $c->stash->{afgehandelde_zaken}->count);
}

sub contact : Chained('base') : PathPart('contact'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{current_page} = 'contact';

    my $res = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{'betrokkene'} = $res;

    $c->stash->{template} = 'plugins/pip/contact.tt';
}

sub login : Chained('base') : PathPart('login'): Args() {
    my ($self, $c, $type) = @_;
    my ($bsn, $kvknummer);

    my $saml_state = $c->session->{ _saml } || {};

    ### Type natuurlijk_persoon or bedrijf
    if (!$type) {
        $c->stash->{template} = 'plugins/pip/login_type.tt';

        # Retrieve possible saml error before logging out.
        if($c->session->{ _saml_error }) {
            my %dispatch = (
                'urn:oasis:names:tc:SAML:2.0:status:AuthnFailed' => 'cancelled',
                'urn:oasis:names:tc:SAML:2.0:status:NoAuthnContext' => 'context_insufficient',
                'urn:oasis:names:tc:SAML:2.0:status:PartialLogout' => 'partial_logout',
                'urn:oasis:names:tc:SAML:2.0:status:RequestDenied' => 'denied'
            );

            $c->stash->{ saml_error } = $dispatch{ $c->session->{ _saml_error } } || 'unknown';
        }

        ### Zorg voor een schone start
        $c->logout;
        $c->delete_session;

        # Setup IDPs

        $c->stash->{ idps } = [ $c->model('DB::Interface')->search({ module => 'samlidp' }) ];

        $c->detach;
    }

    ### Just check if user is logged in via digid
    if (
        $type eq 'natuurlijk_persoon'
    ) {
        if (!$saml_state->{ success }) {
            $c->res->redirect($c->uri_for('/auth/saml/' . $c->req->param('idp_id'), {
                verified_url => $c->uri_for('/pip/login/natuurlijk_persoon')
            }));

            $c->detach;
        } else {
            $bsn = $saml_state->{ uid };
        }
    } elsif ($type eq 'bedrijf') {
        if (!$c->model('Plugins::Bedrijfid')->succes) {
            $c->res->redirect($c->uri_for(
                '/auth/bedrijfid',
                {
                    verified_url    => $c->uri_for('/pip/login/bedrijf')
                }
            ));

            $c->detach;
        } else {
            $kvknummer = $c->model('Plugins::Bedrijfid')->login;
        }
    } else {
        $c->res->redirect( $c->uri_for('/pip') );
        $c->detach;
    }


    if ($bsn) {
        $c->log->debug(
            'Request for DigID uid: ' . $bsn
        );

        my $res = $c->model('Betrokkene')->search(
            {
                type    => 'natuurlijk_persoon',
                intern  => 0,
            },
            {
                'burgerservicenummer'   => $bsn
            },
        );

        if ($res->count) {
            my $bo = $res->next;

            if ($bo->gmid) {
                $c->session->{pip}->{ztc_aanvrager} = 'betrokkene-natuurlijk_persoon-'
                    . $bo->gmid;

                $c->push_flash_message('U bent succesvol aangemeld via Digid');
                $c->response->redirect($c->uri_for('/pip'));
                $c->detach;
            }
        }

        $c->log->debug("No subject found with bsn $bsn, logging out. Hint: create a subject with this bsn to log in using this digid account");

        ### Hmm, BSN not found, logout with message
        $c->push_flash_message(
            'U bent succesvol aangemeld, maar helaas kunnen'
            . ' wij geen zaken vinden in ons systeem. Om veiligheidsredenen'
            . ' bent u uitgelogd.'
        );

        $c->res->redirect($c->uri_for('/auth/digid/logout'));
        $c->detach;
    } elsif ($kvknummer) {
        $c->log->debug(
            'Request for Bedrijfid kvknummer: ' . $kvknummer
        );

        my $res = $c->model('Betrokkene')->search(
            {
                type    => 'bedrijf',
                intern  => 0,
            },
            {
                'dossiernummer'   => $kvknummer
            },
        );

        if ($res->count) {
            my $bo = $res->next;

            if ($bo->gmid) {
                $c->log->debug(
                    'Succesvol Bedrijfid kvknummer: ' . $kvknummer
                );
                $c->session->{pip}->{ztc_aanvrager} = 'betrokkene-bedrijf-'
                    . $bo->gmid;

                $c->push_flash_message('U bent succesvol aangemeld via Bedrijfid');
                $c->response->redirect($c->uri_for('/pip'));
                $c->detach;
            }
        }
 
        $c->log->debug("No subject found with bsn $bsn, logging out. Hint: create a subject with this bsn to log in using this digid account");

        ### Hmm, BSN not found, logout with message
        $c->push_flash_message('U bent succesvol aangemeld, maar helaas kunnen'
            . ' wij geen zaken vinden in ons systeem. Om veiligheidsredenen'
            . ' bent u uitgelogd.'
        );
        $c->log->debug(
            'Geen zaken voor Bedrijfid kvknummer: ' . $kvknummer
        );

        $c->res->redirect($c->uri_for('/auth/bedrijfid/logout'));
        $c->detach;
    }
}

sub logout : Chained('base') : PathPart('logout'): Args(0) {
    my ($self, $c) = @_;

    $c->delete_session;

    $c->response->redirect($c->uri_for('/pip'));
}

sub zaaktypeinfo : Chained('zaak'): PathPart('zaaktypeinfo'): Args(0) {
    my ($self, $c) = @_;

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaak/zaaktypeinfo.tt'
}

sub update_feedback : Chained('zaak'): PathPart('update/feedback') : Arg(0) {
    my ($self, $c) = @_;

    $c->res->redirect($c->req->referer);
    $c->detach unless $c->req->params->{note};

    $c->model('DB::Contactmoment')->contactmoment_create({
        type         => 'note',
        subject_id   => $c->stash->{betrokkene}->betrokkene_identifier,
        created_by   => $c->stash->{betrokkene}->betrokkene_identifier,
        case_id      => $c->stash->{zaak}->id,
        medium       => 'webformulier',
        message      => $c->req->params->{note},
    });

    my $event = $c->model('DB::Logging')->trigger('subject/contactmoment/create', {
        component => 'zaak',
        zaak_id => $c->stash->{zaak}->id,
        created_by => $c->stash->{betrokkene}->betrokkene_identifier,
        created_for => $c->stash->{betrokkene}->betrokkene_identifier,
        data => {
            case_id => $c->stash->{zaak}->id,
            content => $c->req->params->{note},
            subject_id => $c->stash->{betrokkene}->betrokkene_identifier,
            contact_channel => 'webformulier'
        }
    });
}

sub update_kenmerk : Chained('zaak'): PathPart('update/kenmerk') : Arg(0) {
    my ($self, $c) = @_;

    if (
        $c->req->params->{update}
    ) {
        my $kenmerk_id      = $c->req->params->{update};
        $kenmerk_id         =~ s/.*?_(\d+)$/$1/;

        my $kenmerk_value   = $c->req->params->{
            $c->req->params->{update}
        };


        ### Retrieve kenmerk
        my $kenmerk = $c->stash->{zaak}
            ->zaaktype_node_id
            ->zaaktype_kenmerken
            ->search({
                'bibliotheek_kenmerken_id'  => $kenmerk_id
            })->first;

        die('PERMISSION DENIED') unless ($kenmerk && $kenmerk->pip_can_change);

        if (!$c->req->params->{confirmed}) {
            $c->stash->{kenmerk}        = $kenmerk;
            $c->stash->{update}         = $c->req->params->{update};
            $c->stash->{kenmerk_value}  = $kenmerk_value;
            $c->stash->{template}       = 'plugins/pip/confirm_update_kenmerk.tt';
            $c->detach;
        } else {
            $c->model('DB')->resultset('ScheduledJobs')->create_task(
                {
                    task                        => 'case/update_kenmerk',
                    bibliotheek_kenmerken_id    => $kenmerk_id,
                    value                       => (
                        $c->req->params->{kenmerk_value} ||
                        ''
                    ),
                    created_by                  => $c->stash
                                                ->{betrokkene}
                                                ->betrokkene_identifier,
                    reason                      => $c->req->params->{toelichting},
                    case                        => $c->stash->{zaak}
                }
            );

            if ($c->req->params->{referer}) {
                $c->res->redirect($c->req->params->{referer});
            } else {
                $c->res->redirect(
                    $c->uri_for('/pip/zaak/' . $c->stash->{zaak}->id)
                );
            }

            $c->detach;
        }
    } else {
        $c->detach;
    }


    $c->forward('Zaaksysteem::View::JSONlegacy');
    $c->detach;
}


sub woz : Chained('base') : PathPart('woz'): Args() {
    my ($self, $c, $subpage, $id) = @_;

    $c->stash->{current_page} = 'woz';

    my $res = $c->model('Betrokkene')->get(
        {},
        $c->session->{pip}->{ztc_aanvrager}
    );

    if (exists($c->req->params->{update})) {
        $res->mobiel($c->req->params->{'npc-mobiel'});
        $res->email($c->req->params->{'npc-email'});
        $res->telefoonnummer($c->req->params->{'npc-telefoonnummer'});
    }

    $c->stash->{betrokkene} = $res;

    # load woz specific settings
    my $settings = $c->model('DB::Settings')->filter({
        filter => 'woz_'
    });

    foreach my $setting (@$settings) {
        $c->stash->{    
            $setting->key
        } = $setting->value;
    }

    if($subpage && $id) {

        my $params = $c->req->params;

        # this must be deleted before production!!
        if(my $easter_owner = $params->{easter_egg_voor_jonas}) {
            $c->stash->{woz_objects} = $c->model('DB::WozObjects')->search({
                owner => $easter_owner,
            });
        } else {
            $c->stash->{woz_objects} = $c->stash->{woz_objects}->search({
                owner => $params->{owner},
                object_id => $params->{object_id},
                id => $id
            });
        }

        if($subpage eq 'report') {
            $c->stash->{extra_body_class} = ' woz-body';
            $c->stash->{layout_type} = 'simple';
            $c->stash->{woz_report} = 1;
            $c->stash->{template} = "plugins/woz/report.tt";
        } else {
            $c->stash->{template} = 'plugins/pip/woz.tt';
        }
    } else {
        $c->stash->{template} = 'plugins/pip/woz.tt';
    }
}




1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut
