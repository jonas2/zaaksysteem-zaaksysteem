package Zaaksysteem::Controller::Search::Chart;

use strict;
use warnings;
use Data::Dumper;
use Time::HiRes qw/gettimeofday tv_interval/;

use parent 'Catalyst::Controller';
use Zaaksysteem::Constants;
use Zaaksysteem::Exception;

use Moose;


sub profile : Local : Args() {
	my ($self, $c, $chart_profile) = @_;

    $c->forward('search/base');

	die "need chart_profile" unless $chart_profile;

    return $self->_search_query()->chart_profile;

}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

