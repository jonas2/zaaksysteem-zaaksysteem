package Zaaksysteem::Controller::Zaak::Export;

use strict;
use warnings;
use Data::Dumper;
use Zaaksysteem::Constants;
use Hash::Merge::Simple qw( clone_merge );

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use XML::Simple;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use File::stat;
use XML::Dumper;
use Encode;
use Params::Profile;

use parent 'Catalyst::Controller';

sub export : Chained('/zaak/base') : PathPart('export') {
    my ( $self, $c) = @_;

    my $zaak = $c->stash->{zaak};
    
    my $file = $zaak->export({filepath=> $c->config->{files}});
    my $filepath = $file->{path}.$file->{filename};
    
    $c->log->debug("exported " . Dumper $file);

    die "file not found: $filepath" unless(-e $filepath);
    $c->serve_static_file($filepath);    

#    system("rm $filepath");
    $c->res->headers->header(
        'Content-Disposition',
        'attachment; filename="' . $file->{filename}
    );
    my $stat = stat($filepath);

    $c->res->headers->content_length( $stat->size );
    $c->res->headers->content_type('application/zip');
    $c->res->content_type('application/zip');
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

