package Zaaksysteem::Controller::Datastore;

use Moose;
use Zaaksysteem::Exception;
use namespace::autoclean;

use Data::Dumper;

BEGIN {extends 'Zaaksysteem::General::ZAPIController'; }

=head1 NAME

Zaaksysteem::Controller::Datastore - ZAPI Controller

=head1 SYNOPSIS

See L<Zaaksysteem::Controller::Sysin>

=head1 DESCRIPTION

Zaaksysteem API Controller for Datastore (Gegevensmagazijn).

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

You can use the C<zapi_form=1> query parameter to retrieve more information
from a function regarding the needed form parameters.

=head1 METHODS

=head2 /datastore [GET READ]

Returns a resultset of loaded interfaces

=cut

use constant DATASTORE_CLASSES => [qw/
    NatuurlijkPersoon
    Bedrijf
    BagLigplaats
    BagNummeraanduiding
    BagOpenbareruimte
    BagPand
    BagStandplaats
    BagVerblijfsobject 
    BagWoonplaats
/];

sub index
    : Chained('/')
    : PathPart('datastore')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{zapi}   = [];
}

sub base
    : Chained('/')
    : PathPart('datastore/search')
    : Args(1)
{
    my ($self, $c, $class) = @_;

    my $classes = DATASTORE_CLASSES;

    # Module/auth allowed check
    $c->assert_any_user_permission('admin');
    if (!grep{$_ eq $class} @{ $classes }) {
        throw 'datastore/class_not_allowed', "Class $class is not allowed for datastore viewing/exporting";
    }

    if (exists $c->req->params->{zapi_crud}) {
        my @columns = _get_columns($c, $class);
        $c->stash->{zapi} = [
            Zaaksysteem::ZAPI::CRUD::Interface->new(
                options     => {
                    select      => 'multi',
                },
                actions => [],
                columns => \@columns,
                url     => '<[cl]>'
            )
        ];
        $c->detach();
    }

    $c->stash->{zapi} = _get_data_for_class($c, $class);
}

sub classes
    : Chained('/')
    : PathPart('datastore/classes')
{
    my ($self, $c) = @_;
    my $classes = DATASTORE_CLASSES;
    $c->stash->{json} = \@$classes;
    $c->forward('Zaaksysteem::View::JSON');
}

sub csv
    : Chained('/')
    : PathPart('datastore/csv')
    : Args(1)
{
    my ($self, $c, $class) = @_;
    my @records = _get_data_for_class($c, $class)->all;
    my @columns = $c->model("DB::$class")->result_source->columns;

    my @active_object_subscription_ids =
        map {
            $_->id
        } $c->model("DB::ObjectSubscription")->search({
            local_table  => $class,
            date_deleted => undef
        })->all();

    my @csv;
    push @csv, \@columns;
    for my $r (@records) {
        my @line;
        for my $c (@columns) {
            push @line, $r->get_column($c);
        }
        my ($has_object_subscription) = grep {$_ eq $r->id} @active_object_subscription_ids;
        if ($has_object_subscription) {
            push @line, 'ja';
        }

        push @csv, \@line;
    }
    push @columns, 'afnemerindicatie';

    $c->stash->{csv} = { data => \@csv };

    my $csv = $c->view('CSV')->render($c, $c->stash);

    $c->res->headers->header( 'Content-Type'  => 'application/x-download' );
    $c->res->headers->header(
        'Content-Disposition'  =>
            sprintf "attachment;filename=%s-%s.csv", ($class, time())
    );
    $c->res->body($csv);
}

sub _get_data_for_class {
    my ($c, $class) = @_;
    my $search_opts = {};



    if ($class eq 'NatuurlijkPersoon') {
        ### Join address
        $search_opts->{prefetch} = 'adres_id';
    }

    return $c->model("DB::$class")->search(
        {
            'me.id' => {
                # Make sure that any objects that are deleted in the database are 
                # excluded from the resultset. This NEEDS to be done beforehand, all
                # alternatives would mean breaking the resultset later on. (And ruining
                # paging, easy exporting.)
                'not in ' => $c->model("DB::ObjectSubscription")->search({
                    local_table  => $class,
                    date_deleted => {'!=', undef}
                })->get_column('id')->as_query 
            }
        },
        $search_opts
    );
}

sub _get_columns {
    my ($c, $class) = @_;

    my @columns = grep {
        $_ ne 'search_index' &&
        $_ ne 'search_term' &&
        $_ ne 'searchable_id' &&
        $_ ne 'object_type' &&
        $_ ne 'adres_id'            ### Special natuurlijkpersoon case
    } $c->model("DB::$class")->result_source->columns;

    ### Skip some internal columns

    # Object subscription isn't an actual column
    my @generic_columns;
    push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id       => 'object_subscription',
        label    => 'afnemerindicatie',
        template => '<[item.object_subscription.external_id]>',
        when     => 'item.object_subscription'
    );

    # Process columns
    for my $col (@columns) {
        push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            id          => 'me.' . $col,
            label       => $col,
            resolve     => $col,
        );
    }

    ### Special NatuurlijkPersoon case
    if ($class eq 'NatuurlijkPersoon') {
        my @adr_columns = grep {
            $_ ne 'search_index' &&
            $_ ne 'search_term' &&
            $_ ne 'searchable_id' &&
            $_ ne 'object_type'
        } $c->model("DB::Adres")->result_source->columns;

        for my $col (@adr_columns) {
            push @generic_columns, Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'adres_id.' . $col,
                label       => $col,
                resolve     => 'adres_id.' . $col,
            );
        }
    }
    return @generic_columns;
}

=head1 AUTHOR

vagrant,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;


