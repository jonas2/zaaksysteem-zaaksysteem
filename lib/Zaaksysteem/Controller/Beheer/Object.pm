package Zaaksysteem::Controller::Beheer::Object;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

sub base : Chained('/') : PathPart('beheer/object') : CaptureArgs(0) {
    my ($self, $c) = @_;
}

sub overview : Chained('base') : PathPart('overview') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/object/overview.tt'
}

# Object search
sub search : Chained('base') : PathPart('search') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/object/search_overview.tt'
}

sub search_base : Chained('base') : PathPart('search') : CaptureArgs(2) {
    my ($self, $c, $local_table, $local_id) = @_;

    $c->stash->{local_table} = $local_table;
    $c->stash->{local_id} = $local_id;
}

sub search_single : Chained('search_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/object/search_single.tt'
}

# Object subscriptions
sub subscriptions : Chained('base') : PathPart('subscriptions') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{ template } = 'beheer/object/subscriptions.tt'
}

# Data store
sub datastore : Chained('base') : PathPart('datastore') : Args(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');

    $c->stash->{ template } = 'beheer/object/datastore.tt'
}

sub subscriptions_base : Chained('base') : PathPart('subscriptions') : CaptureArgs(1) {
    my ($self, $c, $id) = @_;

    $c->stash->{subscription_id} = $id;
}

sub subscription_single : Chained('subscriptions_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ template } = 'beheer/object/subscription_single.tt'
}

1;
