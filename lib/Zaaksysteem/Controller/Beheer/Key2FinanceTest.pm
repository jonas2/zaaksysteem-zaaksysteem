package Zaaksysteem::Controller::Beheer::Key2FinanceTest;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use Text::CSV;
use Data::Dumper;


my @stdnaw_fields = qw/
    STAMNUMMER_BELASTINGEN
    NAAM
    STRAATNAAM_VBL
    HUISNUMMER_VBL
    POSTKODE_NUMERIEK_VBL
    POSTKODE_ALFANUMERIEK_VBL
    NATUURLIJK_NIET_NATUURLIJK
    PLAATSNAAM_VBL
    IND_AUT_INCASSO_TOEGESTAAN
    IND_ADRESGEBRUIK
    VOORLETTERS_VBL
    VOORVOEGSEL_VBL
    HUISLETTER_VBL
    TOEVOEGING_HUISNUMMER_VBL
    AANDUIDING_HUISNUMMER_VBL
    SOFINUMMER
    A_NUMMER
    POSTBUSNUMMER_CP
    ANTWOORDNUMMER_CA
    POSTKODE_NUM_CP
    POSTKODE_ALFA_CP
    PLAATSNAAM_CP
    BUITENLANDS_ADRES1
    BUITENLANDS_ADRES2
    BUITENLANDS_ADRES3
/;

my @stddeb_fields = qw/STAMNUMMER_BELASTINGADM BEDRAG OMSCHRIJVING NOTAGEBONDEN_TEKST AANSLAGNUMMER STAMNUMMER_BELASTINGADM OMSCHRIJVING OMSCHRIJVING_FOUTKODE BTW_KODE TAAK/;


sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    my $cases = $c->model('DB::Zaak')->search({
#        status => { '!=' => undef } 
    });




    my $csv = Text::CSV->new ( { binary => 1, always_quote => 1 } )
        or die "Cannot use CSV: ".Text::CSV->error_diag (); 

    $csv->combine(@stddeb_fields) or die "csv error";
    my $stddeb_csv = $csv->string() . "\n";

    $csv->combine(@stdnaw_fields) or die "csv error";
    my $stdnaw_csv = $csv->string() . "\n";

    while(my $case = $cases->next()) {

        my $stddeb_row = {
    #        BEDRAG => $case->payment_amount,  # to be implemented
            BEDRAG => '1001134.56',
            OMSCHRIJVING => $case->onderwerp,
            NOTAGEBONDEN_TEKST => 'notagebonden_tekst',
            AANSLAGNUMMER => '1122',
            STAMNUMMER_BELASTINGADM =>'stamnumm',
            OMSCHRIJVING => 'omsdhrijcg',
            OMSCHRIJVING_FOUTKODE => 'OMSCHRIJVING_FOUTKODE',
            BTW_KODE => 'btwkode',
            TAAK => 'taak',
            STAMNUMMER_BELASTINGADM => $case->id,
        };

        my @values = map {$stddeb_row->{$_}} @stddeb_fields;

        $csv->combine(@values) or die "csv error";
        $stddeb_csv .= $csv->string() . "\n";

        my $stdnaw_values = $self->make_naw_record($case);
        $csv->combine(@$stdnaw_values);
        $stdnaw_csv .= $csv->string() . "\n";
    }

    $c->stash->{stddeb_csv} = $stddeb_csv;
    $c->stash->{stdnaw_csv} = $stdnaw_csv;
    $cases->reset;

    $c->stash->{cases} = $cases;
    $c->stash->{template} = 'beheer/key2financetest.tt';
}


sub make_naw_record {
    my ($self, $case) = @_;


    my $stdnaw_row = {
        STAMNUMMER_BELASTINGEN => $case->id,
        NAAM => $case->systeemkenmerk('aanvrager_naam'),
        PLAATSNAAM_VBL => $case->systeemkenmerk('aanvrager_woonplaats'),
    };

    my $row = [];
    foreach my $field (@stdnaw_fields) {
        if(exists $stdnaw_row->{$field}) {
            push @$row, $stdnaw_row->{$field};
        } else {
            push @$row, 'todo';
        }
    }

    return $row;
}


1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

