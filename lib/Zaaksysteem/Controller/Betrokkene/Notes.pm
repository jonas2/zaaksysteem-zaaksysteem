package Zaaksysteem::Controller::Betrokkene::Notes;

use Moose;
use Data::Dumper;
use Zaaksysteem::Exception;

BEGIN { extends 'Catalyst::Controller'; }

=head1 create_note

Create a note on a subject object.

=head2 URL construction

B</betrokkene/betrokkene-[type]-[gmid]/notes/create>

=head2 Parameters

=over 4

=item content (string, I<required>)

The text content of the note to be created

=back

=head2 Responses

=over 4

=item HTTP 201

Note was succesfully created. JSON body may be ignored.

=item HTTP 405

Incorrect request method, this URL requires HTTP POST.

=item HTTP 400

No content specified

=back

=head2 Events

=over 4

=item C<subject/note/create>

=back

=cut

sub create_note : Chained('/betrokkene/betrokkene') : PathPart('notes/create') : Args(0) {
    my ($self, $c) = @_;

    unless($c->req->method eq 'POST') {
        throw('request/method', "Invalid request method, should be POST.");
    }

    unless($c->req->param('content')) {
        throw('request/parameter/missing', "Missing request parameter `content`.");
    }

    $c->stash->{ json } = $c->model('DB::Logging')->trigger('subject/note/create', {
        component => 'betrokkene',
        created_for => $c->stash->{ betrokkene }->betrokkene_identifier,
        data => {
            subject_id => $c->stash->{ betrokkene }->betrokkene_identifier,
            content => $c->req->param('content')
        }
    });

    $c->detach('Zaaksysteem::View::JSON');
}

1;

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

