package Zaaksysteem::Controller::Kennisbank;

use strict;
use warnings;
use parent 'Catalyst::Controller';
use Data::Dumper;


use constant KENNISBANK_RELATIES    => [
    'zaaktypen',
    'vragen',
    'producten'
];



=head1 METHODS

=head2 base

Chain: /kennisbank

=cut

sub base : Chained('/') : PathPart('kennisbank') : CaptureArgs(0) { }

=head2 kennisbank

End Chain: /kennisbank

Publieke index voor /kennisbank

=cut

sub kennisbank : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{template} = 'kennisbank/index.tt';
}


sub search_relatie : Chained('base') : PathPart('search') : Args(1) {
    my ($self, $c, $relatie)    = @_;

    my $KENNISBANK_RELATIES     = KENNISBANK_RELATIES;

    if (!$c->req->is_xhr ||
        !grep({ $relatie eq $_ } @{ $KENNISBANK_RELATIES })
    ) {
        $c->response->redirect('/');
        $c->detach;
    }

    $c->stash->{nowrapper}      = 1;
    $c->stash->{template}       = 'kennisbank/search.tt';

    my $params = $c->req->params();

    if (%$params && $params->{search}) {
        
        if($relatie eq 'zaaktypen') {
            $c->forward('/zaaktype/search');
        } elsif($relatie eq 'vragen' || $relatie eq 'producten') {

            $c->stash->{template} = "kennisbank/result.tt";
            
            $c->detach(
                'search_' . $relatie, 
                [ $c->req->params->{term} ]
            );    
        }
    }
}

sub search_zaaktypen : Private {
    my ($self, $c)     = @_;
  
    $c->forward('/zaaktype/search2');
    $c->detach();
}


sub _generic_kennisbank_search : Private {
    my ($self, $c, $table, $searchterm)     = @_;

    my $rv = {};

    my $search_request  = { ilike => '%' .  $searchterm . '%' };

    my $entries         = $c->model('DB::' . $table)->search(
        {
            'naam'      => $search_request,
            'deleted'   => undef,
        },
    );

    $rv->{total}        = $entries->count;

    my $limited_entries = $entries->search({},{ rows => 20 });
    $rv->{count}         = $limited_entries->count;

    $c->stash->{results} = $limited_entries;
}

sub search_producten : Private {
    my $self    = shift;
    my $c       = shift;

    return $c->forward('_generic_kennisbank_search', [ 'KennisbankProducten', @_ ]);
}

sub search_vragen : Private {
    my $self    = shift;
    my $c       = shift;

    return $c->forward('_generic_kennisbank_search', [ 'KennisbankVragen', @_ ]);
}




1;

=head1 INTERNAL

=head2 PATH STRUCTUUR

De structuur van de publieke paden is als volgt

=over 4

=item /kennisbank

Publiek overzicht

=item /kennisbank/product/44/energiebelasting_met_vrijstelling_leverancier/

Product overzicht

=item /kennisbank/vraag/31/sluiten_wij_homo_huwelijken/

Vraag overzicht

=back

=cut

