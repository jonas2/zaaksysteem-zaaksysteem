=head1 NAME

Zaaksysteem::Controller::Sysin - System integration API doc

=head1 SYNOPSIS

 ### Interface settings

 # Creating an interface (POST)
 /sysin/interface/create

 # A list of interfaces (a search) and their metadata (GET)
 /sysin/interface

 # A list of transactions (a search) and their metadata (GET)
 /sysin/transaction

 # View a transaction (GET)
 /sysin/transaction/<ID>

 # Deleting an transaction (POST)
 /sysin/transaction/<ID>/update

=head1 DESCRIPTION

This is the System Integration API documentation. Controller is based on our
ZAPIController.

=head1 SYSIN namespace

Our system integration component of zaaksysteem resides in the sysin namespace
of our module. Every message in and out of our system will run through this
module to keep track of changes when it's send from systems outside
zaaksystee.nl

=head1 SCRUD-H API

For a basic knowledge about our JSON API, please take a look at
L<Zaaksysteem::Manual::API>

=head1 WHERE NEXT?

Now you know the basics about our SYSIN API, please look at the following
sections for detailed usage information.

=head2 INTERFACE

See L<Zaaksysteem::Controller::Sysin::Interface> for an API description
for interface calls

=head2 TRANSACTIONS

See L<Zaaksysteem::Controller::Sysin::Transaction> for an API description
for transaction calls

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

