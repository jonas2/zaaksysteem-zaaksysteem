package Zaaksysteem::Documenten;

use Data::Dumper;
use Moose;

sub _validate {
    my ($self, $params) = @_;

    my $caller_method   = [caller(1)]->[3];

    Params::Profile->register_profile(
        'method'    => [caller(0)]->[3],
        'profile'   => Params::Profile->get_profile(
            method  => $caller_method
        ),
    );

    my $dv = Params::Profile->check(
        params  => $params,
    );

    die(
        $caller_method . ': invalid options:'
        . Dumper([$dv->invalid, $dv->missing])
    ) unless $dv->success;

    my $opts = $dv->valid;

    return $opts;
}


1;
