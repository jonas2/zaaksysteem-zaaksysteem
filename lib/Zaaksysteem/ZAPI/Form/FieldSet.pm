package Zaaksysteem::ZAPI::Form::FieldSet;

use Moose;

use constant NAME_CONSTRAINT    => qr/[a-zA-Z0-9_-]+/;

use constant FIELDSET_PROFILE          => {
    required            => [qw/
        name
        title
    /],
    optional            => [qw/
        description
        actions
        fields
    /],
    constraint_methods  => {
        name            => NAME_CONSTRAINT,
    }
};

=head1 NAME

Zaaksysteem::ZAPI::Form::FieldSet - Construct a container (FieldSet) for fields

=head1 SYNOPSIS

    my $fieldset    = Zaaksysteem::ZAPI::Form::FieldSet->new(
        name        => 'fieldset-gegevens',
        title       => 'Benodigde gegevens',
        description => 'Lorem Ipsum Larieda',
        fields      => [
            Zaaksysteem::ZAPI::Form::Field->new(
                name        => 'subject_type',
                label       => 'Subject Type',
                type        => 'radio',
                required    => 1,
                options     => [
                    {
                        value   => 'extern',
                        label   => 'Extern',
                    },
                    {
                        value   => 'intern',
                        label   => 'Intern'
                    }

                ]
                description => 'Select a subject type'
            )
        ]
    );

    $fieldset->TO_JSON;

=head1 DESCRIPTION

This generates a readable form fieldset for the Angular Forms goodness. Please
don't use this module directly, but use: L<Zaaksysteem::ZAPI::Form>.

=head1 ATTRIBUTES

=head2 name (required)

Input "name" for this fields.

=cut

has 'name'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);

=head2 title (required)

Title for this fieldset

=cut

has 'title'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
    'required'  => 1,
);


=head2 description (optional)

Description for this fieldset

=cut

has 'description'  => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

=head2 fields

The fields for this form. For more info, see L<Zaaksysteem::ZAPI::Form::Field>

=cut

has 'fields'    => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub { return []; }
);

=head2 actions

=cut

has 'actions'    => (
    'is'        => 'rw',
    'isa'       => 'ArrayRef',
    'lazy'      => 1,
    'default'   => sub { return []; }
);

=head1 METHODS

=head2 TO_JSON

Returns a Angular readable json representation of this field

=cut

sub TO_JSON {
    my $self        = shift;

    $self->validate;

    return $self->_object_params;
}

=head2 validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        FIELDSET_PROFILE
    );

    die(
        'Cannot validate form fieldset, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;

}


=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ FIELDSET_PROFILE->{required} },
        @{ FIELDSET_PROFILE->{optional} }
    ) };
}

=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::Form::Field> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut 

1;



