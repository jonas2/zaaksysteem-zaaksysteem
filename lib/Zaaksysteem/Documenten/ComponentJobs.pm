package Zaaksysteem::Documenten::ComponentJobs;

use strict;
use warnings;

use Moose;

use Data::Dumper;

extends 'DBIx::Class';

use constant CONTEXT_MAP    => {
    zaak    => 'zaak_id'
};

sub opts { 
    my $self = shift;

    my $obj = Data::Serializer->new(
        serializer  => 'Storable',
    );

    return $obj->deserialize($self->raw_input);
}

sub accept {
    my $self    = shift;

    my ($document, $context_obj) = $self->_verify_row;

    my $context     = CONTEXT_MAP->{ $self->task_context };
    my $task        = $self->task;

    my $succes;
    eval {
        my $opts = $self->opts;

        if ($context_obj->jobs->$task($opts)) {
            $succes = 1;
        }
    };

    if ($@) {
        warn('Failed running job: ' . $task . ' on context '
            . $context . ': ' . $@
        );

        warn ('Arguments: ' . Dumper($self->opts));

        return;
    }

    if ($succes) {
        $document->queue(undef);
        $document->naam('GEACCEPTEERD: ' . $document->naam);
        $document->update;
    }

    return $succes;
}

sub deny {
    my $self    = shift;
    my $opts    = shift;

    my $document    = $self->documenten->first;

    if ($opts && $opts->{omschrijving}) {
        my $job = $document->job_id;
        $job->bericht(
            $document->job_id->bericht . "\n\n"
            . "Reden weigering:\n"
            . $opts->{omschrijving}
        );
        $job->update;
    }

    $document->queue(undef);
    $document->naam('GEWEIGERD: ' . $document->naam);
    $document->update;
}

sub _verify_row {
    my $self        = shift;

    my $document    = $self->documenten->first;

    unless (
        $self->task_context && 
        CONTEXT_MAP->{ $self->task_context }
    ) {
        die('Unknown context: ' . $self->task_context);
    }

    my $context     = CONTEXT_MAP->{ $self->task_context };

    my $context_obj = ($self->can($context)
        ? $self->$context
        : $document->$context
    );

    unless (
        $context_obj
    ) {
        die ('Context ' . $context . ' does not filled');
    }

    unless (
        $context_obj->can('jobs')
    ) {
        die ('Context ' . $context . ' doest not have a jobs handler');
    }

    unless (
        $context_obj->jobs->can($self->task)
    ) {
        die (
            'Could not find task '
            . $self->task
            . ' on context ' .
            $context
        );
    }

    return ($document, $context_obj);
}

1; #__PACKAGE__->meta->make_immutable;

