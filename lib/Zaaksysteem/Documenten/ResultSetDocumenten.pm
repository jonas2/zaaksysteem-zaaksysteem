package Zaaksysteem::Documenten::ResultSetDocumenten;

use strict;
use warnings;

use Moose;
use Data::Dumper;

use Zaaksysteem::Constants;
use Zaaksysteem::Profiles;

extends qw/DBIx::Class::ResultSet Zaaksysteem::Documenten/;

Params::Profile->register_profile(
    method  => 'create_document',
    profile => PROFILE_DOCUMENT_CREATE,
);

sub create_document {
    my ($self,$args) = @_;

    my $opts        = $self->_validate($args);

    my ($document);

    my $zaak;
    if (
        $opts->{zaak_id} &&
        (
            !(
                $zaak = $self->result_source->schema->resultset('Zaak')->find(
                    $opts->{zaak_id}
                )
            ) ||
            $zaak->is_afgehandeld ||
            $zaak->status eq 'deleted'
        )
    ) {
        delete($opts->{zaak_id})
    }

    $self->result_source->schema->txn_do(sub {
        my $document_args    = {};

        for my $key ($self->result_source->columns) {
            $document_args->{ $key } = $opts->{ $key } if defined
                $opts->{ $key };
        }

        $document     = $self->create($document_args)
            or die('create_docment fail');
    });

    return $document;
}

sub search_active {
    my $self        = shift;

    my $resultset   = $self->search(@_);

    return $resultset->search(
        {
            'me.deleted'    => undef,
            'queue'         => undef,
        },
        {
            prefetch    => ['notitie_id', 'filestore_id', 'job_id'],
            order       => { '-desc' => 'created' },
        }
    );
}

sub search_active_queue {
    my $self        = shift;

    my $resultset   = $self->search(@_);

    return $resultset->search(
        {
            'me.deleted'    => undef,
            'me.queue'      => 1,
        },
        {
            prefetch    => ['notitie_id', 'filestore_id', 'job_id'],
            order       => { '-desc' => 'created' },
        }
    );
}

sub search_active_jobs {
    my $self        = shift;

    my $resultset   = $self->search(
        {
            'me.deleted'    => undef,
            'me.queue'      => 1,
            'store_type'    => 'job',
        },
        {
            prefetch    => ['job_id'],
            order       => { '-desc' => 'created' },
        }
    );

    return $resultset->search(@_);
}

# sub is_alert {
#     my $self    = shift;

#     return $self->search(
#         {
#             deleted     => undef,
#             queue       => 1,
#         }
#     )->count;
# }

1;
