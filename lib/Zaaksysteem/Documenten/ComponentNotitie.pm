package Zaaksysteem::Documenten::ComponentNotitie;

use strict;
use warnings;

use Moose;

use Data::Dumper;

extends 'DBIx::Class';

sub accept {
    my $self    = shift;

    my $document    = $self->documenten->first;

    $document->queue(undef);
    return $document->update;
}

1; #__PACKAGE__->meta->make_immutable;

