package Zaaksysteem::Zaken::Hooks;

use Moose::Role;

with qw/
    Zaaksysteem::Zaken::Hooks::Sysin
/;

=head1 HOOKS

=head2 hook_case_register

Triggers on a case register

=cut

after '_bootstrap'  => sub {
    my $self            = shift;

    $self->hook_case_register();
};

sub hook_case_register {}

=head2 hook_case_advance

Triggers after a case advanced

=cut

sub hook_case_advance {}

after 'advance'     => sub {
    my $self            = shift;
};

1;