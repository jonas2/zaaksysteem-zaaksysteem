package Zaaksysteem::Backend::Filestore::ResultSet;

use strict;
use warnings;

use Digest::MD5::File qw(file_md5_hex);
use File::Basename;
use File::MimeInfo::Magic;
use File::stat;
use Params::Profile;
use Moose;
use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Filestore::ResultSet::Exception::FileType' => {
        isa         => 'Zaaksysteem::Backend::Filestore::ResultSet::Exception',
        description => 'Filetype exception',
        alias       => 'throw_filetype_exception',
    },
);

=head2 filestore_create

Creates a Filestore entry.

=head3 Arguments

=over

=item file_path [required]

The path to the file that needs to be added to the file storage.

=item original_name [required]

The name the file originally has or had. This is always required to prevent
issues where a file storage (uuid-based) file name gets used.

=item ignore_extension [optional]

Does not execute the allowed extension check. Use with care.

=back

=head3 Returns

The newly created Filestore object.

=cut

Params::Profile->register_profile(
    method  => 'filestore_create',
    profile => {
        required => [qw/
            original_name
            file_path
        /],
        optional => [qw/
            ignore_extension
        /]
    }
);

sub filestore_create {
    my $self = shift;
    my $opts = $_[0];

    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/filestore/filestore_create/missing_parameters',
            error => "Missing options: @missing",
        );
    }

    my $db_params;
    my $file_path = $opts->{file_path};

    # Check if the filetype is allowed
    unless ($opts->{ignore_extension}) {
        $self->assert_allowed_filetype($opts->{original_name});
    }

    # Open the file and add it to the filestore
    open(my $file, $file_path) or die "Unable to open file: $file_path";
    my $store = $self->ustore;
    my $uuid  = $store->add($file);
    $db_params->{uuid} = $uuid;

    # File database properties
    my $stat = stat($file);
    $db_params->{md5}      = file_md5_hex($file_path);
    $db_params->{size}     = $stat->size;
    $db_params->{mimetype} = mimetype($file_path);
    $db_params->{original_name} = $opts->{original_name};

    my $create = $self->create($db_params);

    return $create->discard_changes;
}

sub find_by_uuid {
    shift->search({ uuid => shift })->first;
}

=head2 ustore

Get a File::UStore object.

=cut

sub ustore {
    my $self = shift;
    my ($path) = $self->result_source->schema->resultset('Config')
        ->get_value('filestore_location');

    require File::UStore;
    my $store = File::UStore->new(
        path     =>  $path,
        prefix   => 'zs_',
        depth    => 5,
    );
    return $store;
}

=head2 $self->assert_allowed_filetype($filename)

Check whether or not a filetype is allowed within the file storage.

Returns true or error.

=cut

sub assert_allowed_filetype {
    my $self = shift;
    my ($name) = @_;

    my ($extension) = $name =~ qr/(\.[0-9A-Za-z]{1,5}$)/;

    if (!$extension) {
        throw_filetype_exception(
            code  => '/filestore/assert_allowed_filetype/no_extension',
            error => "File does not have an extension: $name",
        );
    }
    
    # Match against lowercase
    $extension      = lc($extension);

    if (!MIMETYPES_ALLOWED->{$extension}) {
        throw_filetype_exception(
            code  => '/filestore/assert_allowed_filetype/extension_not_allowed',
            error => "Bestandstype niet toegestaan: $extension",
        );
    }
    return 1;
}

1;
