package Zaaksysteem::Backend::Contactmoment::Component;

use strict;
use warnings;

use File::Temp qw(tempfile);
use Moose;
use Params::Profile;
use Zaaksysteem::Constants;

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Contactmoment::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Contactmoment::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Contactmoment::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Contactmoment::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Contactmoment::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
);

=head2 $self->create_notitie({$opts})

Creates a contactmoment_notitie entry referencing this contactmoment.

=cut

Params::Profile->register_profile(
    method  => 'create_note',
    profile => {
        required => [qw/
            message
        /],
    }
);

sub create_note {
    my ($self, $opts) = @_;
    
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/contactmoment/create_note/invalid_parameters',
            error => "Invalid options given: @invalid"
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/contactmoment/create_note/missing_parameters',
            error => "Missing options: @missing"
        );
    }

    return $self->result_source->schema->resultset('ContactmomentNote')->create({
        message          => $opts->{message},
        contactmoment_id => $self->id,
    });
}

=head2 $self->create_email({$opts})

Creates a contactmoment_email entry referencing this contactmoment.

=cut


Params::Profile->register_profile(
    method  => 'create_email',
    profile => {
        required => [qw/
            body
            recipient
            subject
        /],
        optional => [qw/
            from
        /]
    }
);

sub create_email {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/contactmoment/create_email/invalid_parameters',
            error => "Invalid options given: @invalid"
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/contactmoment/create_email/missing_parameters',
            error => "Missing options: @missing"
        );
    }

    my ($email_fh, $email_file) = tempfile(SUFFIX => '.email');

    # Add email to Filestore for archival purposes. 
    use Data::Dumper;
    $Data::Dumper::Purity = 1;
    print $email_fh Dumper $opts;
    $email_fh->close;
    my $fs = $self->result_source->schema->resultset('Filestore')->filestore_create({
        file_path        => $email_file,
        original_name    => sprintf("%s.email", $opts->{recipient}),
        ignore_extension => 1,
    });
    unlink($email_file);

    # Truncate subject-string in logging. Will still remain intact in the filestore object.
    $opts->{subject} = sprintf("%.150s", $opts->{subject});

    return $self->result_source->schema->resultset('ContactmomentEmail')->create({
        body      => $opts->{body},
        recipient => $opts->{recipient},
        subject   => $opts->{subject},
        filestore_id     => $fs->id,
        contactmoment_id => $self->id,
    });
}

1;
