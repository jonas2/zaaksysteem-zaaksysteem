package Zaaksysteem::Backend::Case::Action::Component;

use Moose;
use JSON;
use File::Basename;
use Data::Dumper;

BEGIN { extends 'DBIx::Class'; }

use constant SUBCASE_NAMES => {
    deelzaak => 'Deelzaak',
    vervolgzaak => 'Vervolgzaak',
    vervolgzaak_datum => 'Vervolgzaak',
    gerelateerd => 'Gerelateerde zaak'
};

sub TO_JSON {
    my $self = shift;

    return {
        id => $self->id,
        type => $self->type,
        label => $self->mangle_label,
        automatic => $self->automatic ? JSON::true : JSON::false,
        tainted => $self->tainted ? JSON::true : JSON::false,
        description => $self->description,

        # Dude! Nasty!
        # Use Catalyst->uri_for maaaaaaaaaaan!
        url => '/zaak/' . $self->get_column('case_id') . '/action?id=' . $self->id,

        data => $self->mangle_data
    };
}

sub tainted {
    my $self = shift;

    return $self->state_tainted || $self->data_tainted;
}

sub description {
    my $self = shift;

    my %mapping = (
        email => sub { 'E-mail bewerken of direct versturen' },
        allocation => sub { 'Toewijzing bewerken of direct wijzigen' },
        template => sub { 'Sjabloon bewerken of direct aanmaken' },
        case => sub { SUBCASE_NAMES->{ shift->data->{ relatie_type } } . ' bewerken of direct starten' }
    );

    return 'Geen beschrijving voor deze actie' unless exists $mapping{ $self->type };

    return $mapping{ $self->type }->($self);
}

sub mangle_label {
    my $self = shift;

    return $self->label unless($self->type eq 'allocation');

    my $users = Zaaksysteem::Schema->default_resultset_attributes->{ users };

    my $ou = $users->get_ou_by_id($self->data->{ ou_id });
    my $role = $users->get_role_by_id($self->data->{ role_id });

    return sprintf(
        '%s, %s',
        ($ou ? $ou->get_value('ou') : sprintf('afdeling-onbekend(%d)', $self->data->{ ou_id })),
        ($role ? $role->get_value('cn') : sprintf('rol-onbekend(%d)', $self->data->{ role_id }))
    );
}

sub mangle_data {
    my $self = shift;

    my $data = $self->data;

    if ($self->type eq 'email') {
        if ($data->{case_document_attachments}) {
            my @existing_attachments;
            for my $attachment (@{$data->{case_document_attachments}}) {
                my $schema = $self->result_source->schema;
                my $file   = $schema->resultset('File')->search({
                    'case_documents.case_document_id' => [$attachment->{case_document_ids}],
                    date_deleted => undef,
                    accepted     => 1,
                    case_id      => $self->case_id->id,
               }, {
                    join => {case_documents =>'file_id'},
                });
                if ($file->count) {
                    push @existing_attachments, $attachment;
                }
            }
            $data->{case_document_attachments} = \@existing_attachments;
        }
    }

    if ($self->type eq 'template') {
        ($data->{ filename }) = fileparse($data->{ filename }, qr[\.[^.]*]);    
    }

    if($self->type eq 'case') {
        $data->{ description } = SUBCASE_NAMES->{ $data->{ relatie_type } };
    }
    
    return $data;
}


=head1 METHODS

=head2 set_automatic({ automatic => 1 })

Return value: None C<$STRING_RESPONSE>

    $action->set_automatic({ automatic => 1 }

Updates the automatic property of the action. First checks if status
changed. Because of potential frequent updates of this we may end up
with doing the same SQL UPDATE 10 times per page view - therefore the 
current value is first inspected. But maybe DBIX::Class is already
smart enough? Not taking that risk.

B<Parameters>

=over 4

=item automatic

Boolean value that will replace the current value of automatic.

=back
=cut
sub set_automatic {
    my ($self, $options) = @_;

    die "need automatic parameter" unless exists $options->{automatic};

    my $desired = $options->{automatic} ? 1 : 0; # normalize for boolean input
    my $current = $self->automatic || 0;

    if($desired ne $current) {
        $self->automatic($desired);
        $self->update;        
    }
}


1;
