package Zaaksysteem::Backend::Sysin::Modules::SAMLSP;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

extends 'Zaaksysteem::Backend::Sysin::Modules';

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'samlsp';

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_cert',
        type        => 'file',
        label       => 'Public key (zaaksysteem)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_key',
        type        => 'file',
        label       => 'Private key (zaaksysteem)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_contact_email',
        type        => 'text',
        label       => 'Contact e-mailadres',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_contact_name',
        type        => 'text',
        label       => 'Organisatie naam',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_webservice',
        type        => 'text',
        label       => 'Webdienst URL (e.g.: http://customer.zaaksysteem.nl/auth/saml)',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sp_application_name',
        type        => 'text',
        label       => 'Applicatie naam',
        required    => 1,
    ),
];

has 'name'          => (
    is          => 'ro',
    default     => INTERFACE_ID,
    lazy        => 1
);

has 'is_multiple'  => (
    is          => 'ro',
    default     => '0',
    lazy        => 1,
);

has 'interface_config'  => (
    is          => 'ro',
    lazy        => 1,
    default     => sub { return INTERFACE_CONFIG_FIELDS; }
);

has 'is_casetype_interface'  => (
    is          => 'ro',
    default     => '0',
    lazy        => 1,
);

has 'module_type' => (
    is          => 'ro',
    default     => sub { return ['samlsp']; },
    lazy        => 1,
);

1;
