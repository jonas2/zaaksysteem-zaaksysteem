package Zaaksysteem::Backend::Sysin::Modules::BAGCSV;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::CSV
/;

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'bagcsv';

use constant INTERFACE_CONFIG_FIELDS    => [
    # Zaaksysteem::ZAPI::Form::Field->new(
    #     name        => 'interface_how_hot',
    #     type        => 'text',
    #     label       => 'How hot is it in here',
    #     required    => 1,
    # )
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'BAG import via CSV',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['file'],
    is_multiple                     => 1,
    is_manual                       => 1,
    allow_multiple_configurations   => 1,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    parser_options                  => {
        validate_header => 1,
        input_file_only => 1,
    },
    attribute_list                  => [
        {
            external_name   => 'woonplaats_identificatie',
            internal_name   => 'woonplaats_identificatie',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'woonplaats_begindatum',
            internal_name   => 'woonplaats_begindatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'woonplaats_einddatum',
            internal_name   => 'woonplaats_einddatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'woonplaats_naam',
            internal_name   => 'woonplaats_naam',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'woonplaats_status',
            internal_name   => 'woonplaats_status',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'woonplaats_inonderzoek',
            internal_name   => 'woonplaats_inonderzoek',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_identificatie',
            internal_name   => 'openbareruimte_identificatie',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_begindatum',
            internal_name   => 'openbareruimte_begindatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_einddatum',
            internal_name   => 'openbareruimte_einddatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_naam',
            internal_name   => 'openbareruimte_naam',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_type',
            internal_name   => 'openbareruimte_type',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_inonderzoek',
            internal_name   => 'openbareruimte_inonderzoek',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'openbareruimte_status',
            internal_name   => 'openbareruimte_status',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_identificatie',
            internal_name   => 'nummeraanduiding_identificatie',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_begindatum',
            internal_name   => 'nummeraanduiding_begindatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_einddatum',
            internal_name   => 'nummeraanduiding_einddatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_huisnummer',
            internal_name   => 'nummeraanduiding_huisnummer',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_huisletter',
            internal_name   => 'nummeraanduiding_huisletter',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_huisnummertoevoeging',
            internal_name   => 'nummeraanduiding_huisnummertoevoeging',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_postcode',
            internal_name   => 'nummeraanduiding_postcode',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_inonderzoek',
            internal_name   => 'nummeraanduiding_inonderzoek',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_type',
            internal_name   => 'nummeraanduiding_type',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'nummeraanduiding_status',
            internal_name   => 'nummeraanduiding_status',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_identificatie',
            internal_name   => 'verblijfsobject_identificatie',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_begindatum',
            internal_name   => 'verblijfsobject_begindatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_einddatum',
            internal_name   => 'verblijfsobject_einddatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_oppervlakte',
            internal_name   => 'verblijfsobject_oppervlakte',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_status',
            internal_name   => 'verblijfsobject_status',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_inonderzoek',
            internal_name   => 'verblijfsobject_inonderzoek',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'verblijfsobject_gebruiksdoel',
            internal_name   => 'verblijfsobject_gebruiksdoel',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_identificatie',
            internal_name   => 'pand_identificatie',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_begindatum',
            internal_name   => 'pand_begindatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_einddatum',
            internal_name   => 'pand_einddatum',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_status',
            internal_name   => 'pand_status',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_inonderzoek',
            internal_name   => 'pand_inonderzoek',
            attribute_type  => 'defined'
        },
        {
            external_name   => 'pand_bouwjaar',
            internal_name   => 'pand_bouwjaar',
            attribute_type  => 'defined'
        },
    ],
    additional_description  => qq{
        Een voorbeeld CSV kunt u <a href="/examples/csv/bag_csv.csv">hier</a> downloaden
    },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


sub _process_row {
    my $self            = shift;
    my $record          = shift;
    my $params          = shift;

    #warn(Data::Dumper::Dumper($row));
    my $schema          = $record->result_source->schema;

    my @woonplaats_cols = $schema
                        ->resultset('BagWoonplaats')
                        ->result_source
                        ->columns;

    my ($mutations, $narrowest_object) = $schema->resultset('BagWoonplaats')->bag_create_or_update(
        $params,
    );

    $record->preview_string($narrowest_object->to_string) if $narrowest_object;

    $self->process_stash->{row}->{mutations} = $mutations
        if (UNIVERSAL::isa($mutations, 'ARRAY'));

    $record->output('SUCCESS');
}


1;