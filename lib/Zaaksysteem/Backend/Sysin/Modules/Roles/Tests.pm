package Zaaksysteem::Backend::Sysin::Modules::Roles::Tests;

use Moose::Role;
use Zaaksysteem::Exception;
use URI;

use IO::Socket::INET;

=head2 test_host_port($url)

Connection testing. Will get the host and port from the given url, and tries a simple
connect on the host and port.

=cut

sub test_host_port {
    my $self                        = shift;
    my $uri                         = URI->new(shift);

    my $host                        = $uri->host;
    my $port                        = $uri->port;

    my $sock                        = IO::Socket::INET->new(
        PeerPort    => $port,
        PeerAddr    => $host,
        Timeout     => 10,
        Proto       => 'tcp',
    );

    unless ($sock) {
        throw(
            'sysin/modules/test/error',
            'Could not connect with host and port, are you sure your firewall is correct?'
            . ' [' . $host . ':' . $port . ']' 
        );
    }

    #warn('SOCK: ' . $sock->connect($port));
}

1;