package Zaaksysteem::Backend::Sysin::Modules::STUFVBO;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use JSON;

use Zaaksysteem::Exception;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
/;

###
### Interface Properties
###
### Below a list of interface properties, see
### L<Zaaksysteem::Backend::Sysin::Modules> for details.

use constant INTERFACE_ID               => 'stufvbo';

use constant INTERFACE_CONFIG_FIELDS    => [];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'StUF Koppeling VBO',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 1,
    retry_on_error                  => 1,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    trigger_definition  => {
        disable_subscription   => {
            method  => 'disable_subscription',
            #update  => 1,
        },
    },
};

has 'stuf_object_type' => (
    'is'        => 'ro',
    'default'   => 'VBO'
);

has 'stuf_subscription_table' => (
    'is'        => 'ro',
    'default'   => 'BagVerblijfsobject'
);


###
### BUILDARGS
###

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};


=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFVBO - STUFVBO engine for StUF VBO related queries

=head1 SYNOPSIS

    # See testfile:
    # t/434-sysin-modules-stufvbo.t

=head1 DESCRIPTION

STUFPRS engine for StUF VBO related queries

=head1 TRIGGERS



=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_BEDRIJF

Creates a new L<Zaaksysteem::DB::Component::Bedrijf> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $stuf_params         = $object->get_params_for_vbo;

    my $org_rs              = $record
                            ->result_source
                            ->schema
                            ->resultset('BagWoonplaats');

    my ($mutations, $preview, $entry);
    eval {
        ($mutations, $preview, $entry) = $org_rs->bag_create_or_update(
            $stuf_params,
            {
                "return_entry" => 'BagVerblijfsobject'
            }
        );
    };

    if ($@) {
        $self->stuf_throw(
            $object,
            'stuf/parameters_invalid',
            'Parameter failure: ' . $@->message
        );
    }

    $self->process_stash->{row}->{mutations} = $mutations;

    $record->preview_string(
        $preview->to_string
    );

    return $entry;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut


sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $entry               = $self->get_entry_from_subscription(@_, 'BagVerblijfsobject');

    $self->_update_stuf_entry(
        @_, $entry
    );


    return $entry;    
}

sub _update_stuf_entry {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;
    my ($mutations, $preview);

    my $stuf_params         = $object->get_params_for_vbo;

    ($mutations, $preview, $entry)    = $entry
                            ->result_source
                            ->schema
                            ->resultset('BagWoonplaats')
                            ->bag_create_or_update(
                                $stuf_params,
                                {
                                    "return_entry" => 'BagVerblijfsobject'
                                }
                            );

    eval {
        ($mutations, $preview, $entry)    = $entry
                                ->result_source
                                ->schema
                                ->resultset('BagWoonplaats')
                                ->bag_create_or_update(
                                    $stuf_params,
                                    {
                                        "return_entry" => 'BagVerblijfsobject'
                                    }
                                );
    };

    if ($@) {
        $self->stuf_throw(
            $object,
            'stuf/parameters_invalid',
            'Parameter failure: ' . $@->message
        );
    }

    $self->process_stash->{row}->{mutations} = $mutations;

    $record->preview_string(
        $preview->to_string
    );

    return $entry;
}


=head2 DELETE SUBJECT

=head2 stuf_delete_entry

DUMMY

=cut

sub stuf_delete_entry {
    my $self                                = shift;
    my ($record, $object, $subscription)    = @_;


    my $entry                               = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'BagVerblijfsobject',
                                                $subscription
                                            );

    $record->preview_string(
        $entry->hoofdadres->to_string
    );

    $entry->einddatum(DateTime->now->strftime('%Y%m%d'));
    $entry->update;

    $self->_remove_subscription_from_entry($record, $object, $entry);

    return $entry;
}

=head1 INTERNAL METHODDS



=head1 EXAMPLES

See L<SYNOPSIS>

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 CONTRIBUTORS

Rudolf Leermakers

Marco Baan

Dario Gieselaar

Nick Diel

Laura van der Kaaij

Arne de Boer

Nicolette Koedam

Marjolein Bryant

Peter Moen

Michiel Ootjers

Jonas Paarlberg

Jan-Willem Buitenhuis

Martin Kip

Gemeente Bussum

=head1 COPYRIGHT

Copyright (c) 2009, the above named PROJECT FOUNDER and CONTRIBUTORS.

=head1 LICENSE

The contents of this file and the complete zaaksysteem.nl distribution
are subject to the EUPL, Version 1.1 or - as soon they will be approved by the
European Commission - subsequent versions of the EUPL (the "Licence"); you may
not use this file except in compliance with the License. You may obtain a copy
of the License at
L<http://joinup.ec.europa.eu/software/page/eupl>

Software distributed under the License is distributed on an "AS IS" basis,
WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
the specific language governing rights and limitations under the License.

=cut

1;
