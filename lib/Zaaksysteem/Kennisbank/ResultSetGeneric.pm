package Zaaksysteem::Kennisbank::ResultSetGeneric;

use strict;
use warnings;

use Moose;
use Data::Dumper;

use Zaaksysteem::Constants;

extends 'DBIx::Class::ResultSet';

sub find_active {
    my ($self, $id) = @_;

    my $entry   = $self->search(
        {
            '-or'   => [
                'id'    => $id,
                'pid'   => $id
            ],
        },
        {
            rows        => 1,
            order_by    => { '-desc'    => 'id' }
        }
    )->first;

    return unless $entry;

    if ($entry->deleted) {
        my @ids         = ($entry->id);
        if ($entry->pid) {
            push(@ids, $entry->pid->id);
        }

        $entry   = $self->search(
            {
                '-or'   => [
                    { 'id'  => { '-in' => \@ids } },
                    { 'pid' => ($entry->pid ? $entry->pid->id : $entry->id) }
                ],
                deleted => undef,
            },
            {
                rows        => 1,
                order_by    => { '-desc'    => 'id' }
            }
        )->first;
    }

    return unless $entry;

    my $source_name = $self->result_source->name;

    if (my $user = $self->{attrs}->{current_user}) {
        $self->result_source->schema->resultset('Seen')->create(
            {
                component               => $source_name,
                $source_name . '_id'    => ($entry->pid || $id),
                betrokkene_dsn          => 'betrokkene-medewerker-' .
                    $user->uidnumber
            }
        );
    }

    return $entry;
}

my $PROFILE_MAP = {
    'producten' => PROFILE_KENNISBANK_PRODUCT_BEWERKEN,
    'vragen'    => PROFILE_KENNISBANK_VRAAG_BEWERKEN,
};

sub get_edit_profile {
    my $self = shift;

    my $keys = join('|', keys %{ $PROFILE_MAP });

    my ($table_name) = $self->result_source->name =~ m[kennisbank_(${keys})];

    return $PROFILE_MAP->{ $table_name };
}

sub bewerk_entry {
    my ($self, $raw_options)    = @_;
    my ($current_id);

    {
        my $profile = $self->get_edit_profile;

        unless($profile) {
            die('Unable to determine profile for Kennisbank item edit sub');
        }

        Params::Profile->register_profile(
            method      => 'bewerk_entry',
            profile     => $profile
        );
    }

    my $dv      = Params::Profile->check(
        params      => $raw_options
    );

    my $opts = $dv->valid;

    ### Auteur en versie
    $opts->{author}     = $self->_retrieve_author unless $opts->{author};

    my $current_entry;

    if ($opts->{id}) {
        $current_entry  = $self->find($opts->{id});

        $opts->{pid}    = $current_entry->pid;

        $opts->{pid}    = $opts->{id} unless $opts->{pid};
        delete($opts->{id});
    }

    ### Versie terug?
    if (
        $opts->{versie_id} &&
        $current_entry &&
        $current_entry->id != $opts->{versie_id}
    ) {
        return $self->_restore_versie($current_entry,$opts->{versie_id});
    }

    $opts->{versie}     = $self->_calculate_version($current_entry);

    my $entry;
    eval {
        $self->result_source->schema->txn_do(sub {
            $self->_delete_old_versions($current_entry);

            my @columns     = $self->result_source->columns;
            my $createopts  = {};
            for my $column (@columns) {
                next unless defined($opts->{$column});
                $createopts->{$column} = $opts->{$column};
            }
            $entry  = $self->create( $createopts );

            $self->_create_relaties($entry, $opts);
        });
    };

    if ($@) {
        warn(
            'Dikke vette error op '
            . 'DB::Kennisbank::ResultSetGeneric->bewerk_entry: '
            . $@
        );
    }

    return $entry;
}

sub _restore_versie {
    my ($self, $current_entry, $versie_id) = @_;

    return if $current_entry->id == $versie_id;

    my $entry = $self->find($versie_id);

    eval {
        $self->result_source->schema->txn_do(sub {
            $entry->deleted(undef);
            $entry->update;
            $self->_delete_old_version($current_entry);
        });
    };

    if ($@) {
        warn(
            'Dikke vette error op '
            . 'DB::Kennisbank::ResultSetGeneric->_restore_versie: '
            . $@
        );
        return;
    }

    return $entry;

}

sub _create_relaties {
    my ($self, $entry, $opts) = @_;

    for my $relatie (qw/vragen producten zaaktypen/) {
        if (
            !defined($opts->{$relatie}) ||
            !$opts->{$relatie} ||
            $self->result_source->name =~ /$relatie/
        ) { next; }

        my @relatie_ids = (ref($opts->{$relatie}) ? @{ $opts->{$relatie} } :
            $opts->{$relatie});

        my $column = (
            $relatie eq 'zaaktypen'
                ? 'zaaktype_id'
                : 'kennisbank_' . $relatie . '_id'
        );

        $entry->kennisbank_relaties->create(
            {
                $column => $_
            }
        ) for @relatie_ids
    }
}

sub _delete_old_version {
    my ($self, $entry) = @_;

    return unless $entry;

    $entry       = $self->find($entry) unless ref($entry);

    $entry->deleted(DateTime->now());
    $entry->update;
}

sub _delete_old_versions {
    my ($self, $entry) = @_;

    return unless $entry;

    my $versies  = $entry->search_versies->search(
        {
            'deleted'   => undef,
        }
    );

    while (my $versie = $versies->next) {
        $self->_delete_old_version($versie);
    }
}

sub _retrieve_author {
    my $self        = shift;

    my $user        = $self->{attrs}->{current_user};
    return unless $user;

    return 'betrokkene-medewerker-' . $user->uidnumber;
}

sub _calculate_version {
    my ($self, $id) = @_;

    return 1 unless ($id && ref($id));

    my @ids = ($id->id);

    if ($id->pid) {
        push(@ids, $id->pid->id);
    }

    my $nodes = $self->search({
        '-or'   => [
            { 'id'  => { '-in' => \@ids } },
            { 'pid' => ($id->pid ? $id->pid->id : $id->id) }
        ],
    });

    return ($nodes->count + 1);
}

sub find_external {
    my $self    = shift;

    my $result  = $self->find_active(@_) or return;

    return unless (
        !$result->deleted
    );

    return $result;
}

sub search_external {
    my $self    = shift;

    return $self->search(
        {
            deleted     => undef,
        },
        {
            order_by    => { '-desc'    => 'id' }
        }
    );
}

1;
