BEGIN;

-- 1000
CREATE TABLE interface (
    id SERIAL PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    active BOOLEAN NOT NULL,
    case_type_id INTEGER REFERENCES zaaktype(id),
    max_retries INTEGER NOT NULL,
    interface_config TEXT NOT NULL,
    multiple BOOLEAN default 'f' NOT NULL,
    module TEXT NOT NULL
);

CREATE TABLE interface_local_to_remote (
    id SERIAL PRIMARY KEY,
    local_table VARCHAR(100),
    interface_id INTEGER NOT NULL REFERENCES interface(id),
    local_id INTEGER NOT NULL,
    remote_id VARCHAR(150) NOT NULL
);

CREATE TABLE remote_api_keys (
    id SERIAL PRIMARY KEY,
    key VARCHAR(60) NOT NULL,
    permissions TEXT NOT NULL
);

CREATE TABLE transaction (
    id SERIAL PRIMARY KEY,
    interface_id INTEGER NOT NULL REFERENCES interface(id),
    external_transaction_id VARCHAR(250),
    input_data TEXT,
    input_file INTEGER REFERENCES filestore(id),
    automated_retry_count INTEGER,
    date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    date_last_retry TIMESTAMP WITHOUT TIME ZONE,
    date_next_retry TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT input_data_or_input_file check(input_data IS NOT NULL OR input_file IS NOT NULL)
);

CREATE TABLE transaction_record (
    id SERIAL PRIMARY KEY,
    transaction_id INTEGER REFERENCES transaction(id),
    input TEXT NOT NULL,
    output TEXT NOT NULL,
    is_error BOOLEAN DEFAULT 'f' NOT NULL,
    date_executed TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
);

-- 1001
ALTER TABLE transaction ADD COLUMN processed BOOLEAN default 'f';

-- 1002
DROP TABLE IF EXISTS interface_local_to_remote;

CREATE TABLE transaction_record_to_object (
    id SERIAL PRIMARY KEY,
    transaction_record_id INTEGER REFERENCES transaction_record(id) NOT NULL,
    local_table VARCHAR(100) NOT NULL,
    local_id VARCHAR(255) NOT NULL,
    mutations TEXT
);

-- 1003
ALTER TABLE transaction ADD COLUMN date_deleted timestamp without time zone;

ALTER TABLE transaction_record ADD COLUMN date_deleted timestamp without time zone;

ALTER TABLE transaction_record_to_object ADD COLUMN date_deleted timestamp without time zone;

-- 1004
ALTER TABLE transaction ADD COLUMN error_count INTEGER;

-- 1005
ALTER TABLE transaction_record ADD COLUMN preview_string VARCHAR(200);

-- 1006
ALTER TABLE interface ADD COLUMN date_deleted TIMESTAMP WITHOUT TIME ZONE;

-- 1007
CREATE TABLE object_subscription (
    id SERIAL PRIMARY KEY,
    interface_id INTEGER REFERENCES interface(id) NOT NULL,
    external_id VARCHAR(255) NOT NULL,
    local_table VARCHAR(100) NOT NULL,
    local_id VARCHAR(255) NOT NULL,
    date_created TIMESTAMP WITHOUT TIME ZONE default now(),
    date_deleted TIMESTAMP WITHOUT TIME ZONE,
    object_preview TEXT
);

-- 1008
-- Direction: incoming/outgoing.
ALTER TABLE transaction
    ADD COLUMN direction
        CHARACTER VARYING(255)
        NOT NULL
        DEFAULT 'incoming'
        CHECK(direction = 'incoming' OR direction = 'outgoing');


-- last_error: errormessage describing the problem for this row.
ALTER TABLE transaction_record ADD COLUMN last_error TEXT;

-- 1009
ALTER TABLE transaction ADD COLUMN success_count INTEGER ;
ALTER TABLE transaction ADD COLUMN total_count INTEGER ;

ALTER TABLE transaction ALTER COLUMN success_count SET DEFAULT 0;
ALTER TABLE transaction ALTER COLUMN total_count SET DEFAULT 0;
ALTER TABLE transaction ALTER COLUMN error_count SET DEFAULT 0;

-- 1010
ALTER TABLE transaction_record_to_object ADD COLUMN mutation_type VARCHAR(100);

UPDATE transaction_record_to_object SET mutation_type = 'update';

-- 1011
ALTER TABLE transaction ADD COLUMN processor_params TEXT;
ALTER TABLE transaction ADD COLUMN error_fatal BOOLEAN;

-- 1012
CREATE TABLE file_case_document (
    id SERIAL PRIMARY KEY,
    file_id INTEGER NOT NULL REFERENCES file(id),
    case_document_id INTEGER NOT NULL REFERENCES zaaktype_kenmerken(id),
    UNIQUE(file_id, case_document_id)
);

INSERT INTO file_case_document (file_id, case_document_id)
    (SELECT
        id,
        case_type_document_id
    FROM
        file
    WHERE
      case_type_document_id
    IS NOT NULL)
;

ALTER TABLE file DROP COLUMN case_type_document_id;

-- 1013
ALTER TABLE bag_ligplaats ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN documentdatum DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN documentdatum DROP NOT NULL;

ALTER TABLE bag_ligplaats ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN documentnummer DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN documentnummer DROP NOT NULL;

-- 1014
ALTER TABLE bag_woonplaats DROP CONSTRAINT pk_woonplaats;

ALTER TABLE bag_woonplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_woonplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_woonplaats ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_openbareruimte DROP CONSTRAINT pk_openbareruimte;

ALTER TABLE bag_openbareruimte ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_openbareruimte ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_openbareruimte ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_nummeraanduiding DROP CONSTRAINT pk_nummeraanduiding;

ALTER TABLE bag_nummeraanduiding ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_nummeraanduiding ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_nummeraanduiding ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_verblijfsobject DROP CONSTRAINT pk_verblijfsobject;


ALTER TABLE bag_verblijfsobject ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN correctie DROP NOT NULL;
ALTER TABLE bag_verblijfsobject ALTER COLUMN oppervlakte DROP NOT NULL;

ALTER TABLE bag_verblijfsobject ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_pand DROP CONSTRAINT pk_pand;

ALTER TABLE bag_pand ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN bouwjaar DROP NOT NULL;
ALTER TABLE bag_pand ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_pand ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_verblijfsobject_gebruiksdoel DROP CONSTRAINT pk_verblijfsobject_gebrdoel;


ALTER TABLE bag_verblijfsobject_gebruiksdoel ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_verblijfsobject_gebruiksdoel ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_verblijfsobject_pand DROP CONSTRAINT pk_verblijfsobject_pand;

ALTER TABLE bag_verblijfsobject_pand ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_verblijfsobject_pand ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE bag_ligplaats DROP CONSTRAINT pk_ligplaats;


ALTER TABLE bag_ligplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_ligplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_ligplaats ADD COLUMN id SERIAL PRIMARY KEY;


ALTER TABLE bag_standplaats DROP CONSTRAINT pk_standplaats;

ALTER TABLE bag_standplaats ALTER COLUMN officieel DROP NOT NULL;
ALTER TABLE bag_standplaats ALTER COLUMN correctie DROP NOT NULL;

ALTER TABLE bag_standplaats ADD COLUMN id SERIAL PRIMARY KEY;

-- 1015
ALTER TABLE zaak ADD payment_status text;
ALTER TABLE zaak ADD payment_amount numeric(100,2);

-- 1016
ALTER TABLE zaaktype_node ADD COLUMN is_public BOOLEAN DEFAULT 'f';

-- 1017
ALTER TABLE logging ADD COLUMN created_by_name_cache VARCHAR;

-- 1018
ALTER TABLE zaaktype_node ADD COLUMN prevent_pip BOOLEAN DEFAULT 'f';

-- 1020
-- This fixes entries like:
--
--  zaak_id | bibliotheek_kenmerken_id |      value       |  id
--  ---------+--------------------------+------------------+-------
--     2412 |                       24 | ARRAY(0xfc67160) | 65914
--
-- It has already been rollback tested on all production databases.

DELETE FROM zaak_kenmerk WHERE value ~ 'ARRAY\(0x.*\)$';

-- the delay type is influenced by relatie_type. since this can yield a conflicting situation
-- i'm removing 'delay_type'. delay_type was set by commit_session, when saving a new zaaktype
-- version.

ALTER TABLE zaaktype_relatie DROP delay_type;

COMMIT;

BEGIN;

-- handy to have, however, if there's null rows may not work on some db's.
alter table bibliotheek_sjablonen alter filestore_id set not null;

COMMIT;

BEGIN;

-- 1019
-- It's okay if this one fails! Some databases don't have this constraint.
ALTER TABLE zaak_kenmerk DROP CONSTRAINT zaak_kenmerk_zaak_id_key;

COMMIT;
