-- Column: event_type

-- ALTER TABLE logging DROP COLUMN event_type;

ALTER TABLE logging ADD COLUMN event_type character varying(32);

-- Column: event_data

-- ALTER TABLE logging DROP COLUMN event_data;

ALTER TABLE logging ADD COLUMN event_data text;

