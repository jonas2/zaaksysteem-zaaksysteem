ALTER TABLE case_action ADD COLUMN state_tainted boolean DEFAULT false;
ALTER TABLE case_action ADD COLUMN data_tainted boolean DEFAULT false;
