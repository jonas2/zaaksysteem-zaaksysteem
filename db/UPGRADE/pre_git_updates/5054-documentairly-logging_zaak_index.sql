BEGIN;

CREATE INDEX logging_zaak_id_idx
  ON logging
  USING btree (zaak_id);

COMMIT;
