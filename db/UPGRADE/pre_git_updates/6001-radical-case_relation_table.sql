-- Table: case_relation

-- DROP TABLE case_relation;

CREATE TABLE case_relation
(
  id serial NOT NULL,
  case_id_a integer,
  case_id_b integer,
  order_seq_a integer,
  order_seq_b integer,
  type_a character varying(64),
  type_b character varying(64),
  CONSTRAINT case_relation_pkey PRIMARY KEY (id ),
  CONSTRAINT case_relation_case_id_a_fkey FOREIGN KEY (case_id_a)
      REFERENCES zaak (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT case_relation_case_id_b_fkey FOREIGN KEY (case_id_b)
      REFERENCES zaak (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

