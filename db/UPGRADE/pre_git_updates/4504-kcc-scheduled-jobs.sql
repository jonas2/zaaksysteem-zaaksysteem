
CREATE TABLE scheduled_jobs (
    id serial,
    task character varying(255) NOT NULL,
    scheduled_for timestamp without time zone NOT NULL,
    parameters text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone
);


ALTER TABLE ONLY scheduled_jobs
    ADD CONSTRAINT scheduled_jobs_pkey PRIMARY KEY (id);
