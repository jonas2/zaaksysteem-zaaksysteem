BEGIN;

UPDATE zaaktype_status SET fase = 'Registreren' WHERE fase = 'Registratie';
UPDATE zaaktype_status SET fase = 'Afhandelen' WHERE fase = 'Afhandeling';

COMMIT;

