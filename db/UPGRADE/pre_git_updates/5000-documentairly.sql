BEGIN;

--- NEW TABLES/DATA

CREATE TABLE config (
  parameter VARCHAR(100) NOT NULL,
  value     VARCHAR(250) NOT NULL
);

-- Files
CREATE TYPE trust_level AS ENUM ('Openbaar', 'Beperkt openbaar', 'Intern', 'Zaakvertrouwelijk', 'Vertrouwelijk', 'Confidentieel', 'Geheim', 'Zeer geheim');
CREATE TYPE origin AS ENUM ('Inkomend', 'Uitgaand', 'Intern');

CREATE TABLE file_metadata (
  temp_documents_id integer,
  id SERIAL PRIMARY KEY,
  description TEXT,
  document_category TEXT,
  trust_level trust_level,
  origin origin
);

CREATE TABLE directory (
  id SERIAL PRIMARY KEY,
  name VARCHAR(40),
  case_id INTEGER REFERENCES zaak(id) NOT NULL,
  immutable BOOLEAN NOT NULL DEFAULT 'f'
);


-- FIX MIGRATION LATER
ALTER TABLE filestore RENAME TO filestore_old;
ALTER SEQUENCE filestore_id_seq RENAME TO filestore_old_id_seq;
ALTER INDEX filestore_pkey RENAME TO filestore_old_pkey;

UPDATE bibliotheek_sjablonen SET filestore_id = (SELECT id FROM filestore_old WHERE id = filestore_id);

DELETE FROM bibliotheek_sjablonen_magic_string;
DELETE FROM zaaktype_sjablonen;
DELETE FROM bibliotheek_sjablonen;

CREATE TABLE filestore (
  temp_documents_id integer,
  id SERIAL PRIMARY KEY,
  uuid uuid NOT NULL,
  thumbnail_uuid uuid,
  original_name VARCHAR(150) NOT NULL,
  size INTEGER NOT NULL,
  mimetype VARCHAR(160) NOT NULL,
  md5 VARCHAR(100) NOT NULL,
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL
);

CREATE TABLE file (
  temp_documents_id integer,
  -- disabled for now, fix later
  --   search_index TSVECTOR,
  --   search_term TEXT,
  --   object_type CHARACTER VARYING(100) DEFAULT 'file'::character VARYING,
  id SERIAL PRIMARY KEY,
  filestore_id INTEGER REFERENCES filestore(id),
  name VARCHAR(150) NOT NULL,
  extension VARCHAR(10) NOT NULL,
  root_file_id INTEGER REFERENCES file(id),
  version INTEGER DEFAULT 1,
  case_id INTEGER REFERENCES zaak(id),
  metadata_id INTEGER REFERENCES file_metadata(id),
  subject_id VARCHAR(100),
  directory_id INTEGER REFERENCES directory(id),
  case_type_document_id INTEGER REFERENCES zaaktype_kenmerken(id),
  creation_reason VARCHAR(150) NOT NULL,
  accepted_temp INTEGER,
  accepted BOOLEAN NOT NULL DEFAULT 'f',
  rejection_reason TEXT,
  is_duplicate_name BOOLEAN NOT NULL DEFAULT 'f',
  publish_pip BOOLEAN NOT NULL DEFAULT 'f',
  publish_website BOOLEAN NOT NULL DEFAULT 'f',
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  created_by VARCHAR(100) NOT NULL,
  date_modified TIMESTAMP WITHOUT TIME ZONE,
  modified_by VARCHAR(100),
  date_deleted TIMESTAMP WITHOUT TIME ZONE,
  deleted_by VARCHAR(100),
  publish_type_temp TEXT
);

-- Contactmomenten
CREATE TYPE contactmoment_type AS ENUM ('email', 'note');
CREATE TYPE contactmoment_medium AS ENUM ('behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier');

CREATE TABLE contactmoment (
  id SERIAL PRIMARY KEY,
  subject_id VARCHAR(100),
  case_id INTEGER REFERENCES zaak(id),
  type contactmoment_type NOT NULL,
  medium contactmoment_medium NOT NULL,
  date_created TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  created_by VARCHAR(100) NOT NULL
);

CREATE TABLE contactmoment_note (
  id SERIAL PRIMARY KEY,
  message VARCHAR(200),
  contactmoment_id INTEGER REFERENCES contactmoment(id) NOT NULL
);

CREATE TABLE contactmoment_email (
  id SERIAL PRIMARY KEY,
  body_preview TEXT NOT NULL,
  filestore_id INTEGER REFERENCES filestore(id) NOT NULL,
  contactmoment_id INTEGER REFERENCES contactmoment(id) NOT NULL
);

-- MIGRATION OF EXISTING DATA
INSERT INTO filestore (SELECT
  id as temp_documents_id,
  id,
  'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11' as uuid,
  NULL as thumbnail_uuid,
  filename as original_name,
  filesize as size,
  mimetype,
  md5,
  created as date_created
FROM
  documents
WHERE
  documenttype != 'mail' AND
  documenttype != 'sjabloon'
);

INSERT INTO file(SELECT
  d.id as temp_documents_id,
  -- d.search_index,
  -- d.search_term,
  -- d.object_type,
  d.id,
  NULL as filestore_id,
  d.filename as name,
  'WTF' as extension,
  d.pid as root_file_id,
  d.versie as version,
  d.zaak_id as case_id,
  NULL as metadata_id,
  NULL as subject_id,
  NULL as directory_id,
  d.zaaktype_kenmerken_id as case_type_document_id,
  'Migrated' as creation_reason,
  d.queue as accepted_temp,
  'f' as accepted,
  NULL as rejection_reason,
  'f' as is_duplicate_name,
  'f' as publish_pip,
  'f' as publish_website,
  d.created as date_created,
  d.betrokkene_id as created_by,
  d.last_modified as date_modified,
  NULL as modified_by,
  d.deleted_on as date_deleted,
  NULL as deleted_by,
  d.pip as publish_type_temp
FROM
  documents d
WHERE
  d.documenttype != 'mail' AND
  d.documenttype != 'sjabloon'
); 


-- Insert metadata
INSERT INTO file_metadata (SELECT
  id as temp_documents_id,
  id,
  description,
  documenttype as document_type
FROM
  documents
); 

-- Fix metadata_id
UPDATE file SET metadata_id  = (SELECT id FROM file_metadata WHERE temp_documents_id = file.temp_documents_id);
UPDATE file SET filestore_id = (SELECT id FROM filestore WHERE temp_documents_id = file.temp_documents_id);

-- Fix accepted
UPDATE file SET accepted =
  case accepted_temp 
    when 1 then true 
    else false 
  end;

-- Fix publish_type_id
UPDATE file SET publish_pip = 't' WHERE publish_type_temp = 'true';

ALTER TABLE file ALTER COLUMN filestore_id SET NOT NULL;

-- Set foreign key from bibliotheek_sjablonen to filestore
-- (This should already have been there, just fixing)
ALTER TABLE bibliotheek_sjablonen ADD CONSTRAINT bibliotheek_sjablonen_filestore_id FOREIGN KEY (filestore_id) REFERENCES filestore (id);

-- Fix created_by?

-- Fix modified_by?

-- Fix deleted_by? -- no-history user oid? static in de db

-- Fix directory_id?

-- Fix root_file_id (voorheen parent_id, dus die moet later aangepast 
-- worden naar de root file.
    
-- Delete temp columns 
ALTER TABLE file DROP COLUMN accepted_temp ;
ALTER TABLE file DROP COLUMN publish_type_temp;
ALTER TABLE file DROP COLUMN temp_documents_id;
ALTER TABLE file_metadata DROP COLUMN temp_documents_id;
ALTER TABLE filestore DROP COLUMN temp_documents_id;


-- Update foreign key to filestore from bibliotheek_sjablonen
ALTER TABLE bibliotheek_sjablonen DROP CONSTRAINT "bibliotheek_sjablonen_filestore_id_fkey";


-- Up the file ID sequence (so migrations don't break)
SELECT setval('file_id_seq', (SELECT MAX(id) FROM file));

-- Same for file_metadata
SELECT setval('file_metadata_id_seq', (SELECT MAX(id) FROM file_metadata));
-- And filestore
SELECT setval('filestore_id_seq', (SELECT MAX(id) FROM filestore));

-- Cleanup of old tables can be done at a later time. Not recommended to do it right away, too useful for possible patching/fixes or even a rollback.

COMMIT;
