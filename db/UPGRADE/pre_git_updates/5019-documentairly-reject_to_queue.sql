BEGIN;

ALTER TABLE file ADD COLUMN reject_to_queue BOOLEAN DEFAULT 'f';

COMMIT;
