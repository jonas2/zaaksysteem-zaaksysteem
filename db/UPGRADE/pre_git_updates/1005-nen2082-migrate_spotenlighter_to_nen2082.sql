BEGIN;

CREATE TABLE bibliotheek_notificaties (
    id serial,
    bibliotheek_categorie_id integer,
    label text,
    subject text,
    message text,
    created timestamp without time zone,
    last_modified timestamp without time zone,
    deleted timestamp without time zone,
    search_index tsvector,
    search_term text,
    object_type varchar(100) default 'bibliotheek_notificaties'
)
INHERITS (searchable);

ALTER TABLE zaaktype_notificatie
    ADD COLUMN bibliotheek_notificaties_id integer,
    ADD COLUMN behandelaar character varying(255);

ALTER TABLE searchable
    ADD CONSTRAINT searchable_pkey PRIMARY KEY (searchable_id);

ALTER TABLE bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_pkey PRIMARY KEY (id);

ALTER TABLE bibliotheek_notificaties
    ADD CONSTRAINT bibliotheek_notificaties_bibliotheek_categorie_id_fkey FOREIGN KEY (bibliotheek_categorie_id) REFERENCES bibliotheek_categorie(id);

COMMIT;
