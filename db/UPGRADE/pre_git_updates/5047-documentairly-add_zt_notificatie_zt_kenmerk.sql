BEGIN;

CREATE TABLE zaaktype_notificatie_kenmerk (
    id SERIAL PRIMARY KEY,
    zaaktype_notificatie_id INTEGER NOT NULL REFERENCES zaaktype_notificatie(id),
    zaaktype_kenmerken INTEGER NOT NULL REFERENCES zaaktype_kenmerken(id)
);

COMMIT;
