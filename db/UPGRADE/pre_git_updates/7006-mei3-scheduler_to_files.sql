BEGIN;

ALTER TABLE file ADD COLUMN scheduled_jobs_id INTEGER REFERENCES scheduled_jobs(id);

COMMIT;
