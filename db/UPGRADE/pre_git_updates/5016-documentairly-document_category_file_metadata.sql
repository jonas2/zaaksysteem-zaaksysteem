BEGIN;

-- Drop the old implementation
ALTER TABLE file_metadata DROP COLUMN document_category;

-- New structure is nested
ALTER TABLE file_metadata ADD COLUMN document_category_parent VARCHAR(150);
ALTER TABLE file_metadata ADD COLUMN document_category_child VARCHAR(150);

COMMIT;