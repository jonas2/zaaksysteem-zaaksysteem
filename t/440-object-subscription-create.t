#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

$zs->zs_transaction_ok(sub {
    my $result = $schema->resultset('ObjectSubscription')->object_subscription_create({
        interface_id => $zs->create_interface_ok->id,
        external_id  => 'Anystring1234*(#!',
        local_table  => 'Zaak',
        local_id     => $zs->get_case_ok->id,
        object_preview => 'What are we going to do tonight, Brain?'
    });

    ok $result, 'Created ObjectSubscription';
    my @set = ('external_id', 'interface_id', 'local_table', 'local_id', 'object_preview');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }
}, 'interface_create');

$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('ObjectSubscription')->object_subscription_create({})
    }, qr/Validation of profile/, 'No parameters dies';
}, 'interface_create no params');

zs_done_testing();