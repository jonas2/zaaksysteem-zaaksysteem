#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use JSON;

use TestSetup;
use Test::Deep;

my $zs              = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema          = $zs->schema;

BEGIN { use_ok('Zaaksysteem::StUF') };
BEGIN { use_ok('Zaaksysteem::StUF::Stuurgegevens') };

my $VALIDATION_MAP      = {
    'NNP'   => {
        'handelsnaam'               => 'Mintlab B.V. 1',
        'statutaireNaamVennootschapsnaam' => 'Mintlab B.V. 2',
        'zaaknaam'                  => 'Mintlab B.V. 3',
        'handelsRegisternummer'     => '51987465',
    },
    'ADR'   => {
        'postcode'                  => '1051JL',
        'woonplaatsnaam'            => 'Amsterdam',
        'straatnaam'                => 'Donker Curtiusstraat',
        'huisnummer'                => '7',
    },
    # 'PRSHUW'   => {
    #     'a-nummer'                  => '5654321023',
    #     'bsn-nummer'                => '568316589',
    #     'geslachtsnaam'             => 'TestpartnernaamGOOD',
    # },
    # 'PRS_Moved'   => {
    #     'a-nummer'                  => '1234567890',
    #     'bsn-nummer'                => '987654321',
    #     'voornamen'                 => 'Minus',
    #     'voorletters'               => 'M',
    #     'geslachtsnaam'             => 'Mestpersoon',
    #     'geslachtsaanduiding'       => 'M',
    # },

};

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        'share/stuf/nnp/101-nnp-create-mintlab.xml',
    );

    is($stuf->entiteittype, 'NNP', 'Found entiteittype NNP');

    my $params  = $stuf->as_params;
    note(explain($stuf->as_params));

    for my $key (keys %{ $VALIDATION_MAP->{NNP} }) {
        my $givenvalue = $params->{NNP}->{ $key };
        my $wantedvalue = $VALIDATION_MAP->{NNP}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }
    
    #note(explain($params));
    #note(explain($stuf->parser->xml));


}, 'Checked PRS native params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        'share/stuf/nnp/101-nnp-create-mintlab.xml',
    );

    my $params  = $stuf->as_params;

    for my $key (keys %{ $VALIDATION_MAP->{ADR} }) {
        my ($active_rel)    = grep { $_->{is_active} } @{ $params->{NNP}->{NNPADRVBL} };
        my $givenvalue      = $active_rel->{ADR}->{ $key };
        my $wantedvalue     = $VALIDATION_MAP->{ADR}->{ $key };

        is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
    }

}, 'Checked PRS related ADR params');

$zs->zs_transaction_ok(sub {
    my $stuf    = Zaaksysteem::StUF->from_file(
        'share/stuf/nnp/101-nnp-create-mintlab.xml',
    );

    my $params  = $stuf->get_params_for_organization;

    note(explain($params));

    ok($params->{ $_ }, 'Found filled key: ' . $_) for qw/
        dossiernummer
        fulldossiernummer
        handelsnaam
        hoofdvestiging_dossiernummer
        hoofdvestiging_subdossiernummer
        subdossiernummer
        vestiging_adres
        vestiging_huisnummer
        vestiging_postcode
        vestiging_postcodewoonplaats
        vestiging_straatnaam
        vestiging_woonplaats
    /;
    ok ($params, 'Checked: $stuf->get_params_for_natuurlijk_persoon');

}, 'Checked PRS helper functions');

# $zs->zs_transaction_ok(sub {
#     my $stuf    = Zaaksysteem::StUF->from_file(
#         'share/stuf/prs/121-prs-update-tinus.xml',
#     );

#     my $params  = $stuf->as_params;

#     for my $key (keys %{ $VALIDATION_MAP->{PRS_Moved} }) {
#         my $givenvalue = $params->{PRS}->{ $key };
#         my $wantedvalue = $VALIDATION_MAP->{PRS_Moved}->{ $key };

#         is($givenvalue, $wantedvalue, 'Correct value for: ' . $key);
#     }

#     ok(
#         !exists($params->{PRS}->{geboortedatum}),
#         'Geboortedatum ignored'
#     );
#     ok(
#         (
#             exists(
#                 $params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}
#             ) && !defined($params->{PRS}->{PRSADRCOR}->[0]->{ADR}->{huisnummertoevoeging}),
#         ),
#         'huisnummertoevoeging given, but set to undef'
#     );

#     #note(explain($params));

# }, 'Checked PRS Wijziging');


zs_done_testing;