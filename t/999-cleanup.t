#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

remove_test_db_ok(connect_test_db_ok());

$zs->empty_filestore_ok();

zs_done_testing();
