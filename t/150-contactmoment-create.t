#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $message = 'Hallo, ik ben een bericht.';
    my $result = $schema->resultset('Contactmoment')->contactmoment_create({
        type => 'note',
        subject_id => $zs->get_subject_ok,
        created_by => $zs->get_subject_ok,
        medium     => 'balie',
        case_id    => $zs->get_case_ok,
        message    => $message,
    });
    ok $result, 'Created note contactmoment';
    my @set = ('case_id', 'type', 'medium', 'date_created', 'created_by');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }
    is $result->contactmoment_notes->count, 1, 'Got 1 note';
    is $result->contactmoment_notes->first->message, $message, 'Notitie has correct message';
}, 'contactmoment_create note');


$zs->zs_transaction_ok(sub {
    my @mediums = ('behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier');
    for my $m (@mediums) {
        my $result = $schema->resultset('Contactmoment')->contactmoment_create({
            type       => 'note',
            subject_id => $zs->get_subject_ok,
            created_by => $zs->get_subject_ok,
            medium     => $m,
            case_id    => $zs->get_case_ok,
            message    => 'Hoi.',
        });
        ok $result, "Created note contactmoment with medium: $m";
    }
}, 'contactmoment_create note all mediums');


$zs->zs_transaction_ok(sub {
    my $email = {
        body      => 'Hallo, ik ben een email.',
        recipient => 'ontvanger@mintlab.nl',
        from      => 'jim@mintlab.nl',
        subject   => 'Supplies',
    };
    my $result = $schema->resultset('Contactmoment')->contactmoment_create({
        type       => 'email',
        subject_id => $zs->get_subject_ok,
        created_by => $zs->get_subject_ok,
        medium     => 'balie',
        case_id    => $zs->get_case_ok->id,
        email      => {
            %{$email}
        },
    });
    ok $result, 'Created email contactmoment';
    my @set = ('case_id', 'type', 'medium', 'date_created', 'created_by');
    for my $s (@set) {
        ok $result->$s, "$s is set";
    }
    is $result->contactmoment_emails->count, 1, 'Got 1 email';
    is $result->contactmoment_emails->first->body, $email->{body}, 'Body matches';
    is $result->contactmoment_emails->first->subject, $email->{subject}, 'Subject matches';
    is $result->contactmoment_emails->first->recipient, $email->{recipient}, 'Recipient matches';
}, 'contactmoment_create email');


$zs->zs_transaction_ok(sub {
    my @mediums = ('behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier');
    for my $m (@mediums) {
        my $result = $schema->resultset('Contactmoment')->contactmoment_create({
            type       => 'note',
            subject_id => $zs->get_subject_ok,
            created_by => $zs->get_subject_ok,
            medium     => $m,
            case_id    => $zs->get_case_ok,
            message    => 'Hoi.',
        });
        ok $result, "Created note contactmoment with medium: $m";
    }
}, 'contactmoment_create email all mediums');


zs_done_testing();
