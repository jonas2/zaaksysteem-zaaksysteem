#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    throws_ok(
        sub {
            $schema->resultset('ScheduledJobs')->create_task()
        },
        qr/input error: no task/,
        'validation fail: need at least a task'
    );


    throws_ok(
        sub {
            $schema->resultset('ScheduledJobs')->create_task(
                {
                    task => 'case/update_kenmerkINVALID',
                }
            )
        },
        qr/input error: invalid task/,
        'validation fail: need a valid task'
    );
}, 'Validation tests');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    throws_ok(
        sub {
            $schema ->resultset('ScheduledJobs')
                    ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        created_by => $zs->get_subject_ok,
                    }
            )
        },
        qr/ScheduledJobs\[.*Profile Error/,
        'validation fail: need a valid task'
    );

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    is($task->task, 'case/update_kenmerk', 'Found task in db');
    is($task->schedule_type, 'manual', 'schedule_type: manual');
    ok(!$task->scheduled_for, 'schedule_for: not set');
    is($task->parameters->{value}, "Completely new value", 'Found value');
    is(
        $task->parameters->{bibliotheek_kenmerken_id},
        9,
        'Found bibliotheek_kenmerken_id'
    );

    my $caseid = $case->id;
    is(
        $task->parameters->{case_id}, 
        $case->id,
        'Found case_id'
    );
}, 'Create manual task: update_kenmerk');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ### Run task
    ###
    ok($task->run, 'Ran task');

    ### Check kenmerk
    my $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    is($task->parameters->{value}, $kenmerk->value, 'Task run: value set');
}, 'Manual task: Run update_kenmerk');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ### Create empty task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => '',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ### Run task
    ###
    ok($task->run, 'Ran task');

    ### Check kenmerk
    my $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    ok(!$kenmerk, 'Task run: empty set');

    ### Create empty task
    ###
    $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'bla',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ### Run task
    ###
    ok($task->run, 'Ran task');

    ### Check kenmerk
    $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    is($kenmerk->value, 'bla', 'Double run: 1 found kenmerk');

    $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => '',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    ok($task->run, 'Ran empty task');

    ### Check kenmerk
    $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    ok(!$kenmerk, 'Double run: 2 kenmerk unset');
}, 'Manual task: Run update_kenmerk / empty value');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    is($task->task, 'case/update_kenmerk', 'Found task in db');
    ok($task->reject, 'Rejected task');
    ok($task->deleted, 'Task deleted');
}, 'Manual task: Reject update_kenmerk');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;

    ### Create task
    ###
    my $task    =  $schema
                ->resultset('ScheduledJobs')
                ->create_task(
                    {
                        task => 'case/update_kenmerk',
                        bibliotheek_kenmerken_id => '9',
                        value => 'Completely new value',
                        case  => $case,
                        created_by => $zs->get_subject_ok,
                    }
                );

    is($task->task, 'case/update_kenmerk', 'Found task in db');

    my $file = $case->files->search({ scheduled_jobs_id => $task->id })->first;

    ok($file, 'Found file belonging to manual task');
    my $opts = {
        accepted    => 1,
        subject     => $zs->get_subject_ok()
    };

    ok(
        $file->update_properties($opts),
        'Accepted file'
    );

    ### Check kenmerk
    my $kenmerk = $case->zaak_kenmerken->search(
        {
            bibliotheek_kenmerken_id => 9,
        }
    )->first;

    note($file->metadata_id->description);

    is($task->parameters->{value}, $kenmerk->value, 'Task run: value set');
}, 'task: update_kenmerk: accept by file');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok();

    ### Create 3 tasks
    ###
    $schema ->resultset('ScheduledJobs')
            ->create_task(
            {
                task => 'case/update_kenmerk',
                bibliotheek_kenmerken_id => '9',
                value => 'Completely new value',
                case  => $case,
                created_by => $subject,
            }
        ) for (1..3);

    my $tasks = $schema ->resultset('ScheduledJobs')
                        ->search_manual_tasks_with_case_id(
                            {
                                task        => 'case/update_kenmerk',
                                case_id     => $case->id,
                                created_by  => $subject,
                            }
                        );

    is($tasks->count, 3, 'Found 3 task');
}, 'task: update_kenmerk: search by case_id');


$zs->zs_transaction_ok(sub {
    my $case    = $zs->get_case_ok;
    my $subject = $zs->get_subject_ok();

    ### Create 3 tasks
    ###
    my $task = $schema ->resultset('ScheduledJobs')
            ->create_task(
            {
                task => 'case/update_kenmerk',
                bibliotheek_kenmerken_id => '9',
                value => 'Completely new value',
                case  => $case,
                created_by => $subject,
            }
        );

    $task->run();

    my $log    = $schema
                ->resultset('Logging')
                ->search(
                    {
                        event_type => 'scheduler/run'
                    }
                )->first;


    ok($log, 'Found log');
    like($log->onderwerp, qr/wijzigingsvoorstel/, 'Found log string');
}, 'task: update_kenmerk: check log fields');


zs_done_testing();