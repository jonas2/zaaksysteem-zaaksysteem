#! perl

### This TEST script tests the default CSV implementation

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use_ok('Zaaksysteem::Backend::Sysin::Modules::Key2Finance');

# my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
# my $schema = $zs->schema;

# $zs->zs_transaction_ok(sub {
#     my $m = Zaaksysteem::Backend::Sysin::Modules::Key2Finance->new;

#     my $interface = $schema ->resultset('Interface')
#             ->interface_create(
#                 {
#                     module          => 'key2finance',
#                     name            => 'BAG Import'
#                 },                                
#             );

#     ## /sysin/interface/12345/trigger/
#     my $transaction = $interface->process_trigger('set_case_list');

#     ok($transaction->transaction_records->count, 'Found transaction ID');

# }, 'Tested: Key2Finance processing');

zs_done_testing();