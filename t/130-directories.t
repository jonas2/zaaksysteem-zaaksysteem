#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end


$zs->zs_transaction_ok(sub {
    my $test_dir = 'Test Directory';

    my $case = $zs->get_case_ok;

    my $result = $schema->resultset('Directory')->directory_create({
        case_id  => $case->id,
        name     => $test_dir,
    });

    ok $result, "Created directory $result";
    is $result->name, $test_dir, "Directory name is $test_dir";
    is $result->case->id, $case->id, "Directory case is the correct case";
}, 'directory_create()');


$zs->zs_transaction_ok(sub {
    my $test_dir  = 'Test Directory';
    my $directory = $zs->create_directory_ok;
    
    throws_ok sub {
        $schema->resultset('Directory')->directory_create({
            case_id => $directory->case->id,
            name    => $directory->name,
        });
    }, qr/Found existing/, "Duplicate directory names within same case fails";
}, 'directory_create() duplicate entry');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->get_case_ok;
    my $directory = $zs->create_directory_ok;
    my $rename    = 'Renamed Directory';

    my $result = $directory->update_properties({
        name    => $rename,
        case_id => $case->id,
    });
    ok $result, 'Updated directory';
    is $result->name, $rename, 'Directory is renamed';
    is $result->case->id, $case->id, 'New case ID set';
}, 'update_properties rename + assign case');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->get_case_ok;
    my $directory = $zs->create_directory_ok;
    my $rename    = 'Renamed Directory';

    my $result = $directory->update_properties({
        name    => $rename,
        case_id => $case->id,
    });

    my $second_directory = $zs->create_directory_ok;
    is $second_directory->update({case => $directory->case})->case->id,
       $directory->case->id,
       'Set second directory to same case ID';

    throws_ok sub {
        $second_directory->update_properties({
            name => $rename,
        })
    }, qr/Directory with name $rename and case/,
        'Renaming into existing name+case combination fails';
}, 'update_properties existing name(+case) fails');


$zs->zs_transaction_ok(sub {
    my $case      = $zs->get_case_ok;
    my $directory = $zs->create_directory_ok;

    my $second_directory = $zs->create_directory_ok;
    is $second_directory->update({name => $directory->name})->name,
       $directory->name,
       'Set second directory to same case name';

    throws_ok sub {
        $second_directory->update_properties({
            case_id => $case->id,
        })
    }, qr/Directory with name.*and case.*exists/,
        'Renaming into existing case+name combination fails';
}, 'update_properties existing case(+name) fails');


$zs->zs_transaction_ok(sub {
    my $directory = $zs->create_directory_ok;
    my $result    = $directory->delete;
    ok $result, 'Deleted directory';
}, 'delete()');


$zs->zs_transaction_ok(sub {
    my $directory = $zs->create_directory_ok;
    my $file      = $zs->create_file_ok;
    ok $file->update({directory => $directory}), 'Set directory on file';
    throws_ok(sub {
        $directory->delete;
    }, qr/There are still files/, 'delete() failed');
}, 'delete() on non-empty directory');


$zs->zs_transaction_ok(sub {
    throws_ok sub {
        $schema->resultset('Directory')->directory_create()
    }, qr/invalid input/, 'directory_create() noargs';
    throws_ok sub {
        $zs->create_directory_ok->update_properties();
    }, qr/invalid input/, 'update_properties() noargs';
}, 'noargs calls');

zs_done_testing();
