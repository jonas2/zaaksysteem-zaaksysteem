#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';

use TestSetup;

my $zs = bless {schema => connect_test_db_ok()}, 'Zaaksysteem::TestUtils';
my $schema = $zs->schema;
### Test header end

use Cwd;
use File::Basename;

BEGIN { use_ok('Zaaksysteem::SBUS'); }

my $currentdir  = getcwd();

my $xmlo        = Zaaksysteem::SBUS::Types::StUF::XML->new(
    home        => $currentdir,
);

note 'Parse incoming XML';
can_ok($xmlo, 'transform_to_perl', 'transform_to_xml');

note 'Correct XML TO HASH tranformation';
{
    $xmlo->xmlin(get_xml_from_file(
        $currentdir .
        '/t/inc/API/StUF/in-prs-start.xml'
    ));

    my $hash    = $xmlo->transform_to_perl;

    ok($hash->{content}->{body}, 'Got body');
    ok($hash->{content}->{stuurgegevens}, 'Got stuurgegevens');
}

note 'Correct HASH TO XML transformation';
{
    $xmlo->perlin(
        {
            '{http://www.egem.nl/StUF/StUF0204}bevestigingsBericht' => {
                stuurgegevens   => {
                    'berichtsoort'      => 'Bv01',
                    entiteittype        => 'PRS',
                    'tijdstipBericht'   => '2009040717084815',
                    'sectormodel'       => 'BG',
                    'versieSectormodel' => '0204',
                    'versieStUF'        => '0204',
                    'zender'            => {
                        'applicatie'        => 'zaaksysteem.nl',
                    },
                    'referentienummer'  => '2322232323',
                    'ontvanger'         => {
                        'applicatie'        => 'ONBEKEND'
                    },
                    'kennisgeving'      => {
                        'mutatiesoort'      => 'T'
                    }

                }
            },
        }
    );

    my $xml = $xmlo->transform_to_xml->toString;

    like($xml, qr/bevestigingsBericht/, 'Found bevestigingsbericht');
}

sub get_xml_from_file {
    my $filename    = shift;
    my $rv          = '';

    open(my $fh, "<:encoding(UTF-8)", $filename) or die('Cannot open file');

    while (<$fh>) {
        $rv .= $_;
    }

    close($fh);

    return $rv;
}

zs_done_testing();