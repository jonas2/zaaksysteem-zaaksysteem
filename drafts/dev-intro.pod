=head1 Zaaksysteem introduction

This document describes a generic approach for feature development in
L<Zaaksysteem|http://www.zaaksysteem.nl>.

=head1 Case

Let's imagine making a simple feature, CD collection management. We'll follow
a simple phased plan, starting with...

=head1 The database model

First things first, what do we want our representative entity to encapsulate?
Let's take it easy for now, and imagine the CDs in our collection just have a
name, an artist and a barcode.

    ,----------------.
    | table: cds     | 
    |----------------|
    | field: id      | # pretty much all tables have an id field
    | field: public  | # boolean whether this cd is public
    | field: name    |
    | field: artist  |
    | field: barcode |
    `----------------'

As you can see, real simple stuff. Next we'll need to get this into a very real
PostgreSQL database. Zaaksysteem's flow is a bit different from the usual MVC
framework, where it's the rule you first create a schema, and let the framework
work out the gory SQL details, we'll reverse this.

=head2 SQL base

    CREATE TABLE cd (
        id integer NOT NULL,
        public boolean,
        name character varying(128),
        artist character varying(128),
        barcode character varying(12)
    )

While the barcode is a number of sorts, it has no meaning as such, it is just a
string of digits identifying a product in the UPC-A barcode format, that's why
we'll store it as a string.

Execute this on one of the available PostgreSQL servers and we can continue with
the next step.

=head2 Rebuilding the model classes

Rebuilding all models is handled through a simple script which can be located at
/path/to/zaaksysteem/I<dev-bin/db_redeploy.sh>

At the moment this is a pretty hardcoded script, and you will need to make sure
the script updates from the correct database. Adjust the DSN in in the line
containing the C<zaaksysteem_create.pl> call.

Simply run the script like so:

    $ cd /path/to/zaaksysteem
    $ ./dev-bin/db_redeploy.sh

And you can check that everything went as planned by inspecting your new model
module in /path/to/zaaksysteem/I<lib/Zaaksysteem/Schema/Cds.pm>.

=head1 Building a controller

As Zaaksysteem is developed in a relatively plain MVC framework, we'll need a 
controller in order for the user to interact with the model, let's construct
an example controller that highlights some of the basic features of Catalyst.

=head2 Generating the required files

Catalyst provides a bunch of scripts to help speed things along, we don't use
all generated files. For example, we don't use the generated tests (yet), so
don't commit those to the repository after you're done.

    $ cd /path/to/zaaksysteem
    $ ./script/zaaksysteem_create controller Cd

This will create a stock controller in
/path/to/zaaksysteem/I<lib/Zaaksysteem/Controller/Cd.pm> with base POD entries
and a intro sub. Usually we'll want to create a C<base> action that sets up
the environment for 'real' requests that enter. Usually you'll want to stash
resultsets and other information needed in the bulk of chained actions.

    # lib/Zaaksysteem/Controller/Cd.pm
    sub base : Chained('/') : PathPart('cd') : CaptureArgs(1) {
        my ($self, $c, $id) = @_;

        $c->stash->{cd} = $c->model('DB:Cd')->find($id);
    }

    sub view : Chained('base') : PathPart('view') : Args() {
        $c->stash->{template} = 'cd/view.tt';
    }

There we go, now you're thinking with portals. This will create a route that
matches requests according to the following pattern: http://<host>/cd/<id>/view
which, as you probably guessed, allows the end user to simply view a cd in the
database.

So what if the user wants to edit the item instead? Easy peasy, add a edit sub
like so:

    # lib/Zaaksysteem/Controller/Cd.pm
    sub edit : Chained('base') : PathPart('edit') : Args() {
        $c->stash->{template} = 'cd/edit.tt';
    }

As you can see, simpler controllers are just plain pleasant to work with. We
want to keep controllers easy to work with, so less is more. If it makes sense
to chain controllers, don't hesitate to do so.

=head2 Extending the CD ResultSet

If you look back at the data model, you'll notice a field called 'public' which
we've defined as a boolean. This field basically dictates whether the item is
publically listed or not. Let's build an extension of the base ResultSet from
DBIx:Class that takes this into consideration. As a rule of thumb, if you notice
yourself building specific queries or otherwise constrained resultsets, it's a
good idea to abstract that behaviour away in a table specific resultset. This 
ensures we're not cargo-culting queries and eases maintenance of the system as
a whole.

    # lib/Zaaksysteem/DB/ResultSet/Cd.pm
    package Zaaksysteem::DB::ResultSet::Cd;

    use Moose;

    BEGIN { extends 'DBIx::Class::ResultSet'; }

    sub find {
        my ($self, $id) = @_;

        $rs = $self->c->model('Cd')->find($id);
        $rs->public(1);

        return $rs;
    }

=head1 A simple view for our CD collection

Okay, we've arrived at the least techy part of the introduction, and also the
part with the least regulartory flow of stuff happening. It might be that a
design has already 'been created, or that you're the first to touch the
template. If it's the case that there's no design, it's not unusual to create
the template file, commit the stub, and let design know there's something to
work on.

For now, let's assume there's no design available and we're tasked with
building a stock template. This doesn't mean we're to design it, or make sure
it looks the part, just get the data in there, and structure the HTML in a
basic way.

    # root/tpl/zaak_v1/nl_NL/cd/view.tt
    <div class="view_cd">
        <div class="cdtitle">[% cd.artist %] - [% cd.name %]</div>

        <div class="barcode">[% cd.barcode %]</div>
    </div>

Exceedingly simple in our case, but you get the idea. If you're unsure of what
to put in and what not, sneak a peek at the other templates, and you'll pick
up the vibe.
