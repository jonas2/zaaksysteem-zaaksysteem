Run on database:

# cat 1002-documentairly.sql | psql DATABASE

Creating a clean database

# pg_dump -c --schema-only DATABASE > schema.sql
# psql template1
#> DROP DATABASE [DATABASE];
#> CREATE DATABASE [DATABASE] WITH OWNER zaaksysteem ENCODING 'UTF-8';
#>\q
#
#cat schema.sql | psql DATABASE

Make sure you set a config item pointing to data store

-- INSERT INTO config (parameter, value) VALUES ('filestore_location', '/srv/zaaksysteem/data/CUSTOMER.zaaksysteem.nl/storage');
-- INSERT INTO config (parameter, value) VALUES ('tmp_location', '/tmp/zaaksysteem');

