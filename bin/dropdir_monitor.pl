#!/usr/bin/perl

use warnings;
use strict;
use FindBin qw/$Bin/;

use lib "$Bin/../lib";

use ClamAV::Client;
use Zaaksysteem::Constants;
use Zaaksysteem::Schema;

use Getopt::Long qw{ :config no_ignore_case };
my %opt = (
    directory   => '',
    dbi         => '',
    db_username => undef,
    db_password => undef,
    create_subject => 'betrokkene-medewerker-20000',
);

GetOptions \%opt => qw{
    directory=s
    dbi=s
    db_username=s
    db_password=s
    create_subject=s
} or exit usage();

my @args = ($opt{dbi}, $opt{db_username}, $opt{db_password});

my $schema = Zaaksysteem::Schema->connect(
    @args, {AutoCommit => 1},
);

my $scanner = ClamAV::Client->new();

sub import_directory {
    my ($directory) = @_;

    opendir(DIR, $directory);

    my @errors;
    for my $file (readdir(DIR)) {
        # Skip directories
        next if (!-f "$directory/$file");

        # Files younger than 60 seconds are considered to possible
        # be incomplete.
        my @stat = stat("$directory/$file") or die "Unable to stat $file";
        next if (time - $stat[9] < 60);

        # Virusscanner
        #my ($path, $scan_result) = $scanner->scan_path("$directory/$file");
        #if ($path && $scan_result) {
        #    push @errors, "Error processing $file: virus found, file removed";
        #    unlink("$directory/$file");
        #    next;
        #}
       
        eval {
            $schema->resultset('File')->file_create({
                file_path => "$directory/$file",
                name      => $file,
                db_params => {
                    created_by => $opt{create_subject},
                },
            })
        };
        if ($@) {
            push @errors, "Error processing $file: $@";
        }
        else {
            unlink "$directory/$file";
        }
    }

    if (@errors) {
        print "Errors occurred processing files in dropdir:\n\n";
        print join("\n", @errors) if @errors;
        print "\n";
    }
}

sub usage {

print qq[Imports files from a certain directory in zaaksysteem documentintake.

--directory        Directory to read files from
--dbi              DBI DSN string
--db_username      DBI username
--db_password      DBI password
--create_subject   Subject to create files with (ex. betrokkene-medewerker-20000)

];
}

import_directory($opt{directory});

1;
