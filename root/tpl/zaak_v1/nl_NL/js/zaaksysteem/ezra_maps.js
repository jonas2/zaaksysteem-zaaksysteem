$(document).ready(function() {
    //Initialized from initializeEverything
    //load_ezra_map();
});

function load_ezra_map() {
    if ($('.ezra_map').length) {
        var proxy_host       = '/plugins/maps/proxy/geodata';
        var ezra_maps_layers = {
            layers: {
                nationaalregister: new OpenLayers.Layer.TMS(
                    'Nationaal Register',
                    //'http://geodata.nationaalgeoregister.nl/tms/',
                    proxy_host + '/tms/',
                    {
                        layername       : 'brtachtergrondkaart',
                        type            : "png8",
                        transparent     : true,
                        visibility      : true,
                        singleTile      : false,
                        alpha           : true,
                        numZoomLevels   : 14,
                        zoom            : 6,
                        projection      : 'EPSG:28992',
                        units           : 'm',
                        resolutions     : [3440.640,1720.320, 860.160, 430.080, 215.040, 107.520, 53.760, 26.880, 13.440, 6.720, 3.360, 1.680, 0.840, 0.420, 0.210, 0.105, 0.0525]
                        //opacity         : 0.7,
                    }
                ),
                grenzen_gemeenten2012: new OpenLayers.Layer.WMS("Gemeente grenzen 2012",
                    //'http://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms?',
                    proxy_host + '/bestuurlijkegrenzen/wms?',
                    {'layers': 'gemeenten_2012', 'format': 'image/png', transparent: true},
                    {'isBaseLayer': false, singleTile: true, buffer: 0,  visibility: false, featureInfoFormat: "application/vnd.ogc.gml"}
                ),
                grenzen_provincies2012: new OpenLayers.Layer.WMS("Provincie grenzen 2012",
                    //'http://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms?',
                    proxy_host + '/bestuurlijkegrenzen/wms?',
                    {'layers': 'provincies_2012', 'format': 'image/png', transparent: true},
                    {'isBaseLayer': false, singleTile: true, buffer: 0,  visibility: false, featureInfoFormat: "application/vnd.ogc.gml"}
                )
            },
            base_layer: 'nationaalregister'
        };

        $('.ezra_map').ezra_map({
            layer_type  : 'pdoc',
            theme       : '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/style.css',
            img_path    : '/tpl/zaak_v1/nl_NL/js/OpenLayers-2.12/theme/dark/img/',
            marker_icon : '/tpl/zaak_v1/nl_NL/images/marker.png',
            geocode_url : '/plugins/maps',
            viewport    : {
                smallfixed:  {
                    css: {
                        width: '520px',
                        height: '350px'
                    }
                },
                smallauto:  {
                    css: {
                        width: 'auto',
                        height: '350px'
                    }
                },
                medium:  {
                    css: {
                        width: '1024px',
                        height: '768px'
                    }
                },
                large:  {
                    css: {
                        width: '900px',
                        height: '400px'
                    }
                }
            },
            layers      : ezra_maps_layers.layers,
            base_layer  : ezra_maps_layers.base_layer,
            proxy       : '/maps-tiles',
            update      : function (element) {
                var input_element = element.find('.ezra_map-kenmerkfield');
                updateField(input_element);
            }
        });
    }

    //$('.ezra_map').each(function() {
        //$(this).ezra_map('setAddress', 'Amsterdam, Marnixstraat 32');
        //$(this).ezra_map('setAddress', 'Amsterdam, Marnixstraat 10');
        //$(this).ezra_map('setAddress', 'Amsterdam, Marnixstraat 40');
        //$(this).ezra_map('setAddress', 'Amsterdam, Marnixstraat 60');
    //});
    //$('.ezra_map').ezra_map('setAddress', 'Amsterdam, Binnen dommersstraat 24');
    //$('.ezra_map').ezra_map('clearMouseCache');
}

(function( $ ){
    var methods = {
        required: function(options) {
            var required = [
                'layer_type',
                'theme',
                'viewport'
            ];

            if (!options && required && required.length) { 
                $(this).ezra_map(
                    'log',
                    'ezra_map error: No options given'
                );
                return false;
            }

            for(var i = 0; i < required.length; i++) {
                var param = required[i];

                if(!options[param]) {
                    $(this).ezra_map(
                        'log',
                        'ezra_map error: Missing required parameter ' + param
                    );
                    return false;
                }
            }

            return true;
        },
        init : function( options ) {
            return this.each(function(){
                var $this   = $(this),
                    data    = $this.data('ezra_map');

                // Check required parameters
                if (!$this.ezra_map('required', options)) {
                    return false;
                }

                if ($this.hasClass('ezra_map-initialized')) {
                    return true;
                }

                // If the plugin hasn't been initialized yet
                if ( ! data ) {
                    /*
                    Do more setup stuff here
                    */
                    var layer   = $this.ezra_map('_load_base_layer', options);
                    var map     = $this.ezra_map('_load_map', options, layer);

                    $(this).data('ezra_map', {
                        target  : $this,
                        map     : map,
                        options : options
                    });

                    // Check for option center or class center
                    if ($this.find('.ezra_map-center').length) {
                        options.center = $this.find('.ezra_map-center').val();
                    }

                    $this.ezra_map('_load_autocomplete');
                    $this.ezra_map('_load_markers');

                    if(!$this.hasClass('ezra_map-readonly')) {
                        var vectorLayer = map.getLayer('Markers');

                        vectorLayer.events.includeXY = false;
                        vectorLayer.events.fallThrough = true;

                        vectorLayer.events.register('touchstart', this, function() {
                            var touch = event.changedTouches[0];
                            $this._start_clientX = event.clientX; 
                            $this._start_clientY = event.clientY; 
                            $this.touchStartTime = new Date().getTime();                      
                            return false;
                        });
    
                        vectorLayer.events.register('touchend', this, function(event) {
                            var touch = event.changedTouches[0];
                            var clientX = touch.clientX;
                            var clientY = touch.clientY;
    
                            //if finger moves more than 10px flag to cancel
                            // alternative: use time diff
                            //http://trac.osgeo.org/openlayers/attachment/ticket/3424/click_handler_patch.txt
                            //var touchLength = new Date().getTime() - $this.touchStartTime;
    
                            if (Math.abs(clientX - $this._start_clientX) > 10 || Math.abs(clientY - $this._start_clientY) > 10) {
                            } else {
                                setTimeout( function() { 
                                    $this.ezra_map('_set_click_handler', event)
                                }, 0);
                            }
                            return false; 
                        });
                    }

                    $this.addClass('ezra_map-initialized');
                }
            });
        },
        _load_autocomplete: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options;

            if (! jQuery().autocomplete ) {
                // No autocomplete function available, stop
                return false;
            }

            $this.find('input[type="text"].ezra_map-autocomplete').each(function() {
                $(this).autocomplete(
                    {
                        source: function(tag, response) {
                            $.getJSON(options.geocode_url,
                                {
                                    'term': tag['term']
                                },
                                function(data) {
                                    if (!(data.json.success == '1'))
                                    {
                                        response();
                                        return false;
                                    }

                                    var addresses = data.json.addresses;

                                    var list_of_addresses = [];
                                    for(var i = 0; i < addresses.length; i++) {
                                        var address = addresses[i];

                                        if (!address.identification) {
                                            continue;
                                        }

                                        list_of_addresses.push(address.identification);
                                    }

                                    response(list_of_addresses);
                                }
                            );
                        },
                        select: function(event, ui) {
                            var only_center = 0;
                            if ($this.hasClass('ezra_map-readonly')) {
                                only_center = 1;
                            }

                            $this.ezra_map('setAddress', { 
                                value       : ui.item.value, 
                                only_center : only_center
                            });
                        },
                        open: function() {
                            $(this).autocomplete('widget').css('z-index', 2000);
                        }
                    }
                );

                $(this).autocomplete('widget').css('z-index', 1000);
            });
        },
        _set_click_handler: function(event) {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options;

//            var pixel = new OpenLayers.Pixel(event.layerX, event.layerY);

            var p;
            if(typeof event.hasOwnProperty != 'undefined' && event.hasOwnProperty('changedTouches')) {
                p = fetch('nl.mintlab.utils.dom.getMousePosition')(event.changedTouches[0]); 
            } else {
                p = fetch('nl.mintlab.utils.dom.getMousePosition')(event); 
            }
            
            var local = fetch('nl.mintlab.utils.dom.fromGlobalToLocal')($this.find('.map')[0], p);

            var pixel = new OpenLayers.Pixel(local.x, local.y);
   
            var lonlat  = map.getLonLatFromViewPortPx(pixel);

            var wgs     = $this.ezra_map('_get_wgs_latlon', lonlat.lon, lonlat.lat);

            $.getJSON(options.geocode_url,
                {
                    'term'              : wgs.lat +' ' + wgs.lon
                },
                function(data) {
                    if (data.json.success == '1')
                    {
                        var geo     = data.json.addresses[0],
                        lat         = geo.coordinates.lat,
                        lon         = geo.coordinates.lng,
                        address     = geo.address;

                        var epsg    = $this.ezra_map('_get_epsg_points', lat, lon);

                        $this.ezra_map('setMarker', {
                            longitude       : epsg['x'], 
                            latitude        : epsg['y'], 
                            identification  : geo.identification, 
                            popup_data      : geo
                        });
                    }
                }
            );
        },
        _load_markers: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                options = data.options;

            markers     = new OpenLayers.Layer.Vector(
                'Markers',
                {
                    eventListeners: {
                        'featureselected': function(evt) {
                            var feature         = evt.feature;

                            var popup           = new OpenLayers.Popup.FramedCloud(
                                "popup",
                                OpenLayers.LonLat.fromString(feature.geometry.toShortString()),
                                new OpenLayers.Size(240,150),
                                feature.attributes.htmlcontent,
                                null,
                                null
                            );

                            popup.panMapIfOutOfView = true;

                            popup.autoSize      = true;
                            popup.minSize       = new OpenLayers.Size(240,150);

                            feature.popup       = popup;

                            map.addPopup(popup);

                            popup.show();
                        },
                        'featureunselected': function(evt) {
                            var feature = evt.feature;
                            map.removePopup(feature.popup);
                            feature.popup.destroy();
                            feature.popup = null;
                        }
                    }
                }
            );

            markers.id  = 'Markers';

            var selector = new OpenLayers.Control.SelectFeature(
                markers,
                {
                    autoActivate:true
                }
            );

            map.addControl(selector);
            map.addLayer(markers);

            data.selector   = selector;

            // Make map clickable
            if (!$this.hasClass('ezra_map-readonly')) {
                map.events.register('click', this, methods['_set_click_handler']);
            }

            // Search for input fields
            $this.find('input[class="ezra_map-address"]').each(function() {
                $this.ezra_map('setAddress', {
                    value : $(this).val(),
                    no_update: 1
                });
            });

            // Search for input fields with json content
            $this.find('input[class="ezra_map-json"]').each(function() {
                $this.ezra_map('setFromJSON', jQuery.parseJSON($(this).val()));
            });

            if (options.center) {
                var lonlat = options.center.split(" ");

                if (lonlat) {
                    var epsg    = $this.ezra_map('_get_epsg_points', lonlat[0], lonlat[1]);
                    $this.ezra_map('setCenter', epsg['x'], epsg['y']);
                }
            }
        },
        clearMarkers: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            data.selector.unselectAll();
            markers.removeAllFeatures();
        },
        setCenter: function(lon, lat) {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            var lonlat  = new OpenLayers.LonLat(lon, lat);
            map.setCenter(lonlat, 9);
        },
        setMarker: function(options) {
            if(
                !options.hasOwnProperty('longitude') && 
                !options.hasOwnProperty('latitude')  &&      
                !options.hasOwnProperty('popup_data')       // data fields to fill out template
            ) {
                // required fields
                return;
            }

            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map,
                markers = map.getLayer('Markers');

            var lonlat  = new OpenLayers.LonLat(options.longitude, options.latitude);

            if (options.center) {
                map.setCenter(lonlat, 9);
            }

            var point   = new OpenLayers.Geometry.Point(options.longitude, options.latitude);

            var marker  = new OpenLayers.Feature.Vector(
                    point,
                    {
                        htmlcontent: $this.ezra_map('get_template', options.popup_data)
                    },
                    {
                        externalGraphic: data.options.marker_icon,
                        graphicOpacity: 1.0,
                        graphicWith: 21,
                        graphicHeight: 34,
                        graphicYOffset: -28
                    }
            );

//            marker.style = {
//                externalGraphic: data.options.marker_icon,
//                graphicOpacity: 1.0,
//                graphicWith: 21,
//                graphicHeight: 34,
//                graphicYOffset: -5
//            };

            // Clear all markers
            if (!$this.hasClass('ezra_map-multiple')) {
                $this.ezra_map('clearMarkers');
            }

            markers.addFeatures(marker);

            if (!options.no_popup) {
                data.selector.select(marker);
            }

            // Set address to input type = hidden
            if (options.identification) {
                $this.find('input.ezra_map-address').val(options.identification);

                if (!options.no_update) {
                    data.options.update(this);
                }
            }
        },
        get_template: function(data_mapping) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var tpl     = $this.find('.ezra_map-template').clone().show().html();

            tpl         = tpl.replace(/(%%.*?%%)/g, function(match, contents) {
                var map_key = contents.replace(/^%%/, '');
                map_key     = map_key.replace(/%%$/, '');

                if (data_mapping[map_key]) {
                    return data_mapping[map_key];
                }

                return '';
            });

            return tpl;
        },
        setAddress: function(options) {
            var address = options.value;
            var only_center = options.only_center;

            var $this   = $(this),
                data    = $this.data('ezra_map');

            var map     = data.map;

            $.getJSON('/plugins/maps',
                {
                    'term': address
                },
                function(data) {
                    if (!data || !data.json || !data.json.success ) { return; }

                    if (data.json.success == '1')
                    {
                        var geo     = data.json.addresses[0];
                        lat         = geo.coordinates.lat;
                        lon         = geo.coordinates.lng;

                        var epsg    = $this.ezra_map('_get_epsg_points', lat, lon);
                        if (only_center) {
                            $this.ezra_map('setCenter', epsg['x'], epsg['y']);
                        } else {
                            $this.ezra_map('setMarker', {
                                longitude       : epsg['x'], 
                                latitude        : epsg['y'], 
                                identification  : geo.identification, 
                                popup_data      : geo, 
                                center          : 1,
                                no_update       : options.no_update
                            });
                        }
                    }
                }
            );
        },
        setFromJSON: function(json, only_center) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var map     = data.map;

            lat         = json.coordinates.lat;
            lon         = json.coordinates.lng;

            var epsg    = $this.ezra_map('_get_epsg_points', lat, lon);
            $this.ezra_map('setMarker', { 
                longitude       : epsg['x'], 
                latitude        : epsg['y'], 
                identification  : null, 
                popup_data      : json, 
                center          : null, 
                no_popup        : 1,
                no_update       : 1
            });
        },
        _get_wgs_latlon: function(point_x,point_y) {
            // This calculation was based from the sourcecode of Ejo Schrama's software <schrama@geo.tudelft.nl>.
            // You can find his software on: http://www.xs4all.nl/~digirini/contents/gps.html

            // Fixed constants / coefficients
            var x0      = 155000;
            var y0      = 463000;
            var k       = 0.9999079;
            var bigr    = 6382644.571;
            var m       = 0.003773954;
            var n       = 1.000475857;
            var lambda0 = 0.094032038;
            var phi0    = 0.910296727;
            var l0      = 0.094032038;
            var b0      = 0.909684757;
            var e       = 0.081696831;
            var a       = 6377397.155;

            // Convert RD to Bessel

            // Get radius from origin.
            d_1 = point_x - x0;
            d_2 = point_y - y0;
            r   = Math.sqrt( Math.pow(d_1, 2) + Math.pow(d_2, 2) );  // Pythagoras

            // Get Math.sin/Math.cos of the angle
            sa  = (r != 0 ? d_1 / r : 0);  // the if prevents devision by zero.
            ca  = (r != 0 ? d_2 / r : 0);

            psi  = Math.atan2(r, k * 2 * bigr) * 2;   // php does (y,x), excel does (x,y)
            cpsi = Math.cos(psi);
            spsi = Math.sin(psi);

            sb = (ca * Math.cos(b0) * spsi) + (Math.sin(b0) * cpsi);
            d_1 = sb;
            
            cb = Math.sqrt(1 - Math.pow(d_1, 2));  // = Math.cos(b)
            b  = Math.acos(cb);
            
            sdl = sa * spsi / cb;  // = Math.sin(dl)
            dl  = Math.asin(sdl);         // delta-lambda

            lambda_1 = dl / n + lambda0;
            w        = Math.log(Math.tan((b / 2) + (Math.PI / 4)));
            q        = (w - m) / n;

            // Create first phi and delta-q
            phiprime = (Math.atan(Math.exp(q)) * 2) - (Math.PI / 2);
            dq_1     = (e / 2) * Math.log((e * Math.sin(phiprime) + 1) / (1 - e * Math.sin(phiprime)));
            phi_1    = (Math.atan(Math.exp(q + dq_1)) * 2) - (Math.PI / 2);

            // Create new phi with delta-q
            dq_2     = (e / 2) * Math.log((e * Math.sin(phi_1) + 1) / (1 - e * Math.sin(phi_1)));
            phi_2    = (Math.atan(Math.exp(q + dq_2)) * 2) - (Math.PI / 2);

            // and again..
            dq_3     = (e / 2) * Math.log((e * Math.sin(phi_2) + 1) / (1 - e * Math.sin(phi_2)));
            phi_3    = (Math.atan(Math.exp(q + dq_3)) * 2) - (Math.PI / 2);
            
            // and again...
            dq_4     = (e / 2) * Math.log((e * Math.sin(phi_3) + 1) / (1 - e * Math.sin(phi_3)));
            phi_4    = (Math.atan(Math.exp(q + dq_4)) * 2) - (Math.PI / 2);

            // radians to degrees
            lambda_2 = lambda_1 / Math.PI * 180;  // 
            phi_5    = phi_4    / Math.PI * 180;


            // Bessel to wgs84 (lat/lon)
            dphi   = phi_5    - 52;   // delta-phi
            dlam   = lambda_2 -  5;   // delta-lambda
            
            phicor = (-96.862 - (dphi * 11.714) - (dlam * 0.125)) * 0.00001; // correction factor?
            lamcor = ((dphi * 0.329) - 37.902 - (dlam * 14.667))  * 0.00001;
            
            phiwgs = phi_5    + phicor;
            lamwgs = lambda_2 + lamcor;


            // Return as anonymous object
            return { lat: phiwgs, lon: lamwgs };
        },
        _get_epsg_points: function(lat, lon) {
            // Fixed constants / coefficients
            x0      = 155000;
            y0      = 463000;
            k       = 0.9999079;
            bigr    = 6382644.571;
            m       = 0.003773954;
            n       = 1.000475857;
            lambda0 = 0.094032038;
            phi0    = 0.910296727;
            l0      = 0.094032038;
            b0      = 0.909684757;
            e       = 0.081696831;
            a       = 6377397.155;
            
            // wgs84 to bessel
            dphi = lat - 52;
            dlam = lon - 5;
                
            phicor = ( -96.862 - dphi * 11.714 - dlam * 0.125 ) * 0.00001;
            lamcor = ( dphi * 0.329 - 37.902 - dlam * 14.667 ) * 0.00001;

            phibes = lat - phicor;
            lambes = lon - lamcor;

            // bessel to rd
            phi         = phibes / 180 * Math.PI;
            lambda      = lambes / 180 * Math.PI;
            qprime      = Math.log( Math.tan( phi / 2 + Math.PI / 4 ));
            dq          = e / 2 * Math.log(( e * Math.sin(phi) + 1 ) / ( 1 - e * Math.sin( phi ) ) );
            q           = qprime - dq;
                
            w           = n * q + m;
            b           = Math.atan( Math.exp( w ) ) * 2 - Math.PI / 2;
            dl          = n * ( lambda - lambda0 );

            d_1         = Math.sin( ( b - b0 ) / 2 );
            d_2         = Math.sin( dl / 2 );
            
            s2psihalf   = d_1 * d_1 + d_2 * d_2 * Math.cos( b ) * Math.cos ( b0 );
            cpsihalf    = Math.sqrt( 1 - s2psihalf );
            spsihalf    = Math.sqrt( s2psihalf );
            tpsihalf    = spsihalf / cpsihalf;
            
            spsi        = spsihalf * 2 * cpsihalf;
            cpsi        = 1 - s2psihalf * 2;
            
            sa          = Math.sin( dl ) * Math.cos( b ) / spsi;
            ca          = ( Math.sin( b ) - Math.sin( b0 ) * cpsi ) / ( Math.cos( b0 ) * spsi );
            
            r           = k * 2 * bigr * tpsihalf;
            x           = r * sa + x0;
            y           = r * ca + y0;

            // Return as anonymous object.
            return { x: x, y: y };
        },
        _load_custom_marker_icon: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                options = data.options;

            if (!options.marker_icon) {
                return false;
            }

            var size    = new OpenLayers.Size(21,34);
            var offset  = new OpenLayers.Pixel(-(size.w/2), -size.h);
            var icon    = new OpenLayers.Icon(options.marker_icon, size, offset);

            return icon.clone();
        },
        _load_layer: function(options, name) {
            return options.layers[name].clone();
        },
        _load_layers: function(options) {
            var layers = [];
            jQuery.each(options.layers, function (key, value) {
                layers.push(value.clone());
            });
            return layers;
        },
        _load_base_layer: function(options) {
            var $this   = $(this);

            var base_layer_name = options.base_layer;

            return $this.ezra_map('_load_layer', options, base_layer_name);
        },
        refreshViewport: function(options) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            if (!data && !options) {
                return false;
            }

            if (!options) { options = data.options; }

            var mapelem = $this.find('.ezra_map-viewport');

            $.each(options.viewport, function(key,value) {
                var viewport_conf = options.viewport[key];
                if ($this.hasClass('ezra_map-' + key)) {
                    $.each(viewport_conf.css, function(csskey, cssval) {
                        mapelem.css(csskey,cssval);
                    });
                }
            });

            mapelem.attr('id', 'ezra_map-id-' + Math.floor((Math.random() * 10000)+1));

            return mapelem;
        },
        clearMouseCache: function() {
            var $this   = $(this),
                data    = $this.data('ezra_map'),
                map     = data.map;

            markers = map.getLayer('Markers');
            markers.events.clearMouseCache();
        },
        _load_map: function(options, layer) {
            var $this   = $(this),
                data    = $this.data('ezra_map');

            var mapelem = $this.ezra_map('refreshViewport', options);

            if (options.img_path) {
                OpenLayers.ImgPath = options.img_path;
            }

            var map = new OpenLayers.Map(
                mapelem.attr('id'),
                {
                    theme: options.theme,
                    xy_precision: 3,
                    zoom: 3,
                    center: '155000,463000',
                    maxExtent       : new OpenLayers.Bounds(-285401.92,22598.08,595401.92,903401.92),
                    controls: [
                        new OpenLayers.Control.Navigation(),
                        new OpenLayers.Control.ArgParser(),
                        new OpenLayers.Control.PanZoomBar(),
                        new OpenLayers.Control.LayerSwitcher({'ascending':false}),
                        new OpenLayers.Control.ScaleLine(),
                        new OpenLayers.Control.MousePosition(),
                        new OpenLayers.Control.KeyboardDefaults(
                            {
                                observeElement: mapelem.attr('id')
                            }
                        )
                    ]
                }
            );
        
            map.addLayers($this.ezra_map('_load_layers', options));
            map.setBaseLayer(layer);
            map.zoomToMaxExtent();

            var lonlat  = new OpenLayers.LonLat(155000,463000);
            map.setCenter(lonlat, 3);

            return map;
        },
        destroy : function( ) {
            return this.each(function(){
                var $this = $(this),
                    data = $this.data('ezra_map');

                // Namespacing FTW
                $(window).unbind('.ezra_map');
                data.ezra_map.remove();
                $this.removeData('ezra_map');
            });
        },
        //reposition : function( ) { // ... },
        //show : function( ) { // ... },
        //hide : function( ) { // ... },
        //update : function( content ) { // ...}
        log: function(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    $.fn.ezra_map = function( method ) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_map' );
        }
    };
})( jQuery );
