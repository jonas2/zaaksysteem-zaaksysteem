[%
#
# Widget for selection of betrokkene.
# * Show form elements to choose an available betrokkene.
# * Show currently selected betrokkene
# * Three types of betrokkenen: Medewerker (internal), NatuurlijkPersoon, Bedrijf (both External)
# 
#
# Optional:
# - betrokkene_id   : id of the currently selected betrokkene
# - betrokkene_naam : name of the currently selected betrokkene 
#                    (TODO = make betrokkene so that we just look this up dynamically)
# - betrokkene_type : currently selected betrokkene_type
# - allow_internal  : offer the option to choose an internal betrokkene (Medewerker)
# - allow_external  : offer the option to choose an external betrokkene (NatuurlijkPersoon/Bedrijf).
# - row_classes     : array of classes to append to this row
#
# Required:
# - instance_id     : prefix class for the fields for the form to read, required to allow for multiple instances
# - input_name      : the name of the input field where the resulting id goes.
# - label           : the text before the input
%]

[% IF !instance_id || !input_name || !label %]
    ERROR: need instance_id input_name label
    [% RETURN %]
[% END %]


[% IF !betrokkene_type %]
    [% betrokkene_type = 'natuurlijk_persoon' %]
[% END %]


[% IF !allow_external && betrokkene_type != 'medewerker' %]
    ERROR: only medewerker can be selected now
    [% RETURN %]
[% END %]

[% widget_betrokkene_select_row_classes = ['ezra_select_betrokkene2_type'] %]

[% IF row_classes %]
    [% widget_betrokkene_select_row_classes = 
        widget_betrokkene_select_row_classes.merge(row_classes)
    %]
[% END %]


[% IF allow_external %]
<tr id="[% instance_id %]_type" class="ezra_select_betrokkene2_type">
	<td class="td-label">Type [% label |lower %]</td>
	<td>
		<div class="ezra_betrokkene_type">
		
		    [% IF allow_external %]
			<div class="radio-item">
				<input type="radio" name="betrokkene_type" value="natuurlijk_persoon" 
				[% IF betrokkene_type && betrokkene_type == 'natuurlijk_persoon'%]
				    checked="checked"
				[% END %]
				/>
				<label for="burger">Burger</label>
			</div><br/>
			<div class="radio-item">
				<input type="radio" name="betrokkene_type" value="bedrijf"				
                [% IF betrokkene_type && betrokkene_type == 'bedrijf'%]
				    checked="checked"
				[% END %]
				/>
				<label for="organisatie">Organisatie</label>
			</div><br/>
			[% END %]
			[% IF allow_internal %]
			<div class="radio-item">
				<input type="radio" name="betrokkene_type" value="medewerker"
                [% IF betrokkene_type && betrokkene_type == 'medewerker'%]
				    checked="checked"
				[% END %]
				/>
                <label for="medewerker">Medewerker</label>
			</div>
			[% END %]
			</div>
		</div>
	</td>
	<td>
	    [% PROCESS widgets/general/validator.tt %]
	    [% PROCESS widgets/general/tooltip.tt
	        message = 'Geef aan of deze zaak wordt gestart op
				initiatief van een <strong>burger</strong>
				of een <strong>organisatie</strong>
				(bedrijf of instelling).'
		%]
	</td>
</tr>
[% END %]
<tr id="[% instance_id %]" class="[% widget_betrokkene_select_row_classes.join(' ') %]">
	<td class="td-label">
		<span>[% label %]</span>
	</td>
	<td>
		<div class="ezra_objectsearch_betrokkene zoek_veld">    
	        <div class="search_wrap">
                <input type="text" name="betrokkene_naam" 
                    class="input_large searchbox ezra_id_medewerker_naam" value="[% betrokkene_naam %]"
                    placeholder="Typ hier uw zoekterm" />
    			<span class="icon icon-cancel-search" style="display: none; ">Close</span>
    		</div>
			<a href="#"
					class="ezra_betrokkene_selector knop knop-bootstrap"
				rel="betrokkene_type: [% betrokkene_type %];
				selector_identifier: #[% instance_id %].ezra_select_betrokkene2_type input[name='[% input_name %]'];
				selector_naam: #[% instance_id %].ezra_select_betrokkene2_type input[name='betrokkene_naam'];"
				><i class="icon-magnifier icon"></i></a>
			<span class="error"></span>
			<input
                type="hidden"
                name="[% input_name %]"
                value="[% betrokkene_id %]"
            />
		</div>
	</td>
	<td>
	    [% PROCESS widgets/general/validator.tt %]
	    [% PROCESS widgets/general/tooltip.tt
	        message = 'Selecteer een aanvrager voor deze zaak. 
				Vul delen van de naam of het adres, burgerservicenummer of KVK nummer in
				totdat de gezochte betrokkene verschijnt.'
		%]
	</td>
</tr>

